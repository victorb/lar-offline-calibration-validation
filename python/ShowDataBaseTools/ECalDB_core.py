
import os, sys, time, ctypes
import sqlite3

# ----- Locale -----
from Validation_definitions import *
from ECalDB_definitions import *
from ECalDB_utils import *


class ECalDB(object):
	#------------------------------------------------------------------------
	# __init__
	#------------------------------------------------------------------------
	def __init__(self, _gain, _verbose):
		self.gain = _gain
		self.verbose = _verbose
		
	#------------------------------------------------------------------------
	# GetEngineName
	#------------------------------------------------------------------------
	def GetEngineName(self):
		return 'sqlite3'
	
	#------------------------------------------------------------------------
	# Make addres DB
	#------------------------------------------------------------------------
	def MakeAddressBD(self, _path):
		self.db_address = _path + '/' + DB_NAME_PATTERN + self.gain + '.db'
		if self.verbose["low"]:
			print MED_INDENT,"dataBase is: %s" % self.db_address
	
	#-------------------------------------------------------------------------
	# Open
	#-------------------------------------------------------------------------
	def Open(self, _path):
		self.MakeAddressBD(_path)
		try:
			self.db = sqlite3.connect(self.db_address, timeout=30)
			self.cursor = self.db.cursor()
		except sqlite3.Error, errmsg:
			raise Exception(str(errmsg))

	#-------------------------------------------------------------------------
	# Close
	#-------------------------------------------------------------------------
	def Close(self):
		self.cursor.close()
		self.db.close()
	
	#------------------------------------------------------------------------
	# Execute
	#------------------------------------------------------------------------
	def Execute(self, _query):
		result = []
		try:
			self.cursor.execute(_query)
			result = BuildDict(self.cursor.description, self.cursor.fetchall())
		except sqlite3.Error, errmsg:
			print str(errmsg)
		return result

	
	#------------------------------------------------------------------------
	# GetCampaignMaxId
	#------------------------------------------------------------------------
	def GetCampaignMaxId(self):
		_maxId = 1
		try:
			_query = self.Execute("SELECT MAX(id) FROM Campaign")
			if len(_query)>0 and not (_query[0]['MAX(id)'] is None):
				_maxId = _query[0]['MAX(id)']
		except AttributeError: 
			raise Exception('DB not opened')
		return _maxId
	#------------------------------------------------------------------------
	# GetCampaignList
	#------------------------------------------------------------------------
	def GetCampaignList(self, _idCamp=1):
		lCamp = []
		try:
			lCamp = self.Execute("SELECT * FROM Campaign WHERE id>='%d' ORDER by date" % (_idCamp))
		except AttributeError:
			raise Exception('DB not opened')
		return lCamp
	#------------------------------------------------------------------------
	# GetCampaignById
	#------------------------------------------------------------------------
	def GetCampaignById(self, _idCamp):
		lCamp = []
		try:
			lCamp = self.Execute("SELECT * FROM Campaign WHERE id='%d' ORDER BY date" % (_idCamp))
		except AttributeError:
			raise Exception('DB not opened')
		return lCamp
	
	#------------------------------------------------------------------------
	# DeleteRowsByCampaignId
	#------------------------------------------------------------------------
	def DeleteRowsByCampaignId(self, _idCamp):
		try:
			self.Execute("DELETE FROM Campaign WHERE id='%d'" % _idCamp)
			self.Execute("DELETE FROM Run WHERE campaign='%d'" % _idCamp)
			self.Execute("DELETE FROM Channel WHERE campaign='%d'" % _idCamp)
			self.Execute("DELETE FROM Feb WHERE campaign='%d'" % _idCamp)
		except AttributeError:
			raise Exception('DB not opened')
		self.db.commit()
	#------------------------------------------------------------------------
	# ExecuteVacuum
	#------------------------------------------------------------------------
	def ExecuteVacuum(self):
		self.db.execute("VACUUM")
	

	#------------------------------------------------------------------------
	# GetRunByCampaignId
	#------------------------------------------------------------------------
	def GetRunByCampaignId(self, _idCamp):
		lRun = []
		try:
			lRun = self.Execute("SELECT * FROM Run WHERE campaign='%d' ORDER BY date_start" % (_idCamp))
		except AttributeError:
			raise Exception('DB not opened')
		return lRun
	
	
	#------------------------------------------------------------------------
	# GetChannelByCampaignAndSubPartition
	#------------------------------------------------------------------------
	def GetChannelByCampaignAndSubPartition(self, _idCamp, _subPart):
		lChan = []
		try:
			lChan = self.Execute('''SELECT * FROM Channel WHERE campaign='%d' AND sub_sub_partition='%s' 
				ORDER BY ft, sl, ch, cl''' % (_idCamp, _subPart))
		except AttributeError:
			raise Exception('DB not opened')
		return lChan
	
	
	#------------------------------------------------------------------------
	# PrintCampaign
	#------------------------------------------------------------------------
	def PrintCampaign(self, _lCamp):
		_nCamp = len(_lCamp)
		if self.verbose["low"]:
			print BIG_INDENT,"So, found %d campaigns" % _nCamp
		print BIG_INDENT,"="*100
		print BIG_INDENT,"        refCamp idCamp typeCamp             dateCamp  toroid solenoid     flagDQ      flag      db"
		print BIG_INDENT,"="*100
		_loop = 1
		for _camp in _lCamp:
			print BIG_INDENT,"%3d/%-3d %7d %6d %8s %20s  %6s %8s  %9s %9s %7s" \
				% (_loop, _nCamp, _camp['ref'], _camp['id'], _camp['type'], GetDateTime(_camp['date']), \
					_camp['toroid_status'], _camp['solenoid_status'], _camp['dq_flag'], _camp['flag'], _camp['db'])
			_loop += 1
	#------------------------------------------------------------------------
	# PrintCampaignRun
	#------------------------------------------------------------------------
	def PrintCampaignRun(self, _lCamp, _lRun):
		_nCamp = len(_lCamp)
		if self.verbose["low"]:
			print BIG_INDENT,"So, found %d campaigns" % _nCamp
		_loopCamp = 1
		for _camp in _lCamp:
			_id_camp = _camp['id']
			print BIG_INDENT,"="*125
			print BIG_INDENT,"        refCamp idCamp typeCamp             dateCamp  toroid solenoid     flagDQ      flag      db"
			print BIG_INDENT,"="*125
			print BIG_INDENT,"%3d/%-3d %7d %6d %8s %20s  %6s %8s  %9s %9s %7s" \
				% (_loopCamp, _nCamp, _camp['ref'], _camp['id'], _camp['type'], GetDateTime(_camp['date']), \
					_camp['toroid_status'], _camp['solenoid_status'], _camp['dq_flag'], _camp['flag'], _camp['db'])
			_loopCamp += 1
	
			print BIG_INDENT,"-"*125
			print BIG_INDENT,"       idRun            dateStart             dateStop      type  partition  gain    toroid solenoid     flagDQ    flag"
			print BIG_INDENT,"-"*125
			for _runs in _lRun:
				_id_camp_run = _runs[0]['campaign']
				if _id_camp==_id_camp_run:
					_loopRun = 1
					for _run in _runs:
						print BIG_INDENT,"%3d  %7d %20s %20s %9s %10s %5s %9.2f %8.2f %10s %7s" \
							% (_loopRun, _run['id'], GetDateTime(_run['date_start']), GetDateTime(_run['date_stop']), \
								_run['type'], _run['partition'], _run['gain'], \
								_run['toroid'], _run['solenoid'], _run['dq_flag'], _run['flag'])
						_loopRun += 1
					print ""
	#------------------------------------------------------------------------
	# PrintChannel
	#------------------------------------------------------------------------
	def PrintChannel(self, _channels, _typeCamp):
	
		_devSet = dict({})
		if _typeCamp=='DAILY':
			_devSet = DAILY_DEVIATION_CHANNEL_TAG
		elif _typeCamp=='WEEKLY':
			_devSet = WEEKLY_DEVIATION_CHANNEL_TAG
	
		_devTitle = ""
		_devNumber_1s = dict({})
		_devNumber_3s = dict({})
		for _key in _devSet:
			_devTitle += "%14s (H,M,L)" % _key
			_devNumber_1s.update({_key:{'H':0, 'M':0, 'L':0}})
			_devNumber_3s.update({_key:{'H':0, 'M':0, 'L':0}})
		
		if self.verbose["high"]:
			print "-"*175
			print "       subPart   FT  Slot   Ch   CL  isBad  %s" % _devTitle
			print "-"*175
	
		_loopChan = 1
		for _ch in _channels:
			_subPart = _ch['sub_sub_partition']
			#if _ch['is_bad']==0:
			_devList = ""
			_catch = False
			for _key in _devSet:
				_devStr = ""
				for _gain in GAIN_SHORT_TAG:
					_devItem = 'deviation%d_%s' % (_devSet[_key], _gain.lower())
					_devValue = "_"
					if _ch[_devItem] != None:
						_thresholdID = THRESHOLDS_ORDER[_key]
						_threshold = THRESHOLDS_LEVEL[_subPart][_gain][_thresholdID]
						_devNorm = _ch[_devItem]/_threshold
					
						if abs(_devNorm)>=1 and abs(_devNorm)<3:
							_devNumber_1s[_key][_gain] += 1
						if abs(_devNorm)>=3:
							_devNumber_3s[_key][_gain] += 1
					
						_devValue = "%6.3f" % _devNorm
						_catch = True
					
					if "L" in _gain: 
						_devStr += _devValue
					else: _devStr += _devValue + ','
			
				_devList += "%22s" % _devStr
		
			if _catch and self.verbose["high"]:
				print "%6d %7s %4d %5d %4d %4d %6d %s" \
					% (_loopChan, _subPart, _ch['ft'], _ch['sl'], _ch['ch'], _ch['cl'], _ch['is_bad'], _devList)
				_loopChan += 1
			
			if self.verbose["high"]:
				if (_loopChan%25)==0 and _catch:
					print "-"*175
					print "       subPart   FT  Slot   Ch   CL  isBad %s" % _devTitle
					print "-"*175
	
		# Summary
		if self.verbose["high"]: print "-"*175
		#print "="*175; _sumTitle = " "*43 + _devTitle
		#print _sumTitle; print "-"*175
		_summary = ""
		_totDevNumber_1s = 0
		_totDevNumber_3s = 0
		for _key in _devSet:
			#_keySummary = ""
			for _gain in GAIN_SHORT_TAG:
				if _devNumber_1s[_key][_gain]>0 or _devNumber_3s[_key][_gain]>0:
					#_keySummary += "%3d(%3d)" % (_devNumber_1s[_key][_gain], _devNumber_3s[_key][_gain])
					_totDevNumber_1s += _devNumber_1s[_key][_gain]          
					_totDevNumber_3s += _devNumber_3s[_key][_gain]          
				#else:
					#_keySummary += "_"
				#if "L" not in _gain: 
					#_keySummary += ','
			#_summary += "%22s" % _keySummary
	
		#_offset = " "*7
		
		_criterion_1sigma = __import__('validation_config_warning').CRITERION_1_SIGMA
		_criterion_3sigma = __import__('validation_config_warning').CRITERION_3_SIGMA
		
		print MED_INDENT,"Number deviation > 1 sigma: %3d" % _totDevNumber_1s
		for _key in _devSet:
			_summary = "%8s: " % _key
			for _gain in GAIN_SHORT_TAG:
				if _devNumber_1s[_key][_gain]>0:
					_summary += "%s %3d" % (_gain, _devNumber_1s[_key][_gain])
				else:
					_summary += "%s   -" % _gain
				if _gain!='L':
					_summary += ', '
			print BIG_INDENT,"%s" % _summary
		_status_1s = 'FATAL'
		for _criterion in _criterion_1sigma:
			if _totDevNumber_1s<=_criterion_1sigma[_criterion]:
				_status_1s = _criterion
		print MED_INDENT,"Status 1 sigma: %s" % _status_1s
			
		print ""
		print MED_INDENT,"Number deviation > 3 sigma: %3d" % _totDevNumber_3s
		for _key in _devSet:
			_summary = "%8s: " % _key
			for _gain in GAIN_SHORT_TAG:
				if _devNumber_3s[_key][_gain]>0:
					_summary += "%s %3d" % (_gain, _devNumber_3s[_key][_gain])
				else:
					_summary += "%s   -" % _gain
				if _gain!='L':
					_summary += ', '
			print BIG_INDENT,"%s" % _summary
		_status_3s = 'FATAL'
		for _criterion in _criterion_3sigma:
			if _totDevNumber_3s<=_criterion_3sigma[_criterion]:
				_status_3s = _criterion
		print MED_INDENT,"Status 3 sigma: %s" % _status_3s
			
		
		#_summary = "%snDev>1sig: %4d    (nDev>3sig: %3d) %s" % (_offset, _totDevNumber_1s, _totDevNumber_3s, _summary)
		#print "%s\n" % _summary

	
	
	







