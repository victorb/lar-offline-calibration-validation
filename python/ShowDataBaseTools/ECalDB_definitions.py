
# BadChannelHunter (BCH) programm setting
#BCH_PATH = '/afs/cern.ch/user/v/victorb/Development/bin'
#BCH_PROG = 'badChHunter'
#BCH_WWW_PATH = '/afs/cern.ch/user/l/larcalib/www/BadChanHunter'

# Few variables
#DB_PATH = '/home/victorb/ECalTeam/ECalDBLib'
#DB_PATH = '/afs/cern.ch/user/l/larcalib/public/Validation_run/ver_2/DataBase'
#DB_NAME_PATTERN = 'ECalDB_'

## Variables for analysis: <=
#CRITERION_1_SIGMA = {"OK":10, "WARNING":20}
#CRITERION_3_SIGMA = {"OK":5, "WARNING":10}

DB_NUM_DEV_CHANNEL = 10
DB_NUM_DEV_FEB = 9

EOS_BASE_PATH = '/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/Auto'
EOS_ROOT_PATH = '/rootFiles'

# Bad channel flags
BAD_CHAN_FLAG = dict({
	160: 'unstable/lowNoiseHG'
	})

# Deviation label
DEVIATION_CHANNEL_TAG = dict({
	'AutoCorr': 1,
	'TimeFEB':  2,
	'Ped':      3,
	'Noise':    4,
	'Ramps':    5,
	'n/a':      6,
	'Delay':    7,
	'n/a':      8,
	'PeakTime': 9,
	'n/a':     10
	})
DAILY_DEVIATION_CHANNEL_TAG = dict({
	'AutoCorr': 1,
	'Ped':      3,
	'Ramps':    5,
	'Noise':    4
	})
WEEKLY_DEVIATION_CHANNEL_TAG = dict({
	'AutoCorr': 1,
	'Ped':      3,
	'Noise':    4,
	'Ramps':    5,
	'Delay':    7,
	'PeakTime': 9,
	})

# Run types
RUN_TYPE_PEDESTAL = 'PEDESTAL'
RUN_TYPE_DELAY    = 'DELAY'
RUN_TYPE_RAMP     = 'RAMP'

# RootFilesTag
RUN_CALIB_TAG = dict({
	'PEDESTAL': 'LArPedAutoCorr', 
	'DELAY':    'LArCaliWave', 
	'RAMP':     'LArRamp',
	'AUTOCORR': 'LArPedAutoCorr', 
	'PEAKTIME': 'LArCaliWave', 
	'FEBTIME':  'LArOFCPhys', 
	'OFC':      'LArOFCPhys'
	})

GAIN_TAG = ['HIGH', 'MEDIUM', 'LOW']
GAIN_SHORT_TAG = ['H', 'M', 'L']

SUB_PART = ['EMBA', 'EMBC', 'EMBPS', 'EMECA', 'EMECC', 'HEC', 'FCAL']

SUB_PART_LAYERS = dict({
	'EMBA':  [('1','', 'FRONT'), ('2','', 'MIDDLE'), ('3','-p', 'BACK')],
	'EMBC':  [('1','', 'FRONT'), ('2','', 'MIDDLE'), ('3','-p', 'BACK')],
	'EMBPS': [('0','-p', 'PS')],
	'EMECA': [('0','', 'PS'), ('1','', 'FRONT'), ('2','', 'MIDDLE'), ('3','-p', 'BACK')],
	'EMECC': [('0','', 'PS'), ('1','', 'FRONT'), ('2','', 'MIDDLE'), ('3','-p', 'BACK')],
	'HEC':   [('0','-p', 'HEC1_first'), ('1','', 'HEC1_second'), ('2','', 'HEC2_first'), ('3','', 'HEC2_second')],
	'FCAL':  [('1','-p', 'FCAL1'), ('2','', 'FCAL2'), ('3','', 'FCAL3')]
	})

# Sub partition feature
FCAL_FT  = [6]
HEC_FT   = [3, 10, 16, 22]
HEC_SLOT = [5, 6, 7, 8, 9, 10]




