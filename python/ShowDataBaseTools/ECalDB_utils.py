
import datetime, commands, sys

# ----- Locale ------
from ECalDB_definitions import *
from ECalDB_thresholds import *

def SortByFtSlotCh(_ch):
	return 5000*_ch['ft'] + 128*_ch['sl'] + _ch['ch']

#--------------------------------------------------------
# BuildDict
#--------------------------------------------------------
def BuildDict(_array1, _array2):
	if _array1 is None:
		return []
	_keys = []
	_dict = []
	for _a1 in _array1:
		_keys.append(_a1[0])
	for _a2 in _array2:
		_dict.append(dict(zip(_keys, _a2)))
	return _dict

#--------------------------------------------------------
# GetEosListDir
#--------------------------------------------------------
def GetEosListDir(_path):
	return [_dir for _dir in commands.getoutput('/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin/eos.select ls ' + _path + '/').split('\n')]
     

#--------------------------------------------------------
# GetDate
#--------------------------------------------------------
def GetDateHuman(_timestamp):
	return datetime.datetime.fromtimestamp(_timestamp).strftime('%d/%m/%Y')
def GetDate(_timestamp):
	return datetime.datetime.fromtimestamp(_timestamp).strftime('%d%m%y')

#--------------------------------------------------------
# GetDateTime
#--------------------------------------------------------
def GetDateTime(_timestamp):
	return datetime.datetime.fromtimestamp(_timestamp).strftime('%d/%m/%Y %H:%M:%S')

	
#------------------------------------------------------------------------
# PrintInfoToFile
#------------------------------------------------------------------------
def PrintInfoToFile(_lCamp, _lRuns, _nFile):
	_outFile = open(_nFile, 'w')
	
	# a) Campaign part
	_outFile.write("\n Campaign info\n"); _outFile.write(""); _outFile.write("="*100); _outFile.write("\n")
	_outFile.write("      refCamp idCamp typeCamp             dateCamp  toroid solenoid     flagDQ      flag      db\n")
	_outFile.write("="*100); _outFile.write("\n")
	_loopCamp = 1
	for _camp in _lCamp:
		_outFile.write("{0:>5d} {1:>7d} {2:>6d} {3:>8s} {4:>20s}  {5:>6s} {6:>8s}  {7:>9s} {8:>9s} {9:>7s}\n". \
			format(_loopCamp, _camp['ref'], _camp['id'], _camp['type'], GetDateTime(_camp['date']), \
				_camp['toroid_status'], _camp['solenoid_status'], \
				_camp['dq_flag'], _camp['flag'], _camp['db']))
		_loopCamp += 1
	_outFile.write("\n\n\n")
	
	# b) Runs part
	_outFile.write(" Runs info\n"); _outFile.write("-"*125); _outFile.write("\n")
	_outFile.write("      idRun            dateStart             dateStop      type  partition  gain    toroid solenoid     flagDQ    flag\n")
	_outFile.write("-"*125); _outFile.write("\n")
	_loopRun = 1
	for _run in _lRuns:
		_outFile.write("{0:>3d}  {1:7>d} {2:>20s} {3:>20s} {4:>9s} {5:>10s} {6:>5s} {7:>9.2f} {8:>8.2f} {9:>10s} {10:>7s}\n". \
			format(_loopRun, _run['id'], GetDateTime(_run['date_start']), GetDateTime(_run['date_stop']), \
				_run['type'], _run['partition'], _run['gain'], \
				_run['toroid'], _run['solenoid'], _run['dq_flag'], _run['flag']))
		_loopRun += 1
	
	_outFile.write("\n")
	_outFile.close()
		
		
#------------------------------------------------------------------------
# PrintDirs
#------------------------------------------------------------------------
def PrintDirs(_dirs):
	print(""); print("-"*100); 
	print("      campaign  directory {0:>65s}".format("calibType")); 
	print("-"*100)
	_loop = 1
	for _dir in _dirs:
		print("{0:>5d} {1:>8d}  {2:<65s} {3:s}".format(_loop, _dir['campaign'], _dir['name'], _dir['type']))
		_loop += 1
		
#------------------------------------------------------------------------
# PrintRootFiles
#------------------------------------------------------------------------
def PrintRootFiles(_files):
	pass
	#print(""); print("-"*100)
	#print("      dir")
	#print("-"*100)
	#_loop = 1
	#for _gain in _files:
		#for _file in _gain:
			#print("{0:>5d} {1:s} {2:s} {3:s} {4:s} {5:s}". \
				#format(_loop, _file['name'], _file['subPart'], _file['gain'], _file['typeRun'], _file['path']))
			#_loop += 1
		#print("")

































