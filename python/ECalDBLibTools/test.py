#!/usr/bin/env python

#############################################################################
# Authors : Jerome ODIER, jerome.odier@cern.ch
#         : Julien MOREL, julien.morel@cern.ch
#
# Version : 1.0
#
# This file is part of 'ECalDBLib'
#
#############################################################################

import time

import ECalDBLib

#############################################################################
# :memory: #
#############################################################################

if __name__ == "__main__":
	ecalDB = ECalDBLib.ECalDBLib()

	ecalDB.Open('sqlite://./test.db')

	print("* CAMPAIGN *")
	ecalDB.CampaignFormat(ecalDB.Execute('SELECT * FROM Campaign'))

	print("* RUN *")
	ecalDB.RunFormat(ecalDB.Execute('SELECT * FROM Run'))

	print("* CHANNEL *")
	ecalDB.ChannelFormat(ecalDB.Execute("SELECT * FROM Channel WHERE sub_sub_partition='EMBA' ORDER BY ft,sl,ch"))
	ecalDB.ChannelFormat(ecalDB.Execute("SELECT * FROM Channel WHERE sub_sub_partition='HECA' ORDER BY ft,sl,ch"))

## 	print("* FEB *")
## 	ecalDB.FebFormat(ecalDB.Execute("SELECT * FROM Feb WHERE sub_sub_partition='EMBA'"))

	ecalDB.Commit()
	ecalDB.Close()

#############################################################################

