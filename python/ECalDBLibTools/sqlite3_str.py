#############################################################################
# Authors : Jerome ODIER, jerome.odier@cern.ch
#         : Julien MOREL, julien.morel@cern.ch
#
# Version : 1.0
#
# This file is part of 'ECalDBLib'
#
#############################################################################

CreateTableCampaign = '''
CREATE TABLE "Campaign"(
  "id" INTEGER PRIMARY KEY NOT NULL CHECK("id">=0),
  "ref" INTEGER NOT NULL CHECK("ref">=0),
  "date" DATETIME NOT NULL,
  "type" TEXT NOT NULL CHECK("type" IN('DAILY','WEEKLY')),
  "ntuple" VARCHAR(45) NOT NULL,
  "toroid_status" TEXT NOT NULL CHECK("toroid_status" IN('ON','MIDDLE','OFF')),
  "solenoid_status" TEXT NOT NULL CHECK("solenoid_status" IN('ON','MIDDLE','OFF')),
  "composite" TEXT CHECK("composite" IN('YES','NO')),
  "dq_flag" TEXT NOT NULL CHECK("dq_flag" IN('UNDEFINED','GREEN','YELLOW','RED')),
  "flag" TEXT NOT NULL CHECK("flag" IN('UNDEFINED','GREEN','YELLOW','RED')),
  "db" TEXT NOT NULL CHECK("db" IN('CURRENT','YES','NO')),
  "comment" TEXT
);
''';

IndexTableCampaign = '''
CREATE INDEX campaign_index ON campaign(id,
ref,
date,
type,
ntuple,
toroid_status,
solenoid_status,
composite,
dq_flag,
flag,
db,
comment);
''';

#############################################################################

CreateTableRun = '''
CREATE TABLE "Run"(
  "id" INTEGER PRIMARY KEY NOT NULL CHECK("id">=0),
  "campaign" INTEGER NOT NULL CHECK("campaign">=0),
  "date_start" DATETIME NOT NULL,
  "date_stop" DATETIME NOT NULL,
  "type" TEXT NOT NULL CHECK("type" IN('PEDESTAL','RAMP','DELAY')),
  "partition" TEXT NOT NULL CHECK("partition" IN('ALL','EM','EMBPS','HECFCAL')),
  "gain" TEXT NOT NULL CHECK("gain" IN('H','M','L')),
  "toroid" FLOAT4 NOT NULL,
  "solenoid" FLOAT4 NOT NULL,
  "dq_flag" TEXT NOT NULL CHECK("dq_flag" IN('UNDEFINED','GREEN','YELLOW','RED')),
  "flag" TEXT NOT NULL CHECK("flag" IN('UNDEFINED','GREEN','YELLOW','RED')),
  CONSTRAINT "fk_Run_1"
    FOREIGN KEY("campaign")
    REFERENCES "Campaign"("id")
);
''';

IndexTableRun = '''
CREATE INDEX run_index ON run(id,
campaign,
date_start,
date_stop,
type,
partition,
gain,
toroid,
solenoid,
dq_flag,
flag);
''';
#############################################################################

CreateTableChannel = '''
CREATE TABLE "Channel"(
  "campaign" INTEGER NOT NULL CHECK("campaign">=0),
  "sub_sub_partition" TEXT NOT NULL CHECK("sub_sub_partition" IN('EMBA','EMBC','EMECA','EMECC','EMBPSA','EMBPSC','EMECPSA','EMECPSC','FCALA','FCALC','HECA','HECC')),
  "ft" INTEGER NOT NULL CHECK("ft">=0),
  "sl" INTEGER NOT NULL CHECK("sl">=0),
  "ch" INTEGER NOT NULL CHECK("ch">=0),
  "cl" INTEGER NOT NULL CHECK("cl">=0 OR "cl"==-999),
  "is_bad" INTEGER NOT NULL CHECK("is_bad">=0),
  "deviation1_h" FLOAT8,
  "deviation2_h" FLOAT8,
  "deviation3_h" FLOAT8,
  "deviation4_h" FLOAT8,
  "deviation5_h" FLOAT8,
  "deviation6_h" FLOAT8,
  "deviation7_h" FLOAT8,
  "deviation8_h" FLOAT8,
  "deviation9_h" FLOAT8,
  "deviation10_h" FLOAT8,
  "deviation1_m" FLOAT8,
  "deviation2_m" FLOAT8,
  "deviation3_m" FLOAT8,
  "deviation4_m" FLOAT8,
  "deviation5_m" FLOAT8, 
  "deviation6_m" FLOAT8,
  "deviation7_m" FLOAT8,
  "deviation8_m" FLOAT8,
  "deviation9_m" FLOAT8,
  "deviation10_m" FLOAT8,
  "deviation1_l" FLOAT8,
  "deviation2_l" FLOAT8,
  "deviation3_l" FLOAT8,
  "deviation4_l" FLOAT8,
  "deviation5_l" FLOAT8,
  "deviation6_l" FLOAT8,
  "deviation7_l" FLOAT8,
  "deviation8_l" FLOAT8,
  "deviation9_l" FLOAT8,
  "deviation10_l" FLOAT8,
  "comment" TEXT,
  PRIMARY KEY("campaign","sub_sub_partition","ft","sl","ch"),
  CONSTRAINT "fk_Channel_1"
    FOREIGN KEY("campaign")
    REFERENCES "Campaign"("id")
);
''';

IndexTableChannel = '''
CREATE INDEX channel_index ON channel(campaign,
sub_sub_partition,
ft,
sl,
ch,
cl,
is_bad,
deviation1_h,
deviation2_h,
deviation3_h,
deviation4_h,
deviation5_h,
deviation6_h,
deviation7_h,
deviation8_h,
deviation1_m,
deviation2_m,
deviation3_m,
deviation4_m,
deviation5_m,
deviation6_m,
deviation7_m,
deviation8_m,
deviation1_l,
deviation2_l,
deviation3_l,
deviation4_l,
deviation5_l,
deviation6_l,
deviation7_l,
deviation8_l,
comment,
deviation9_h,
deviation9_m,
deviation9_l,
deviation10_l,
deviation10_m,
deviation10_h);
''';

#############################################################################

CreateTableFeb = '''
CREATE TABLE "Feb"(
  "campaign" INTEGER NOT NULL CHECK("campaign">=0),
  "sub_sub_partition" TEXT NOT NULL CHECK("sub_sub_partition" IN('EMBA','EMBC','EMECA','EMECC','EMBPSA','EMBPSC','EMECPSA','EMECPSC','FCALA','FCALC','HECA','HECC')),
  "ft" INTEGER NOT NULL CHECK("ft">=0),
  "sl" INTEGER NOT NULL CHECK("sl">=0),
  "energy_h" FLOAT8 NOT NULL,
  "energy_m" FLOAT8 NOT NULL,
  "energy_l" FLOAT8 NOT NULL,
  "temperature" FLOAT8 NOT NULL,
  "deviation1_h" FLOAT8,
  "deviation2_h" FLOAT8,
  "deviation3_h" FLOAT8,
  "deviation4_h" FLOAT8,
  "deviation5_h" FLOAT8,
  "deviation6_h" FLOAT8,
  "deviation7_h" FLOAT8,
  "deviation8_h" FLOAT8,
  "deviation9_h" FLOAT8,
  "deviation1_m" FLOAT8,
  "deviation2_m" FLOAT8,
  "deviation3_m" FLOAT8,
  "deviation4_m" FLOAT8,
  "deviation5_m" FLOAT8,
  "deviation6_m" FLOAT8,
  "deviation7_m" FLOAT8,
  "deviation8_m" FLOAT8,
  "deviation9_m" FLOAT8,
  "deviation1_l" FLOAT8,
  "deviation2_l" FLOAT8,
  "deviation3_l" FLOAT8,
  "deviation4_l" FLOAT8,
  "deviation5_l" FLOAT8,
  "deviation6_l" FLOAT8,
  "deviation7_l" FLOAT8,
  "deviation8_l" FLOAT8,
  "deviation9_l" FLOAT8,
  "comment" TEXT,
  PRIMARY KEY("campaign","sub_sub_partition","ft","sl"),
  CONSTRAINT "fk_Channel_1"
    FOREIGN KEY("campaign")
    REFERENCES "Campaign"("id")
);
''';

IndexTableFeb = '''
CREATE INDEX feb_index ON feb(campaign,
sub_sub_partition,
ft,
sl,
energy_h,
energy_m,
energy_l,
temperature,
deviation1_h,
deviation2_h,
deviation3_h,
deviation4_h,
deviation5_h,
deviation6_h,
deviation7_h,
deviation8_h,
deviation1_m,
deviation2_m,
deviation3_m,
deviation4_m,
deviation5_m,
deviation6_m,
deviation7_m,
deviation8_m,
deviation1_l,
deviation2_l,
deviation3_l,
deviation4_l,
deviation5_l,
deviation6_l,
deviation7_l,
deviation8_l,
comment,
deviation9_h,
deviation9_m,
deviation9_l);
''';

#############################################################################


