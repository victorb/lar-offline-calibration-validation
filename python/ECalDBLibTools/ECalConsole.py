#!/usr/bin/env python

#############################################################################
# Authors : Jerome ODIER, jerome.odier@cern.ch
#         : Julien MOREL, julien.morel@cern.ch
#
# Version : 1.0
#
# This file is part of 'ECalConsole'
#
#############################################################################

import sys
import code
import readline

import ECalDBLib

#############################################################################
# Electronic Calibration functions					    #
#############################################################################

db = ECalDBLib.ECalDBLib()

#############################################################################

descriptionList = {
	'RootFiles': '<run>, <type PEDESTAL|RAMP|DELAY>',
	'RootNtuple': '<campaign>, <partition EM|EMPS|HECFCAL>, <type PEDESTAL|RAMP|DELAY>',
	'RootFigure': '<campaign>, <partition EM|EMPS|HECFCAL>, <type PEDESTAL|RAMP|DELAY>',

	'Execute': '<SQL query>',
	'Commit': '',

	'CampaignList': '',
	'CampaignById': '<campaign>',
	'CampaignByRef': '<campaign>',
	'CampaignByDate': '<date_start YYYY-MM-DD>, <date_stop YYYY-MM-DD>',
	'CampaignByType': '<type DAILY|WEEKLY>',
	'CampaignByMagneticFieldStatus': '<toroid_status ON|OFF>, <solenoid_status ON|OFF>',
	'CampaignByDQFlag': '<status GREEN|YELLOW|RED>',
	'CampaignByFlag': '<status GREEN|YELLOW|RED>',
	'CampaignId': '',
	'ReferenceId': '<toroid_status ON|OFF>, <solenoid_status ON|OFF>',
	'UpdateCampaignDQFlag': '<campaign>, <flag GREEN|YELLOW|RED>',
	'UpdateCampaignFlag': '<campaign>, <flag GREEN|YELLOW|RED>',
	'UpdateCampaignComment': '<campaign>, <comment>',
	'UpdateChannelComment': '<campaign>, <sub_sub_partition>, <ft>, <sl>, <ch>, <comment>',
	'UpdateFebComment': '<campaign>, <sub_sub_partition>, <ft>, <sl>, <comment>',
 	'UpdateCampaignDBStatus': '<campaign>, <db_status CURRENT|YES|NO>',
        'RemoveCampaignEntryById': '<campaign>',
        'MergeCampaignsById': '[<id1>,<id2>,...]',
        'MergeCampaignsFilesById': '[<id1>,<id2>,...]',

	'RunById': '<run>',
	'RunByCampaign': '<campaign>',
	'RunByDate': '<date_start YYYY-MM-DD>, <date_stop YYYY-MM-DD>',
	'RunByType': '<type PEDESTAL|RAMP|DELAY>',
	'RunByPartition': '<partition EM|EMPS|HECFCAL>',
	'RunByGain': '<gain H|L|M>',
	'RunByDQFlag': '<status GREEN|YELLOW|RED>',
	'RunByFlag': '<status GREEN|YELLOW|RED>',
	'UpdateRunDQFlag': '<campaign>, <flag GREEN|YELLOW|RED>',
	'UpdateRunFlag': '<campaign>, <flag GREEN|YELLOW|RED>',

	'ChannelByCampaign': '<campaign>, <sub_sub_partition>',
	'ChannelByCampaignFT': '<campaign>, <sub_sub_partition>, <feed through>',
	'ChannelByCampaignSL': '<campaign>, <sub_sub_partition>, <feed through>, <slot>',
	'ChannelByCampaignCH': '<campaign>, <sub_sub_partition>, <feed through>, <slot>, <channel>',
	'CheckChannel': '<sub_sub_partition>, <ft>, <sl>, <ch>[[, <toroid_status>, <solenoid_status>]]',

	'FebByCampaign': '<campaign>, <sub_sub_partition>',
	'FebByCampaignFT': '<campaign>, <sub_sub_partition>, <feed through>',
	'FebByCampaignSL': '<campaign>, <sub_sub_partition>, <feed through>, <slot>',
	'CheckFeb': '<sub_sub_partition>, <ft>, <sl>[[, <toroid_status>, <solenoid_status>]]',

        'DrawPusleShape':'<sub_sub_partition>, <ft>, <sl> ,<ch>, <gain H|M|L>, <campaign>, <option display>',
        'DrawValidationPlots':'<sub_sub_partition>, <type PEDESTAL|RAMP|DELAY>, <gain H|M|L>, <campaign>, <option display>',
        'DrawStabilityPlots':'<sub_sub_partition>, <type PEDESTAL|RAMP|DELAY>, <gain H|M|L>, <type WEEKLY|DAILY|ALL>, <flag GREEN|YELLOW|RED>, <field ON|OFF|SAME>, <ft>, <sl>, <ch>, <option display>',
        'DrawFEBPlots':'<sub_sub_partition>, <type PEDESTAL|RAMP|DELAY>, <gain H|M|L>, <CampaingId>, <option display>',

	'GetCampaignPartition':'<Id>',
	'SuspiciousFebFinder':'<sub_sub_partition>',

	'CampaignFormat': '<item>|<list>',
	'RunFormat': '<item>|<list>',
	'ChannelFormat': '<item>|<list>',
	'FebFormat': '<item>|<list>',
	'SuspiciousFebFormat':'<SuspiciousFebList>',
    
        'ls': 'CampaignFormat(CampaignList())'    
}

#############################################################################

def help(name = None):
	L = sorted(descriptionList.keys())
	for function in L:
		if name is None:
			print function + '(' + descriptionList[function] + ')'
	if name == 'DrawStabilityPlots':
	    print  '   ',' '
	    print name + '(' + descriptionList[name] + ')'
	    print  '   ','option = '
	    print  '      ','Absolute -> Use absolute quantities computed from the NTuple'  
	    print  '      ','Relative -> Use relative quantities computed from the NTuple'
	    print  '      ','PerFeb   -> Average per FEB            '
	    print  '      ','Shift    -> Use means of validation plots' 
	    print  '      ','NoErr    -> Do not display rms on plots'
	    print  '      ','Display  -> Keep the display for command line use'
	    print  '      ','VsDate   -> Plots versus date instead of versus campaign Id'
	    print  '      ','Official -> Use official settings for the plots'
		

	print '\nsub_sub_partition := EMBA|EMBC|EMECA|EMECC|EMBPSA|EMBPSC|EMECPSA|EMECPSC|FCALA|FCALC|HECA|HECC'

#############################################################################

def GetRootFiles(run, type):
	return ECalDBLib.GetRootFiles(run, type)

def GetRootNtuple(campaign, partition, type):
	return ECalDBLib.GetRootNtuple(campaign, partition, type)

def GetRootFigure(campaign, partition, type):
	return ECalDBLib.GetRootFigure(campaign, partition, type)

#############################################################################

def Execute(query):
	return db.Execute(query)

def Commit():
	return db.Commit()

#############################################################################

def GetCampaignById(id):
	return db.GetCampaignById(id)

def GetCampaignList():
	return db.GetCampaignList()

def GetCampaignByRef(ref):
	return db.GetCampaignByRef(ref)

def GetCampaignByDate(date_start, date_stop):
	return db.GetCampaignByDate(ECalDBLib.SQLBuildDate(date_start), ECalDBLib.SQLBuildDate(date_stop))

def GetCampaignByType(type):
	return db.GetCampaignByType(type)

def GetCampaignByMagneticFieldStatus(solenoid_status, toroid_status):
	return db.GetCampaignByMagneticFieldStatus(solenoid_status, toroid_status)

def GetCampaignByDQFlag(dq_flag):
	return db.GetCampaignByDQFlag(dq_flag)

def GetCampaignByFlag(flag):
	return db.GetCampaignByFlag(flag)

def GetCampaignId():
	return db.GetCampaignId()

def GetReferenceId(solenoid_status, toroid_status):
	return db.GetReferenceId(solenoid_status, toroid_status)

def UpdateCampaignDQFlag(id, flag):
	return db.UpdateCampaignDQFlag(id, flag)

def UpdateCampaignFlag(id, flag):
	return db.UpdateCampaignFlag(id, flag)

def UpdateCampaignComment(id, comment):
	return db.UpdateCampaignComment(id, comment)

def UpdateChannelComment(id, sub_sub_partition, ft, sl, ch, comment):
	return db.UpdateChannelComment(id, sub_sub_partition, ft, sl, ch, comment)

def UpdateFebComment(id, sub_sub_partition, ft, sl, comment):
	return db.UpdateFebComment(id, sub_sub_partition, ft, sl, comment)

def UpdateCampaignDBStatus(id, status):
	return db.UpdateCampaignDBStatus(id, status)

def RemoveCampaignEntryById(id):
  return db.RemoveCampaignEntryById(id)

def MergeCampaignsById(campaigns):
  return db.MergeCampaignsById(campaigns)

def MergeCampaignsFilesById(campaigns):
  return db.MergeCampaignsFilesById(campaigns)

#############################################################################

def GetRunById(id):
	return db.GetRunById(id)

def GetRunByCampaign(campaign):
	return db.GetRunByCampaign(campaign)

def GetRunByDate(date_start, date_stop):
	return db.GetRunByDate(ECalDBLib.SQLBuildDate(date_start), ECalDBLib.SQLBuildDate(date_stop))

def GetRunByType(type):
	return db.GetRunByType(type)

def GetRunByPartition(partition):
	return db.GetRunByPartition(partition)

def GetRunByGain(gain):
	return db.GetRunByGain(gain)

def GetRunByDQFlag(dq_flag):
	return db.GetRunByDQFlag(dq_flag)

def GetRunByFlag(flag):
	return db.GetRunByFlag(flag)

def UpdateRunDQFlag(id, flag):
	return db.UpdateRunDQFlag(id, flag)

def UpdateRunFlag(id, flag):
	return db.UpdateRunFlag(id, flag)

#############################################################################

def GetChannelByCampaign(campaign, sub_sub_partition):
	return db.GetChannelByCampaign(campaign, sub_sub_partition)

def GetChannelByCampaignFT(campaign, sub_sub_partition, ft):
	return db.GetChannelByCampaignFT(campaign, sub_sub_partition, ft)

def GetChannelByCampaignSL(campaign, sub_sub_partition, ft, sl):
	return db.GetChannelByCampaignSL(campaign, sub_sub_partition, ft, sl)

def GetChannelByCampaignCH(campaign, sub_sub_partition, ft, sl, ch):
	return db.GetChannelByCampaignCH(campaign, sub_sub_partition, ft, sl, ch)

def CheckChannel(sub_sub_partition, ft, sl, ch, toroid_status = None, solenoid_status = None):
	return db.CheckChannel(sub_sub_partition, ft, sl, ch, toroid_status, solenoid_status)

#############################################################################

def GetFebByCampaign(campaign, sub_sub_partition):
	return db.GetFebByCampaign(campaign, sub_sub_partition)

def GetFebByCampaignFT():
	return db.GetFebByCampaignFT(campaign, sub_sub_partition, ft)

def GetFebByCampaignSL():
	return db.GetFebByCampaignSL(campaign, sub_sub_partition, ft, sl)

def CheckFeb(sub_sub_partition, ft, sl, toroid_status = None, solenoid_status = None):
	return db.CheckFeb(sub_sub_partition, ft, sl, toroid_status, solenoid_status)

#############################################################################

def DrawPusleShape(sub_sub_partition, ft, sl ,ch, gain, campaignId=0, option=''):
    return db.DrawPusleShape(sub_sub_partition, ft, sl ,ch, gain, campaignId, option)

def DrawValidationPlots(sub_sub_partition, type, gain, campaignId=0, option=''):
    return db.DrawValidationPlots(sub_sub_partition, type, gain, campaignId, option)

def DrawStabilityPlots(sub_sub_partition, type, gain, campType='ALL', flag='GREEN', field='ALL', FT=-1, slot=-1, channel=-1, option='', idMin=-1, idMax=-1):
    return db.DrawStabilityPlots(sub_sub_partition, type, gain, campType, flag, field, FT, slot, channel, option, idMin, idMax)

def DrawFEBPlots(sub_sub_partition, type, gain, CampaignId, option=''):
    return db.DrawFEBPlots(sub_sub_partition, type, gain, CampaignId, option)

def GetCampaignPartition(Id):
    return db.GetCampaignPartition(Id)	

def SuspiciousFebFinder(sub_sub_partition, nDevMin=0, flag='RED'):
    return db.SuspiciousFebFinder(sub_sub_partition, nDevMin, flag)	

#############################################################################

def CampaignFormat(data):
	db.CampaignFormat(data)

def RunFormat(data):
	db.RunFormat(data)

def ChannelFormat(data):
	db.ChannelFormat(data)

def FebFormat(data):
	db.FebFormat(data)

def SuspiciousFebFormat(data):
        db.SuspiciousFebFormat(data)	

#############################################################################
def ls():
    return db.CampaignFormat(db.GetCampaignList())

#############################################################################

pointerList = {
	'db': db,

	'ls': ls,

	'help': help,

	'RootFiles': GetRootFiles,
	'RootNtuple': GetRootNtuple,
	'RootFigure': GetRootFigure,

	'Execute': Execute,
	'Commit': Commit,

	'CampaignById': GetCampaignById,
	'CampaignList': GetCampaignList,
	'CampaignByRef': GetCampaignByRef,
	'CampaignByDate': GetCampaignByDate,
	'CampaignByType': GetCampaignByType,
	'CampaignByMagneticFieldStatus': GetCampaignByMagneticFieldStatus,
	'CampaignByDQFlag': GetCampaignByDQFlag,
	'CampaignByFlag': GetCampaignByFlag,
	'CampaignId': GetCampaignId,
	'ReferenceId': GetReferenceId,
	'UpdateCampaignDQFlag<': UpdateCampaignDQFlag,
	'UpdateCampaignFlag': UpdateCampaignFlag,
	'UpdateCampaignComment': UpdateCampaignComment,
	'UpdateChannelComment': UpdateChannelComment,
	'UpdateFebComment': UpdateFebComment,
	'UpdateCampaignDBStatus': UpdateCampaignDBStatus,
        'RemoveCampaignEntryById': RemoveCampaignEntryById,
        'MergeCampaignsById': MergeCampaignsById,
        'MergeCampaignsFilesById': MergeCampaignsFilesById,

	'RunById': GetRunById,
	'RunByCampaign': GetRunByCampaign,
	'RunByDate': GetRunByDate,
	'RunByType': GetRunByType,
	'RunByPartition': GetRunByPartition,
	'RunByGain': GetRunByGain,
	'RunByDQFlag': GetRunByDQFlag,
	'RunByFlag': GetRunByFlag,
	'UpdateRunDQFlag': UpdateRunDQFlag,
	'UpdateRunFlag': UpdateRunFlag,

	'ChannelByCampaign': GetChannelByCampaign,
	'ChannelByCampaignFT': GetChannelByCampaignFT,
	'ChannelByCampaignSL': GetChannelByCampaignSL,
	'ChannelByRCampaignCH': GetChannelByCampaignCH,
	'CheckChannel': CheckChannel,

	'FebByCampaign': GetFebByCampaign,
	'FebByCampaignFT': GetFebByCampaignFT,
	'FebByCampaignSL': GetFebByCampaignSL,
	'CheckFeb': CheckFeb,

        'DrawPusleShape':DrawPusleShape,
	'DrawValidationPlots':DrawValidationPlots,
	'DrawStabilityPlots':DrawStabilityPlots,
	'DrawFEBPlots':DrawFEBPlots,
	
	'GetCampaignPartition':GetCampaignPartition,
	'SuspiciousFebFinder':SuspiciousFebFinder,

	'CampaignFormat': CampaignFormat,
	'RunFormat': RunFormat,
	'ChannelFormat': ChannelFormat,
	'FebFormat': FebFormat,
	'SuspiciousFebFormat':SuspiciousFebFormat,
}

#############################################################################
# CLASS ECalConsole							    #
#############################################################################

def complete(text, state):
	try:
		 return [entry for entry in descriptionList.keys() if entry.startswith(text)][state]

	except IndexError, e:
		 return None

#############################################################################

class ECalConsole:

#############################################################################

	def __init__(self):
	    self.console = code.InteractiveConsole(pointerList)

	    readline.parse_and_bind("tab: complete")
	    readline.set_completer(complete)

#############################################################################

	def loop(self, addr):
		try:
			db.Open(addr)
			
			print 'Welcome to the Electronic Calibration DB Console (beta)'
			print 'Please repport bug to: jerome.odier@cern.ch'
			print '                       julien.morel@cern.ch'
			print ''
			print 'help(), help(<function name>), Ctrl+D to quit'
			print ''

			try:
				self.console.push('import ROOT')

				while True:
					self.console.push(raw_input('>>> '))

			finally:
				db.Close()

		except Exception, e:
			print(e)

#############################################################################
#############################################################################

if __name__ == '__main__':
	if len(sys.argv) == 2:
	    addr = sys.argv[1]
	else:
	    addr = 'sqlite://./test.db'
	    
	if not '://' in addr:
	    print 'Use sqlite protocol'
	    addr = 'sqlite://'+addr
	
	console = ECalConsole()

	console.loop(addr)

#############################################################################
#############################################################################

