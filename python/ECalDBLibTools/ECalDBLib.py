############################################################################
# Authors : Jerome ODIER, jerome.odier@cern.ch
#         : Julien MOREL, julien.morel@cern.ch
#
# Version : 1.0
#
# This file is part of 'ECalDBLib'
#
#############################################################################
import os, sys, time, ctypes
import ROOT, uuid, commands, Messages
import AtlasStyle
import sqlite3, sqlite3_str
#import cx_Oracle, cx_Oracle_str
#import DQFlags
import re

#############################################################################
afsBasePath = '/afs/cern.ch/user/l/larcalib/w0/data/WorkingDirectory'
#casBasePath = '/castor/cern.ch/grid/atlas/caf/atlcal/perm/lar/ElecCalib/Auto'
casBasePath = '/eos/atlas/atlascerngroupdisk/det-larg/ElecCalib/Auto'


#############################################################################
fileTag = dict([('PEDESTAL', 'LArPedAutoCorr'), ('AUTOCORR', 'LArPedAutoCorr'), ('RAMP', 'LArRamp'), ('DELAY', 'LArCaliWave'), ('PEAKTIME', 'LArCaliWave') , ('FEBTIME', 'LArOFCPhys') , ('OFC', 'LArOFCPhys')])

#############################################################################
#CHANGE for ALL pedestals
listOfPart = ['ALL','EM','EMBPS','HECFCAL']

listOfSubPart = {}
#CHANGE for ALL pedestals
listOfSubPart['ALL']       = ['EMB','EMEC','EMB','EMEC','EMBPS','HEC','FCAL']
listOfSubPart['EM']       = ['EMB','EMEC']
listOfSubPart['EMBPS']    = ['EMBPS']
listOfSubPart['HECFCAL']  = ['HEC','FCAL']

listOfSubSubPart = {}
listOfSubSubPart['EMB']   = ['EMBA','EMBC']
listOfSubSubPart['EMEC']  = ['EMECA','EMECC','EMECPSA','EMECPSC']
listOfSubSubPart['EMBPS'] = ['EMBPSA','EMBPSC']
listOfSubSubPart['FCAL']  = ['FCALA','FCALC']
listOfSubSubPart['HEC']   = ['HECA','HECC']

typeRun = {}
typeRun['DELAY']         = 'DELAY'
typeRun['corrUndoDELAY'] = 'DELAY'
typeRun['RAMP']          = 'RAMP'
typeRun['corrUndoRAMP']  = 'RAMP'
typeRun['PEDESTAL']      = 'PEDESTAL'
typeRun['NOISE']         = 'PEDESTAL'
typeRun['AUTOCORR']      = 'PEDESTAL'

#############################################################################
def subSubPartSelector(subSubPart):
    """return true if barrel_ec and pos_neg correspond to subPart
    """
    if subSubPart=='EMBA'   : return '(barrel_ec==0 && pos_neg==1 && slot!=1)'
    if subSubPart=='EMBC'   : return '(barrel_ec==0 && pos_neg==0 && slot!=1)'

    if subSubPart=='EMECA'  : return '(barrel_ec==1 && pos_neg==1 && slot!=1)'
    if subSubPart=='EMECC'  : return '(barrel_ec==1 && pos_neg==0 && slot!=1)'

    if subSubPart=='EMBPSA' : return '(barrel_ec==0 && pos_neg==1 && slot==1)'
    if subSubPart=='EMBPSC' : return '(barrel_ec==0 && pos_neg==0 && slot==1)'

    if subSubPart=='EMECPSA': return '(barrel_ec==1 && pos_neg==1 && slot==1)'
    if subSubPart=='EMECPSC': return '(barrel_ec==1 && pos_neg==0 && slot==1)'

    if subSubPart=='FCALA'  : return '(barrel_ec==1 && pos_neg==1)'
    if subSubPart=='FCALC'  : return '(barrel_ec==1 && pos_neg==0)'

    if subSubPart=='HECA'   : return '(barrel_ec==1 && pos_neg==1)'
    if subSubPart=='HECC'   : return '(barrel_ec==1 && pos_neg==0)'

    return false

#############################################################################

if os.environ.has_key('ECalPlot'):
        resBasePath = os.environ['ECalPlot']
else:
        resBasePath = './'

#############################################################################

if os.environ.has_key('ECalDB'):
        dbBasePath = os.environ['ECalDB']
else:
        dbBasePath = './'

#############################################################################
def split(s, splitters):
        for splitter in splitters:
                s = s.replace(splitter, '|')

        return s.split('|')

#############################################################################

def ns_listdir(path):
        #return [dir for dir in commands.getoutput('nsls ' + path + '/').split('\n')]
        return [dir for dir in commands.getoutput('/usr/bin/eos ls ' + path + '/').split('\n')]

#############################################################################

def GetRootFiles(runNumber, type):
        result1 = [] # DATA
        result2 = [] # DQMF

        rootFileSubDir1 = '/rootFiles/' # DATA
        rootFileSubDir2 =   '/post/'    # DQMF

        fileTag = dict([('PEDESTAL', 'LArPedAutoCorr'), ('AUTOCORR', 'LArPedAutoCorr'), ('RAMP', 'LArRamp'), ('DELAY', 'LArCaliWave'), ('PEAKTIME', 'LArCaliWave') , ('FEBTIME', 'LArOFCPhys') , ('OFC', 'LArOFCPhys')])

        #print "GetRootFiles runNumber: ",runNumber," type: ",type
        ##### Check on AFS ####
        for subDir1 in os.listdir(afsBasePath):
                if str(runNumber) in subDir1:
                    if os.path.exists(afsBasePath + '/' + subDir1 + rootFileSubDir1):
                        for subDir2 in os.listdir(afsBasePath + '/' + subDir1 + rootFileSubDir1):
                            if type=='OFC':
                                if not ('RootHistos' in subDir2) and '_Picked' in subDir2 and '4samples' in subDir2 and fileTag[type] in subDir2:
                                    result1.append(afsBasePath + '/' + subDir1 + rootFileSubDir1 + subDir2)
                            else:
                                if not ('RootHistos' in subDir2) and not('_Picked' in subDir2) and fileTag[type] in subDir2:
                                    # CHANGE for ALL pedestals
                                    if type=='PEDESTAL' and  ('_orig' in subDir2 or len(result1) > 0): continue
                                    result1.append(afsBasePath + '/' + subDir1 + rootFileSubDir1 + subDir2)


                    if os.path.exists(afsBasePath + '/' + subDir1 + rootFileSubDir2):
                        for subDir2 in os.listdir(afsBasePath + '/' + subDir1 + rootFileSubDir2):
                                if '_han' in subDir2 and fileTag[type] in subDir2:
                                        # CHANGE for ALL pedestals
                                        if type=='PEDESTAL' and  ('_orig' in subDir2 or len(result2) > 0): continue
                                        result2.append(afsBasePath + '/' + subDir1 + rootFileSubDir2 + subDir2)

        #print "result1: ",result1," result2: ",result2
        if result1 != [] and result2 != []:
                return result1, result2
        elif result1 != []:
                return result1, []


        #### Check on Castor ####
        for subDir1 in ns_listdir(casBasePath):
                if str(runNumber) in subDir1:
                    #print casBasePath + '/' + subDir1 + rootFileSubDir1
                    for subDir2 in ns_listdir(casBasePath + '/' + subDir1 + rootFileSubDir1):
                        if type=='OFC':
                            if not ('RootHistos' in subDir2) and '_Picked' in subDir2 and '4samples' in subDir2 and fileTag[type] in subDir2:
                                #result1.append('rfio://' + casBasePath + '/' + subDir1 + rootFileSubDir1 + subDir2)
                                result1.append('root://eosatlas/' + casBasePath + '/' + subDir1 + rootFileSubDir1 + subDir2)
                        else:
                            if not ('RootHistos' in subDir2) and not('_Picked' in subDir2) and fileTag[type] in subDir2:
                                #result1.append('rfio://' + casBasePath + '/' + subDir1 + rootFileSubDir1 + subDir2)
                                result1.append('root://eosatlas/' + casBasePath + '/' + subDir1 + rootFileSubDir1 + subDir2)

                    for subDir2 in ns_listdir(casBasePath + '/' + subDir1 + rootFileSubDir2):
                                if '_han' in subDir2 and fileTag[type] in subDir2:
                                        #result2.append('rfio://' + casBasePath + '/' + subDir1 + rootFileSubDir2 + subDir2)
                                        result2.append('root://eosatlas/' + casBasePath + '/' + subDir1 + rootFileSubDir2 + subDir2)

        if result1 != [] and result2 != []:
                return result1, result2
        elif result1 != []:
                return result1, []

        #### Not found ####

        return [], []

#############################################################################

def GetRootNtuple(campaign, partition, type):
        file1 = '%s/Campaign_%d/Ntuple_%s_%s_H_%d.root' % (resBasePath, campaign, partition, type, campaign)
        file2 = '%s/Campaign_%d/Ntuple_%s_%s_M_%d.root' % (resBasePath, campaign, partition, type, campaign)
        file3 = '%s/Campaign_%d/Ntuple_%s_%s_L_%d.root' % (resBasePath, campaign, partition, type, campaign)

        if os.path.exists(file1):
                tree1 = ROOT.TFile.Open(file1)
        else:
                tree1 = None

        if os.path.exists(file2):
                tree2 = ROOT.TFile.Open(file2)
        else:
                tree2 = None

        if os.path.exists(file3):
                tree3 = ROOT.TFile.Open(file3)
        else:
                tree3 = None

        return dict([('H', tree1), ('M', tree2), ('L', tree3)])

#############################################################################

def GetRootFigure(campaign, partition, type):
        file1 = '%s/Campaign_%d/Figure_%s_%s_H_%d.root' % (resBasePath, campaign, partition, type, campaign)
        file2 = '%s/Campaign_%d/Figure_%s_%s_M_%d.root' % (resBasePath, campaign, partition, type, campaign)
        file3 = '%s/Campaign_%d/Figure_%s_%s_L_%d.root' % (resBasePath, campaign, partition, type, campaign)

        if os.path.exists(file1):
                tree1 = ROOT.TFile.Open(file1)
        else:
                tree1 = None

        if os.path.exists(file2):
                tree2 = ROOT.TFile.Open(file2)
        else:
                tree2 = None

        if os.path.exists(file3):
                tree3 = ROOT.TFile.Open(file3)
        else:
                tree3 = None

        return dict([('H', tree1), ('M', tree2), ('L', tree3)])


#############################################################################

def GetTmpFileName():
        result = str(uuid.uuid4())

        return [result + '.png', result + '.eps']

#############################################################################

def SQLBuildDic(array1, array2):

        if array1 is None:
                return []

        L = []
        M = []

        for N in array1:
                L.append(      N[0]     )

        for N in array2:
                M.append(dict(zip(L, N)))

        return M

#############################################################################

def SQLBuildDate(string):
        try:
                result = int(time.mktime(time.strptime(string, "%Y-%m-%d")))

        except ValueError, e:
                result = 0

                print e

        return result

#############################################################################
devDict  = {'Autocorr':'1','Delay':'7','Pedestal':'3','Noise':'4','Ramps':'5','PeakTime':'9','FEBTIME':'2'}

######################################################################################################
# Ped,Noise,PedFEB,NoiseFEB  ,  Autocorr,AutocorrFEB  ,  MaxAmpl,MaxAmplFEB  ,  RampX[0],RampX[0]FEB #
######################################################################################################

thresholdOrder = {'Pedestal':0,'Noise':1,'PedestalFEB':2,'NoiseFEB':3,'Autocorr':4,'AutocorrFEB':5,'Delay':6,'DelayFEB':7,'Ramps':8,'RampsFEB':9,'corrUndoRAMP':8,'corrUndoDELAY':6,'FebTime':10,'PeakTime':11,
                  'PEDESTAL':0,'NOISE':1,'PEDESTALFEB':2,'NOISEFEB':3,'AUTOCORR':4,'AUTOCORRFEB':5,'DELAY':6,'DELAYFEB':7,'RAMP' :8,'RAMPFEB' :9,'FEBTIME':10,'PEAKTIME':11}

#Thresholds for LS2, 2*standard threshold for pedestal, 3*standard threshold for others, to keep table clean
thresholdDict = {
        'EMBA': {
                'H': [1.8,0.6 ,0.4,0.15  ,  0.12,0.12  ,  0.030,0.015    ,  0.045,0.012   , 0.9 , 9 ],
                'M': [1.8,0.15,0.4,0.15  ,  0.12,0.12  ,  0.012,0.015    ,  0.012,0.012   , 0.6 , 9 ],
                'L': [1.8,0.15,0.4,0.15  ,  0.12,0.12  ,  0.012,0.015    ,  0.012,0.012   , 0.  , 9 ]
        },
        'EMBC': {
                'H': [1.8,0.6 ,0.4,0.15  ,  0.12,0.12  ,  0.030,0.015    ,  0.045,0.012   , 0.9 , 9 ],
                'M': [1.8,0.15,0.4,0.15  ,  0.12,0.12  ,  0.012,0.015    ,  0.012,0.012   , 0.6 , 9 ],
                'L': [1.8,0.15,0.4,0.15  ,  0.12,0.12  ,  0.012,0.015    ,  0.012,0.012   , 0.  , 9 ]
        },
        'EMECA': {
                'H': [1.8,0.6 ,3.0,3.0   ,  0.12,0.06  ,  0.0180,0.0060  ,  0.018,0.003   , 0.9 , 9 ],
                'M': [1.8,0.15,3.0,3.0   ,  0.12,0.06  ,  0.0045,0.0018  ,  0.012,0.003   , 0.6 , 9 ],
                'L': [1.8,0.15,3.0,3.0   ,  0.12,0.06  ,  0.0045,0.0018  ,  0.012,0.003   , 0.  , 9 ]
        },
        'EMECC': {
                'H': [1.8,0.6 ,3.0,3.0   ,  0.12,0.06  ,  0.0180,0.0060  ,  0.018,0.003   , 0.9 , 9 ],
                'M': [1.8,0.15,3.0,3.0   ,  0.12,0.06  ,  0.0045,0.0018  ,  0.012,0.003   , 0.6 , 9 ],
                'L': [1.8,0.15,3.0,3.0   ,  0.12,0.06  ,  0.0045,0.0018  ,  0.012,0.003   , 0.  , 9 ]
        },
        'EMBPSA': {
                'H': [1.8,0.6 ,0.4,0.15  ,  0.12,0.12  ,  0.030,0.015    ,  0.018,0.012   , 0.9 , 9 ],
                'M': [1.8,0.15,0.4,0.15  ,  0.12,0.12  ,  0.012,0.015    ,  0.006,0.012   , 0.6 , 9 ],
                'L': [1.8,0.15,0.4,0.15  ,  0.12,0.12  ,  0.012,0.015    ,  0.006,0.012   , 0.  , 9 ]
        },
        'EMBPSC': {
                'H': [1.8,0.6 ,0.4,0.15  ,  0.12,0.12  ,  0.030,0.015    ,  0.045,0.012   , 0.9 , 9 ],
                'M': [1.8,0.15,0.4,0.15  ,  0.12,0.12  ,  0.012,0.015    ,  0.018,0.012   , 0.6 , 9 ],
                'L': [1.8,0.15,0.4,0.15  ,  0.12,0.12  ,  0.012,0.015    ,  0.018,0.012   , 0.  , 9 ]
        },
        'EMECPSA': {
                'H': [1.8,0.6 ,3.0,3.0   ,  0.12,0.06  ,  0.0180,0.0060  ,  0.030,0.003   , 0.9 , 9 ],
                'M': [1.8,0.15,3.0,3.0   ,  0.12,0.06  ,  0.0045,0.0018  ,  0.030,0.003   , 0.6 , 9 ],
                'L': [1.8,0.15,3.0,3.0   ,  0.12,0.06  ,  0.0045,0.0018  ,  0.030,0.003   , 0.  , 9 ]
        },
        'EMECPSC': {
                'H': [1.8,0.6 ,3.0,3.0   ,  0.12,0.06  ,  0.0180,0.0060  ,  0.018,0.003   , 0.9 , 9 ],
                'M': [1.8,0.15,3.0,3.0   ,  0.12,0.06  ,  0.0045,0.0018  ,  0.018,0.003   , 0.6 , 9 ],
                'L': [1.8,0.15,3.0,3.0   ,  0.12,0.06  ,  0.0045,0.0018  ,  0.018,0.003   , 0.  , 9 ]
        },
        'FCALA': {
                'H': [1.8,0.6 ,3.0,3.0   ,  0.15,0.06  ,  0.015,0.009    ,  0.0120,0.003  , 0.9 , 9 ],
                'M': [1.8,0.15,3.0,3.0   ,  0.15,0.06  ,  0.003,0.009    ,  0.0045,0.003  , 0.6 , 9 ],
                'L': [1.8,0.15,3.0,3.0   ,  0.15,0.06  ,  0.003,0.009    ,  0.0045,0.003  , 0.  , 9 ]
        },
        'FCALC': {
                'H': [1.8,0.6 ,3.0,3.0   ,  0.15,0.06  ,  0.015,0.009    ,  0.0120,0.003  , 0.9 , 9 ],
                'M': [1.8,0.15,3.0,3.0   ,  0.15,0.06  ,  0.003,0.009    ,  0.0045,0.003  , 0.6 , 9 ],
                'L': [1.8,0.15,3.0,3.0   ,  0.15,0.06  ,  0.003,0.009    ,  0.0045,0.003  , 0.  , 9 ]
        },
        'HECA': {
                'H': [2.0,6.0,10.0,12.0   ,  0.12,0.06  ,  0.045,0.03     ,  0.060,0.003   , 0.9 , 9 ],
                'M': [2.0,1.8,10.0,12.0   ,  0.12,0.06  ,  0.030,0.03     ,  0.045,0.003   , 0.6 , 9 ],
                'L': [2.0,0.6,10.0,12.0   ,  0.12,0.06  ,  0.030,0.03     ,  0.045,0.003   , 0.  , 9 ]
        },
        'HECC': {
                'H': [2.0,6.0,10.0,12.0   ,  0.12,0.06  ,  0.045,0.03     ,  0.060,0.003   , 0.9 , 9 ],
                'M': [2.0,1.8,10.0,12.0   ,  0.12,0.06  ,  0.030,0.03     ,  0.045,0.003   , 0.6 , 9 ],
                'L': [2.0,0.6,10.0,12.0   ,  0.12,0.06  ,  0.030,0.03     ,  0.045,0.003   , 0.  , 9 ]
        }
}
## Thresholds updated from Jelena s study --> https://indico.cern.ch/getFile.py/access?sessionId=0&resId=0&materialId=0&confId=185386
#thresholdDict = {
#        'EMBA': {
#                'H': [0.9,0.2 ,0.2,0.05  ,  0.04,0.04  ,  0.010,0.005    ,  0.015,0.004   , 0.3 , 3 ],
#                'M': [0.9,0.05,0.2,0.05  ,  0.04,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.2 , 3 ],
#                'L': [0.9,0.05,0.2,0.05  ,  0.04,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.  , 3 ]
#        },
#        'EMBC': {
#                'H': [0.9,0.2 ,0.2,0.05  ,  0.04,0.04  ,  0.010,0.005    ,  0.015,0.004   , 0.3 , 3 ],
#                'M': [0.9,0.05,0.2,0.05  ,  0.04,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.2 , 3 ],
#                'L': [0.9,0.05,0.2,0.05  ,  0.04,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.  , 3 ]
#        },
#        'EMECA': {
#                'H': [0.9,0.2 ,1.5,1.0   ,  0.04,0.02  ,  0.0060,0.0020  ,  0.006,0.001   , 0.3 , 3 ],
#                'M': [0.9,0.05,1.5,1.0   ,  0.04,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.2 , 3 ],
#                'L': [0.9,0.05,1.5,1.0   ,  0.04,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.  , 3 ]
#        },
#        'EMECC': {
#                'H': [0.9,0.2 ,1.5,1.0   ,  0.04,0.02  ,  0.0060,0.0020  ,  0.006,0.001   , 0.3 , 3 ],
#                'M': [0.9,0.05,1.5,1.0   ,  0.04,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.2 , 3 ],
#                'L': [0.9,0.05,1.5,1.0   ,  0.04,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.  , 3 ]
#        },
#        'EMBPSA': {
#                'H': [0.9,0.2 ,0.2,0.05  ,  0.04,0.04  ,  0.010,0.005    ,  0.006,0.004   , 0.3 , 3 ],
#                'M': [0.9,0.05,0.2,0.05  ,  0.04,0.04  ,  0.004,0.005    ,  0.002,0.004   , 0.2 , 3 ],
#                'L': [0.9,0.05,0.2,0.05  ,  0.04,0.04  ,  0.004,0.005    ,  0.002,0.004   , 0.  , 3 ]
#        },
#        'EMBPSC': {
#                'H': [0.9,0.2 ,0.2,0.05  ,  0.04,0.04  ,  0.010,0.005    ,  0.015,0.004   , 0.3 , 3 ],
#                'M': [0.9,0.05,0.2,0.05  ,  0.04,0.04  ,  0.004,0.005    ,  0.006,0.004   , 0.2 , 3 ],
#                'L': [0.9,0.05,0.2,0.05  ,  0.04,0.04  ,  0.004,0.005    ,  0.006,0.004   , 0.  , 3 ]
#        },
#        'EMECPSA': {
#                'H': [0.9,0.2 ,1.5,1.0   ,  0.04,0.02  ,  0.0060,0.0020  ,  0.010,0.001   , 0.3 , 3 ],
#                'M': [0.9,0.05,1.5,1.0   ,  0.04,0.02  ,  0.0015,0.0006  ,  0.010,0.001   , 0.2 , 3 ],
#                'L': [0.9,0.05,1.5,1.0   ,  0.04,0.02  ,  0.0015,0.0006  ,  0.010,0.001   , 0.  , 3 ]
#        },
#        'EMECPSC': {
#                'H': [0.9,0.2 ,1.5,1.0   ,  0.04,0.02  ,  0.0060,0.0020  ,  0.006,0.001   , 0.3 , 3 ],
#                'M': [0.9,0.05,1.5,1.0   ,  0.04,0.02  ,  0.0015,0.0006  ,  0.006,0.001   , 0.2 , 3 ],
#                'L': [0.9,0.05,1.5,1.0   ,  0.04,0.02  ,  0.0015,0.0006  ,  0.006,0.001   , 0.  , 3 ]
#        },
#        'FCALA': {
#                'H': [0.9,0.2 ,1.5,1.0   ,  0.05,0.02  ,  0.005,0.003    ,  0.0040,0.001  , 0.3 , 3 ],
#                'M': [0.9,0.05,1.5,1.0   ,  0.05,0.02  ,  0.001,0.003    ,  0.0015,0.001  , 0.2 , 3 ],
#                'L': [0.9,0.05,1.5,1.0   ,  0.05,0.02  ,  0.001,0.003    ,  0.0015,0.001  , 0.  , 3 ]
#        },
#        'FCALC': {
#                'H': [0.9,0.2 ,1.5,1.0   ,  0.05,0.02  ,  0.005,0.003    ,  0.0040,0.001  , 0.3 , 3 ],
#                'M': [0.9,0.05,1.5,1.0   ,  0.05,0.02  ,  0.001,0.003    ,  0.0015,0.001  , 0.2 , 3 ],
#                'L': [0.9,0.05,1.5,1.0   ,  0.05,0.02  ,  0.001,0.003    ,  0.0015,0.001  , 0.  , 3 ]
#        },
#        'HECA': {
#                'H': [1.0,2.0,5.0,4.0   ,  0.04,0.02  ,  0.015,0.01     ,  0.020,0.001   , 0.3 , 3 ],
#                'M': [1.0,0.6,5.0,4.0   ,  0.04,0.02  ,  0.010,0.01     ,  0.015,0.001   , 0.2 , 3 ],
#                'L': [1.0,0.2,5.0,4.0   ,  0.04,0.02  ,  0.010,0.01     ,  0.015,0.001   , 0.  , 3 ]
#        },
#        'HECC': {
#                'H': [1.0,2.0,5.0,4.0   ,  0.04,0.02  ,  0.015,0.01     ,  0.020,0.001   , 0.3 , 3 ],
#                'M': [1.0,0.6,5.0,4.0   ,  0.04,0.02  ,  0.010,0.01     ,  0.015,0.001   , 0.2 , 3 ],
#                'L': [1.0,0.2,5.0,4.0   ,  0.04,0.02  ,  0.010,0.01     ,  0.015,0.001   , 0.  , 3 ]
#        }
#}

## Old thresholds
## thresholdDict = {
##         'EMBA': {
##                 'H': [1.5,0.8,0.2,0.05  ,  0.05,0.04  ,  0.010,0.005    ,  0.015,0.004   , 0.3 , 1.5 ],
##                 'M': [0.9,0.2,0.2,0.05  ,  0.05,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.2 , 1.5 ],
##                 'L': [0.9,0.2,0.2,0.05  ,  0.05,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.  , 1.5 ]
##         },
##         'EMBC': {
##                 'H': [1.5,0.8,0.2,0.05  ,  0.05,0.04  ,  0.010,0.005    ,  0.015,0.004   , 0.3 , 1.5 ],
##                 'M': [0.9,0.2,0.2,0.05  ,  0.05,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.2 , 1.5 ],
##                 'L': [0.9,0.2,0.2,0.05  ,  0.05,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.  , 1.5 ]
##         },
##         'EMECA': {
##                 'H': [1.5,0.8,1.5,1.0   ,  0.05,0.02  ,  0.0060,0.0020  ,  0.006,0.001   , 0.3 , 1.5 ],
##                 'M': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.2 , 1.5 ],
##                 'L': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.  , 1.5 ]
##         },
##         'EMECC': {
##                 'H': [1.5,0.8,1.5,1.0   ,  0.05,0.02  ,  0.0060,0.0020  ,  0.006,0.001   , 0.3 , 1.5 ],
##                 'M': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.2 , 1.5 ],
##                 'L': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.  , 1.5 ]
##         },
##         'EMBPSA': {
##                 'H': [1.5,0.8,0.2,0.05  ,  0.05,0.04  ,  0.010,0.005    ,  0.015,0.004   , 0.3 , 1.5 ],
##                 'M': [0.9,0.2,0.2,0.05  ,  0.05,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.2 , 1.5 ],
##                 'L': [0.9,0.2,0.2,0.05  ,  0.05,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.  , 1.5 ]
##         },
##         'EMBPSC': {
##                 'H': [1.5,0.8,0.2,0.05  ,  0.05,0.04  ,  0.010,0.005    ,  0.015,0.004   , 0.3 , 1.5 ],
##                 'M': [0.9,0.2,0.2,0.05  ,  0.05,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.2 , 1.5 ],
##                 'L': [0.9,0.2,0.2,0.05  ,  0.05,0.04  ,  0.004,0.005    ,  0.004,0.004   , 0.  , 1.5 ]
##         },
##         'EMECPSA': {
##                 'H': [1.5,0.8,1.5,1.0   ,  0.05,0.02  ,  0.0060,0.0020  ,  0.006,0.001   , 0.3 , 1.5 ],
##                 'M': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.2 , 1.5 ],
##                 'L': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.  , 1.5 ]
##         },
##         'EMECPSC': {
##                 'H': [1.5,0.8,1.5,1.0   ,  0.05,0.02  ,  0.0060,0.0020  ,  0.006,0.001   , 0.3 , 1.5 ],
##                 'M': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.2 , 1.5 ],
##                 'L': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.0015,0.0006  ,  0.004,0.001   , 0.  , 1.5 ]
##         },
##         'FCALA': {
##                 'H': [1.5,0.8,1.5,1.0   ,  0.05,0.02  ,  0.005,0.003    ,  0.0040,0.001  , 0.3 , 1.5 ],
##                 'M': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.001,0.003    ,  0.0015,0.001  , 0.2 , 1.5 ],
##                 'L': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.001,0.003    ,  0.0015,0.001  , 0.  , 1.5 ]
##         },
##         'FCALC': {
##                 'H': [1.5,0.8,1.5,1.0   ,  0.05,0.02  ,  0.005,0.003    ,  0.0040,0.001  , 0.3 , 1.5 ],
##                 'M': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.001,0.003    ,  0.0015,0.001  , 0.2 , 1.5 ],
##                 'L': [0.9,0.2,1.5,1.0   ,  0.05,0.02  ,  0.001,0.003    ,  0.0015,0.001  , 0.  , 1.5 ]
##         },
##         'HECA': {
##                 'H': [5.0,4.0,5.0,4.0   ,  0.05,0.02  ,  0.015,0.01     ,  0.020,0.001   , 0.3 , 1.5 ],
##                 'M': [1.3,0.6,5.0,4.0   ,  0.05,0.02  ,  0.010,0.01     ,  0.015,0.001   , 0.2 , 1.5 ],
##                 'L': [1.0,0.2,5.0,4.0   ,  0.05,0.02  ,  0.010,0.01     ,  0.015,0.001   , 0.  , 1.5 ]
##         },
##         'HECC': {
##                 'H': [5.0,4.0,5.0,4.0   ,  0.05,0.02  ,  0.015,0.01     ,  0.020,0.001   , 0.3 , 1.5 ],
##                 'M': [1.3,0.6,5.0,4.0   ,  0.05,0.02  ,  0.010,0.01     ,  0.015,0.001   , 0.2 , 1.5 ],
##                 'L': [1.0,0.2,5.0,4.0   ,  0.05,0.02  ,  0.010,0.01     ,  0.015,0.001   , 0.  , 1.5 ]
##         }
## }

###########################################################################
# SQLITE ABSTRACTION                                                        #
#############################################################################

class SQLiteAbstract:

#############################################################################

        def EngineName(self):
                return 'sqlite'

#############################################################################

        def Open(self, address):
                self.db_adress = address

                try:
                        self.db = sqlite3.connect(address,timeout=30)
                        self.cursor = self.db.cursor()

                except sqlite3.Error, errmsg:
                        raise Exception(str(errmsg))

#############################################################################

        def TableInfo(self, name):
                result = []

                try:
                        self.cursor.execute('pragma table_info("%s")' % name)

                        fields = self.cursor.fetchall()

                        for field in fields:
                                result.append(field[1].lower())

                except cx_Oracle.Error, errmsg:
                        print str(errmsg)

                return result

#############################################################################

        def Execute(self, query):
                result = []

                try:
                        self.cursor.execute(query)

                        result = SQLBuildDic(self.cursor.description, self.cursor.fetchall())

                except sqlite3.Error, errmsg:
                        print str(errmsg)

                return result

#############################################################################

        def ChangeSchema(self):
            print 'To be done'

#############################################################################

        def CreateSchema(self):
            self.Execute(sqlite3_str.CreateTableCampaign);
            self.Execute(sqlite3_str.CreateTableRun);
            self.Execute(sqlite3_str.CreateTableChannel);
            self.Execute(sqlite3_str.CreateTableFeb);

            self.Execute(sqlite3_str.IndexTableCampaign);
            self.Execute(sqlite3_str.IndexTableRun);
            self.Execute(sqlite3_str.IndexTableChannel);
            self.Execute(sqlite3_str.IndexTableFeb);

#############################################################################

        def Commit(self):
                self.db.commit()

#############################################################################

        def Close(self):
                self.cursor.close()
                self.db.close()

#############################################################################
# ORACLE ABSTRACTION                                                        #
#############################################################################

class OracleAbstract:

#############################################################################

        def EngineName(self):
                return 'oracle'

#############################################################################

        def Open(self, address):
                self.db_adress = address

                try:
                        self.db = cx_Oracle.connect(address)
                        self.cursor = self.db.cursor()

                except cx_Oracle.Error, errmsg:
                        raise Exception(str(errmsg))

#############################################################################

        def TableInfo(self, name):
                result = []

                try:
                        self.cursor.execute('pragma table_info("%s")' % name)

                        fields = self.cursor.fetchall()

                        for field in fields:
                                result.append(field[1].lower())

                except cx_Oracle.Error, errmsg:
                        print str(errmsg)

                return result

#############################################################################

        def Execute(self, query):
                result = []

                try:
                        self.cursor.execute(query)

                        result = SQLBuildDic(self.cursor.description, self.cursor.fetchall())

                except cx_Oracle.Error, errmsg:
                        print str(errmsg)

                return result

#############################################################################

        def CreateSchema(self):
                self.Execute(cx_Oracle_str.CreateTableCampaign);
                self.Execute(cx_Oracle_str.CreateTableRun);
                self.Execute(cx_Oracle_str.CreateTableChannel);
                self.Execute(cx_Oracle_str.CreateTableFeb);

#############################################################################

        def Commit(self):
                pass

#############################################################################

        def Close(self):
                self.cursor.close()
                self.db.close()

#############################################################################
# ECalDBLib                                                                 #
#############################################################################

class ECalDBLib:

#############################################################################

        def __init__(self):
                self.engine = None
#############################################################################

        def Execute(self, query):
                self.engine.Execute(query)

#############################################################################

        def Open(self, address):
                if address[:9] == 'sqlite://':
                        self.engine = SQLiteAbstract()
                if address[:9] == 'oracle://':
                        self.engine = OracleAbstract()

                try:
                        self.engine.Open(address[9:])

                except AttributeError:
                        raise Exception('bad protocol')

#############################################################################

        def EngineName(self):
                try:
                        return self.engine.EngineName()

                except AttributeError:
                        return 'none'

#############################################################################

        def CreateSchema(self):
                try:
                        self.engine.CreateSchema()

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################

        def DropSchema(self):
                try:
                        if self.engine.EngineName() == 'sqlite':
                                self.engine.Execute('DROP TABLE Campaign')
                                self.engine.Execute('DROP TABLE Run')
                                self.engine.Execute('DROP TABLE Channel')
                                self.engine.Execute('DROP TABLE Feb')
                        else:
                                raise Exception('not allow')

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################

        def AddCampaignEntry(self, entry):
                try:
                        query = '''
                                INSERT INTO Campaign (
                                        ref,
                                        date,
                                        type,
                                        ntuple,
                                        toroid_status,
                                        solenoid_status,
                                        composite,
                                        dq_flag,
                                        flag,
                                        db,
                                        comment
                                )
                                VALUES (
                                        '%d',
                                        '%d',
                                        '%s',
                                        '%s',
                                        '%s',
                                        '%s',
                                        '%s',
                                        '%s',
                                        '%s',
                                        '%s',
                                        '%s'
                                )
                        ''' % (
                                entry['ref'],
                                entry['date'],
                                entry['type'],
                                entry['ntuple'],
                                entry['toroid_status'],
                                entry['solenoid_status'],
                                entry['composite'],
                                entry['dq_flag'],
                                entry['flag'],
                                entry['db'],
                                entry['comment']
                        )
                        self.engine.Execute(query)


                except (TypeError, KeyError), errmsg:
                        raise Exception('input error, ' + str(errmsg))

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################

        def RemoveCampaignEntryById( self, id ):
                try:
                        print 'delete campaign'
                        self.engine.Execute('''
                                DELETE FROM Campaign WHERE
                                        id = '%d'
                        ''' % (id ))

                except AttributeError:
                        raise Exception('DB not opened')

                try:
                        print 'delete run'
                        self.engine.Execute('''
                                DELETE FROM Run WHERE
                                        campaign = '%d'
                        ''' % (id))

                except AttributeError:
                        raise Exception('DB not opened')

                try:
                        print 'delete feb'
                        self.engine.Execute('''
                                DELETE FROM Feb WHERE
                                        campaign = '%d'
                        ''' % (id))

                except AttributeError:
                        raise Exception('DB not opened')

                try:
                        print 'delete channel'
                        self.engine.Execute('''
                                DELETE FROM Channel WHERE
                                        campaign = '%d'
                        ''' % (id))

                except AttributeError:
                        raise Exception('DB not opened')

                directory = '%s/Campaign_%d/' % (resBasePath,id)
                os.system('rm -rf '+directory)


#############################################################################

        def AddRunEntry(self, entry):
                try:
                        query = '''
                                INSERT INTO Run (
                                        id,
                                        campaign,
                                        date_start,
                                        date_stop,
                                        type,
                                        partition,
                                        gain,
                                        toroid,
                                        solenoid,
                                        dq_flag,
                                        flag
                                )
                                VALUES (
                                        '%d',
                                        '%d',
                                        '%d',
                                        '%d',
                                        '%s',
                                        '%s',
                                        '%s',
                                        '%f',
                                        '%f',
                                        '%s',
                                        '%s'
                                )
                        ''' % (
                                entry['id'],
                                entry['campaign'],
                                entry['date_start'],
                                entry['date_stop'],
                                entry['type'],
                                entry['partition'],
                                entry['gain'],
                                entry['toroid'],
                                entry['solenoid'],
                                entry['dq_flag'],
                                entry['flag']
                        )
                        self.engine.Execute(query)
                except (TypeError, KeyError), errmsg:
                        raise Exception('input error, ' + str(errmsg))

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################

        def AddChannelEntry(self, entry):
                time_begin = time.clock()
                query = ''

                try:
                        # if 'comment' not in entry.keys():
                        #     prevComment = '';
                        #     listCom = self.GetChannelComments(entry['sub_sub_partition'], entry['ft'], entry['sl'], entry['ch'])
                        #     if listCom!=[]:
                        #         listCom.reverse()
                        #         for c in listCom:
                        #             if c['campaign']<entry['campaign']:
                        #                 prevComment = c['comment']
                        #                 break
                        #     entry['comment'] = prevComment

                        query = query + '''
                                INSERT INTO Channel (
                                        campaign,
                                        sub_sub_partition,
                                        ft,
                                        sl,
                                        ch,
                                        cl,
                                        is_bad,
                        '''

                        if entry.has_key('deviation1_h'):
                                query = query + 'deviation1_h,'
                        if entry.has_key('deviation2_h'):
                                query = query + 'deviation2_h,'
                        if entry.has_key('deviation3_h'):
                                query = query + 'deviation3_h,'
                        if entry.has_key('deviation4_h'):
                                query = query + 'deviation4_h,'
                        if entry.has_key('deviation5_h'):
                                query = query + 'deviation5_h,'
                        if entry.has_key('deviation6_h'):
                                query = query + 'deviation6_h,'
                        if entry.has_key('deviation7_h'):
                                query = query + 'deviation7_h,'
                        if entry.has_key('deviation8_h'):
                                query = query + 'deviation8_h,'
                        if entry.has_key('deviation9_h'):
                                query = query + 'deviation9_h,'
                        if entry.has_key('deviation10_h'):
                                query = query + 'deviation10_h,'

                        if entry.has_key('deviation1_m'):
                                query = query + 'deviation1_m,'
                        if entry.has_key('deviation2_m'):
                                query = query + 'deviation2_m,'
                        if entry.has_key('deviation3_m'):
                                query = query + 'deviation3_m,'
                        if entry.has_key('deviation4_m'):
                                query = query + 'deviation4_m,'
                        if entry.has_key('deviation5_m'):
                                query = query + 'deviation5_m,'
                        if entry.has_key('deviation6_m'):
                                query = query + 'deviation6_m,'
                        if entry.has_key('deviation7_m'):
                                query = query + 'deviation7_m,'
                        if entry.has_key('deviation8_m'):
                                query = query + 'deviation8_m,'
                        if entry.has_key('deviation9_m'):
                                query = query + 'deviation9_m,'
                        if entry.has_key('deviation10_m'):
                                query = query + 'deviation10_m,'

                        if entry.has_key('deviation1_l'):
                                query = query + 'deviation1_l,'
                        if entry.has_key('deviation2_l'):
                                query = query + 'deviation2_l,'
                        if entry.has_key('deviation3_l'):
                                query = query + 'deviation3_l,'
                        if entry.has_key('deviation4_l'):
                                query = query + 'deviation4_l,'
                        if entry.has_key('deviation5_l'):
                                query = query + 'deviation5_l,'
                        if entry.has_key('deviation6_l'):
                                query = query + 'deviation6_l,'
                        if entry.has_key('deviation7_l'):
                                query = query + 'deviation7_l,'
                        if entry.has_key('deviation8_l'):
                                query = query + 'deviation8_l,'
                        if entry.has_key('deviation9_l'):
                                query = query + 'deviation9_l,'
                        if entry.has_key('deviation10_l'):
                                query = query + 'deviation10_l,'

                        query = query + 'comment,'

                        ##################
                        query = query[:-1]
                        ##################

                        query = query + '''
                                )
                                VALUES (
                                        '%d',
                                        '%s',
                                        '%d',
                                        '%d',
                                        '%d',
                                        '%d',
                                        '%d',
                        ''' % (
                                entry['campaign'],
                                entry['sub_sub_partition'],
                                entry['ft'],
                                entry['sl'],
                                entry['ch'],
                                entry['cl'],
                                entry['is_bad']
                        )

                        if entry.has_key('deviation1_h'):
                                query = query + "'%f'," % entry['deviation1_h']
                        if entry.has_key('deviation2_h'):
                                query = query + "'%f'," % entry['deviation2_h']
                        if entry.has_key('deviation3_h'):
                                query = query + "'%f'," % entry['deviation3_h']
                        if entry.has_key('deviation4_h'):
                                query = query + "'%f'," % entry['deviation4_h']
                        if entry.has_key('deviation5_h'):
                                query = query + "'%f'," % entry['deviation5_h']
                        if entry.has_key('deviation6_h'):
                                query = query + "'%f'," % entry['deviation6_h']
                        if entry.has_key('deviation7_h'):
                                query = query + "'%f'," % entry['deviation7_h']
                        if entry.has_key('deviation8_h'):
                                query = query + "'%f'," % entry['deviation8_h']
                        if entry.has_key('deviation9_h'):
                                query = query + "'%f'," % entry['deviation9_h']
                        if entry.has_key('deviation10_h'):
                                query = query + "'%f'," % entry['deviation10_h']

                        if entry.has_key('deviation1_m'):
                                query = query + "'%f'," % entry['deviation1_m']
                        if entry.has_key('deviation2_m'):
                                query = query + "'%f'," % entry['deviation2_m']
                        if entry.has_key('deviation3_m'):
                                query = query + "'%f'," % entry['deviation3_m']
                        if entry.has_key('deviation4_m'):
                                query = query + "'%f'," % entry['deviation4_m']
                        if entry.has_key('deviation5_m'):
                                query = query + "'%f'," % entry['deviation5_m']
                        if entry.has_key('deviation6_m'):
                                query = query + "'%f'," % entry['deviation6_m']
                        if entry.has_key('deviation7_m'):
                                query = query + "'%f'," % entry['deviation7_m']
                        if entry.has_key('deviation8_m'):
                                query = query + "'%f'," % entry['deviation8_m']
                        if entry.has_key('deviation9_m'):
                                query = query + "'%f'," % entry['deviation9_m']
                        if entry.has_key('deviation10_m'):
                                query = query + "'%f'," % entry['deviation10_m']

                        if entry.has_key('deviation1_l'):
                                query = query + "'%f'," % entry['deviation1_l']
                        if entry.has_key('deviation2_l'):
                                query = query + "'%f'," % entry['deviation2_l']
                        if entry.has_key('deviation3_l'):
                                query = query + "'%f'," % entry['deviation3_l']
                        if entry.has_key('deviation4_l'):
                                query = query + "'%f'," % entry['deviation4_l']
                        if entry.has_key('deviation5_l'):
                                query = query + "'%f'," % entry['deviation5_l']
                        if entry.has_key('deviation6_l'):
                                query = query + "'%f'," % entry['deviation6_l']
                        if entry.has_key('deviation7_l'):
                                query = query + "'%f'," % entry['deviation7_l']
                        if entry.has_key('deviation8_l'):
                                query = query + "'%f'," % entry['deviation8_l']
                        if entry.has_key('deviation9_l'):
                                query = query + "'%f'," % entry['deviation9_l']
                        if entry.has_key('deviation10_l'):
                                query = query + "'%f'," % entry['deviation10_l']

                        query = query + "'%s'," % entry['comment']

                        ##################
                        query = query[:-1]
                        ##################

                        query = query + '''
                                )
                        '''

                        self.engine.Execute(query)

                except (TypeError, KeyError), errmsg:
                        raise Exception('input error, ' + str(errmsg))

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################

        def AddFebEntry(self, entry):
                time_begin = time.time()
                query = ''

                try:
                        # if 'comment' not in entry.keys():
                        #     prevComment = '';
                        #     listCom = self.GetFebComments(entry['sub_sub_partition'], entry['ft'], entry['sl'])
                        #     if listCom!=[]:
                        #         listCom.reverse()
                        #         for c in listCom:
                        #             if c['campaign']<entry['campaign']:
                        #                 prevComment = c['comment']
                        #                 break
                        #     entry['comment'] = prevComment

                        query = query + '''
                                INSERT INTO Feb (
                                        campaign,
                                        sub_sub_partition,
                                        ft,
                                        sl,
                                        energy_h,
                                        energy_m,
                                        energy_l,
                                        temperature,
                        '''

                        if entry.has_key('deviation1_h'):
                                query = query + 'deviation1_h,'
                        if entry.has_key('deviation2_h'):
                                query = query + 'deviation2_h,'
                        if entry.has_key('deviation3_h'):
                                query = query + 'deviation3_h,'
                        if entry.has_key('deviation4_h'):
                                query = query + 'deviation4_h,'
                        if entry.has_key('deviation5_h'):
                                query = query + 'deviation5_h,'
                        if entry.has_key('deviation6_h'):
                                query = query + 'deviation6_h,'
                        if entry.has_key('deviation7_h'):
                                query = query + 'deviation7_h,'
                        if entry.has_key('deviation8_h'):
                                query = query + 'deviation8_h,'
                        if entry.has_key('deviation9_h'):
                                query = query + 'deviation9_h,'
                        if entry.has_key('deviation10_h'):
                                query = query + 'deviation10_h,'

                        if entry.has_key('deviation1_m'):
                                query = query + 'deviation1_m,'
                        if entry.has_key('deviation2_m'):
                                query = query + 'deviation2_m,'
                        if entry.has_key('deviation3_m'):
                                query = query + 'deviation3_m,'
                        if entry.has_key('deviation4_m'):
                                query = query + 'deviation4_m,'
                        if entry.has_key('deviation5_m'):
                                query = query + 'deviation5_m,'
                        if entry.has_key('deviation6_m'):
                                query = query + 'deviation6_m,'
                        if entry.has_key('deviation7_m'):
                                query = query + 'deviation7_m,'
                        if entry.has_key('deviation8_m'):
                                query = query + 'deviation8_m,'
                        if entry.has_key('deviation9_m'):
                                query = query + 'deviation9_m,'
                        if entry.has_key('deviation10_m'):
                                query = query + 'deviation10_m,'

                        if entry.has_key('deviation1_l'):
                                query = query + 'deviation1_l,'
                        if entry.has_key('deviation2_l'):
                                query = query + 'deviation2_l,'
                        if entry.has_key('deviation3_l'):
                                query = query + 'deviation3_l,'
                        if entry.has_key('deviation4_l'):
                                query = query + 'deviation4_l,'
                        if entry.has_key('deviation5_l'):
                                query = query + 'deviation5_l,'
                        if entry.has_key('deviation6_l'):
                                query = query + 'deviation6_l,'
                        if entry.has_key('deviation7_l'):
                                query = query + 'deviation7_l,'
                        if entry.has_key('deviation8_l'):
                                query = query + 'deviation8_l,'
                        if entry.has_key('deviation9_l'):
                                query = query + 'deviation9_l,'
                        if entry.has_key('deviation10_l'):
                                query = query + 'deviation10_l,'

                        query = query + 'comment,'

                        ##################
                        query = query[:-1]
                        ##################

                        query = query + '''
                                )
                                VALUES (
                                        '%d',
                                        '%s',
                                        '%d',
                                        '%d',
                                        '%f',
                                        '%f',
                                        '%f',
                                        '%f',
                        ''' % (
                                entry['campaign'],
                                entry['sub_sub_partition'],
                                entry['ft'],
                                entry['sl'],
                                entry['energy_h'],
                                entry['energy_m'],
                                entry['energy_l'],
                                entry['temperature'],
                        )

                        if entry.has_key('deviation1_h'):
                                query = query + "'%f'," % entry['deviation1_h']
                        if entry.has_key('deviation2_h'):
                                query = query + "'%f'," % entry['deviation2_h']
                        if entry.has_key('deviation3_h'):
                                query = query + "'%f'," % entry['deviation3_h']
                        if entry.has_key('deviation4_h'):
                                query = query + "'%f'," % entry['deviation4_h']
                        if entry.has_key('deviation5_h'):
                                query = query + "'%f'," % entry['deviation5_h']
                        if entry.has_key('deviation6_h'):
                                query = query + "'%f'," % entry['deviation6_h']
                        if entry.has_key('deviation7_h'):
                                query = query + "'%f'," % entry['deviation7_h']
                        if entry.has_key('deviation8_h'):
                                query = query + "'%f'," % entry['deviation8_h']
                        if entry.has_key('deviation9_h'):
                                query = query + "'%f'," % entry['deviation9_h']
                        if entry.has_key('deviation10_h'):
                                query = query + "'%f'," % entry['deviation10_h']

                        if entry.has_key('deviation1_m'):
                                query = query + "'%f'," % entry['deviation1_m']
                        if entry.has_key('deviation2_m'):
                                query = query + "'%f'," % entry['deviation2_m']
                        if entry.has_key('deviation3_m'):
                                query = query + "'%f'," % entry['deviation3_m']
                        if entry.has_key('deviation4_m'):
                                query = query + "'%f'," % entry['deviation4_m']
                        if entry.has_key('deviation5_m'):
                                query = query + "'%f'," % entry['deviation5_m']
                        if entry.has_key('deviation6_m'):
                                query = query + "'%f'," % entry['deviation6_m']
                        if entry.has_key('deviation7_m'):
                                query = query + "'%f'," % entry['deviation7_m']
                        if entry.has_key('deviation8_m'):
                                query = query + "'%f'," % entry['deviation8_m']
                        if entry.has_key('deviation9_m'):
                                query = query + "'%f'," % entry['deviation9_m']
                        if entry.has_key('deviation10_m'):
                                query = query + "'%f'," % entry['deviation10_m']

                        if entry.has_key('deviation1_l'):
                                query = query + "'%f'," % entry['deviation1_l']
                        if entry.has_key('deviation2_l'):
                                query = query + "'%f'," % entry['deviation2_l']
                        if entry.has_key('deviation3_l'):
                                query = query + "'%f'," % entry['deviation3_l']
                        if entry.has_key('deviation4_l'):
                                query = query + "'%f'," % entry['deviation4_l']
                        if entry.has_key('deviation5_l'):
                                query = query + "'%f'," % entry['deviation5_l']
                        if entry.has_key('deviation6_l'):
                                query = query + "'%f'," % entry['deviation6_l']
                        if entry.has_key('deviation7_l'):
                                query = query + "'%f'," % entry['deviation7_l']
                        if entry.has_key('deviation8_l'):
                                query = query + "'%f'," % entry['deviation8_l']
                        if entry.has_key('deviation9_l'):
                                query = query + "'%f'," % entry['deviation9_l']
                        if entry.has_key('deviation10_l'):
                                query = query + "'%f'," % entry['deviation10_l']

                        query = query + "'%s'," % entry['comment']

                        ##################
                        query = query[:-1]
                        ##################

                        query = query + '''
                                )
                        '''

                        self.engine.Execute(query)

                except (TypeError, KeyError), errmsg:
                        raise Exception('input error, ' + str(errmsg))

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################
#############################################################################

        def Execute(self, query):
                result = []

                try:
                        result = self.engine.Execute(query)

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def Commit(self):
                try:
                        self.engine.Commit()

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################

        def Close(self):
                try:
                        self.engine.Close()

                        self.engine = None

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################

        def GetCampaignList(self):
                result = []

                try:
                        result = self.engine.Execute('SELECT * FROM Campaign ORDER by date')

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetCampaignById(self, id):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Campaign WHERE
                                        id='%d'
                                    ORDER BY date
                                ''' % (id))

                        if len(result) > 0:
                                result = result[0]

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetCampaignByRef(self, ref):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Campaign WHERE
                                        ref='%d'
                                    ORDER BY date
                                ''' % (ref))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetCampaignByDate(self, date1, date2):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Campaign WHERE
                                        date>='%d'
                                        AND
                                        date<='%d'
                                    ORDER BY date
                                ''' % (date1, date2))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetCampaignByType(self, type):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Campaign WHERE
                                        type='%s'
                                    ORDER BY date
                                ''' % (type))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetCampaignByMagneticFieldStatus(self, toroid_status, solenoid_status):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Campaign WHERE
                                        toroid_status='%s'
                                        AND
                                        solenoid_status='%s'
                                    ORDER BY date
                                ''' % (toroid_status, solenoid_status))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetCampaignByDQFlag(self, dq_flag):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Campaign WHERE
                                        dq_flag='%s'
                                    ORDER BY date
                                ''' % (dq_flag))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetCampaignByFlag(self, flag):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Campaign WHERE
                                        flag='%s'
                                    ORDER BY date
                                ''' % (flag))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetCampaignByDB(self, db):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Campaign WHERE
                                        db='%s'
                                    ORDER BY date
                                ''' % (db))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetCampaignModule(self, Id):
                result = set()

                try:
                    modulePath = './CampaignModules'
                    if not(os.path.exists(modulePath)):
                           Messages.Out('ERROR', "The symbolic link ./CampaignModules to the AP modules is not properly set.");
                           return ''

                    runDictList  = self.GetRunByCampaign(Id)

                    runList = [i['id'] for i in runDictList]
                    for run in runList:
                        for file in os.listdir(modulePath):
                            if not(file.startswith('CALIB_') and file.endswith('.py')): continue
                            if str(run) in file :
                                result.add(file)

                except AttributeError:
                        raise Exception('DB not opened')

                return [i for i in result]

#############################################################################

        def GetCampaignId(self):
                result = 1

                try:
                        query = self.engine.Execute('''
                                SELECT MAX(id) FROM Campaign
                                ''')

                        if len(query) > 0 and not (query[0]['MAX(id)'] is None):
                                result = query[0]['MAX(id)'] + 1

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetReferenceId(self, toroid_status, solenoid_status):
                result = 0

                try:
##                         query = self.engine.Execute('''
##                                 SELECT * FROM Campaign WHERE
##                                         type='WEEKLY'
##                                         AND
##                                         toroid_status='%s'
##                                         AND
##                                         solenoid_status='%s'
##                                         AND
##                                         (dq_flag='GREEN' or 'UNDEFINED')
##                                         AND
##                                         (flag='GREEN' or 'UNDEFINED')
##                                         AND
##                                         (db='CURRENT' or db='YES')
##                                     ORDER BY date
##                                 ''' % (toroid_status, solenoid_status))

                        query = self.engine.Execute('''
                                SELECT * FROM Campaign WHERE
                                        (db='CURRENT' or db='YES')
                                    ORDER BY date
                                ''' )

                        if len(query) > 0:
                                result = query[-1]['id']

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def UpdateCampaignDQFlag(self, campaign, flag):
                try:
                        self.engine.Execute('''
                                UPDATE Campaign SET
                                        dq_flag='%s'
                                WHERE id='%d'
                        ''' % (flag, campaign))

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################

        def UpdateCampaignFlag(self, campaign, flag):
            try:
                self.engine.Execute("UPDATE Campaign SET flag='%s' WHERE id='%d'" % (flag, campaign))

            except AttributeError:
                raise Exception('DB not opened')

#############################################################################

        def UpdateCampaignComment(self, campaign, comment):
            try:
                if comment!='':
                    if re.match('[0-3][0-9]/[0-1][0-9]/[0-9][0-9]:*',comment)==None:
                        Messages.Out('ERROR', "A comment must have the format dd/mm/yy:comment");
                        return

                    com = self.engine.Execute("SELECT comment FROM Campaign WHERE id='%d'" % (campaign))[0]
                    if (com['comment']!='' and com['comment']!=None): comment = com['comment']+', '+comment
                self.engine.Execute("UPDATE Campaign SET comment='%s' WHERE id='%d'" % (comment, campaign))

            except AttributeError:
                raise Exception('DB not opened')

#############################################################################

        def UpdateChannelComment(self, campaign, sub_sub_partition, ft, sl, ch, comment):
            try:
                if comment!='':
                    if re.match('[0-3][0-9]/[0-1][0-9]/[0-9][0-9]:*',comment)==None:
                        Messages.Out('ERROR', "A comment must have the format dd/mm/yy:comment");
                        return

                    com = self.engine.Execute("SELECT comment FROM channel WHERE campaign='%d' AND sub_sub_partition='%s' AND ft='%d' AND sl='%d' AND ch='%d'" % (campaign, sub_sub_partition, ft, sl, ch))
                    if len(com)>0: com = com[0]
                    else : com={'comment':''}
                    if (com['comment']!='' and com['comment']!=None): comment = com['comment']+', '+comment
                self.engine.Execute("UPDATE channel SET comment='%s' WHERE campaign='%d' AND sub_sub_partition='%s' AND ft='%d' AND sl='%d' AND ch='%d'" % (comment, campaign, sub_sub_partition, ft, sl, ch))

            except AttributeError:
                raise Exception('DB not opened')

#############################################################################

        def GetChannelComments(self, sub_sub_partition, ft, sl, ch):
            try:
                res = self.engine.Execute("SELECT campaign,comment FROM channel WHERE sub_sub_partition='%s' AND ft='%d' AND sl='%d' AND ch='%d' order by campaign" % (sub_sub_partition, ft, sl, ch))

            except AttributeError:
                raise Exception('DB not opened')

            return res

#############################################################################

        def UpdateFebComment(self, campaign, sub_sub_partition, ft, sl, comment):
            try:
                if comment!='':
                    if re.match('[0-3][0-9]/[0-1][0-9]/[0-9][0-9]:*',comment)==None:
                        Messages.Out('ERROR', "A comment must have the format dd/mm/yy:comment");
                        return

                    com = self.engine.Execute("SELECT comment FROM feb WHERE campaign='%d' AND sub_sub_partition='%s' AND ft='%d' AND sl='%d'" % (campaign, sub_sub_partition, ft, sl))
                    if len(com)>0: com = com[0]
                    else : com={'comment':''}
                    if (com['comment']!='' and com['comment']!=None): comment = com['comment']+', '+comment
                self.engine.Execute("UPDATE feb SET comment='%s' WHERE campaign='%d' AND sub_sub_partition='%s' AND ft='%d' AND sl='%d'" % (comment, campaign, sub_sub_partition, ft, sl))

            except AttributeError:
                raise Exception('DB not opened')

#############################################################################

        def GetFebComments(self, sub_sub_partition, ft, sl):
            try:
                res = self.engine.Execute("SELECT campaign,comment FROM feb WHERE sub_sub_partition='%s' AND ft='%d' AND sl='%d' order by campaign" % (sub_sub_partition, ft, sl))

            except AttributeError:
                raise Exception('DB not opened')

            return res

#############################################################################

        def UpdateCampaignDBStatus(self, campaign, status):
                try:
                        self.engine.Execute('''
                                UPDATE Campaign SET
                                        db='%s'
                                WHERE id='%d'
                        ''' % (status, campaign))

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################
#############################################################################

        def GetRunById(self, id):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Run WHERE
                                        id='%d'
                                    ORDER BY date_start
                                ''' % (id))

                        if len(result) > 0:
                                result = result[0]

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetRunByCampaign(self, campaign):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Run WHERE
                                        campaign='%d'
                                    ORDER BY date_start
                                ''' % (campaign))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetRunByDate(self, date1, date2):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Run WHERE
                                        (date_start>='%d' AND date_start<='%d')
                                        OR
                                        (date_stop >='%d' AND date_stop <='%d')
                                    ORDER BY date_start
                                ''' % (date1, date2, date1, date2))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetRunByType(self, type):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Run WHERE
                                        type='%s'
                                    ORDER BY date_start
                                ''' % (type))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetRunByPartition(self, partition):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Run WHERE
                                        partition='%s'
                                    ORDER BY date_start
                                ''' % (partition))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetRunByGain(self, gain):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Run WHERE
                                        gain='%s'
                                    ORDER BY date_start
                                ''' % (gain))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetRunByDQFlag(self, dq_flag):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Run WHERE
                                        dq_flag='%s'
                                    ORDER BY date_start
                                ''' % (dq_flag))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetRunByFlag(self, flag):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Run WHERE
                                        flag='%s'
                                    ORDER BY date_start
                                ''' % (flag))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def UpdateRunDQFlag(self, run, flag):
                try:
                        self.engine.Execute('''
                                UPDATE Run SET
                                        dq_flag='%s'
                                WHERE id='%d'
                        ''' % (flag, run))

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################

        def UpdateRunFlag(self, run, flag):
                try:
                        self.engine.Execute('''
                                UPDATE Run SET
                                        flag='%s'
                                WHERE id='%d'
                        ''' % (flag, run))

                except AttributeError:
                        raise Exception('DB not opened')

#############################################################################
#############################################################################

        def GetChannelByCampaign(self, campaign, sub_sub_partition):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Channel WHERE
                                        campaign='%d'
                                        AND
                                        sub_sub_partition='%s'
                                ''' % (campaign, sub_sub_partition))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetChannelByCampaignFT(self, campaign, sub_sub_partition, ft):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Channel WHERE
                                        campaign='%d'
                                        AND
                                        sub_sub_partition='%s'
                                        AND
                                        ft='%d'
                                ''' % (campaign, sub_sub_partition, ft))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetChannelByCampaignSL(self, campaign, sub_sub_partition, ft, sl):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Channel WHERE
                                        campaign='%d'
                                        AND
                                        sub_sub_partition='%s'
                                        AND
                                        ft='%d'
                                        AND
                                        sl='%d'
                                ''' % (campaign, sub_sub_partition, ft, sl))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetChannelByCampaignCH(self, campaign, sub_sub_partition, ft, sl, ch):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Channel WHERE
                                        campaign='%d'
                                        AND
                                        sub_sub_partition='%s'
                                        AND
                                        ft='%d'
                                        AND
                                        sl='%d'
                                        AND
                                        ch='%d'
                                ''' % (campaign, sub_sub_partition, ft, sl, ch))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def CheckChannel(self, sub_sub_partition, ft, sl, ch, toroid_status = None, solenoid_status = None):
                N = [
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0],
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0],
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0],
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0],
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0],
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0]
                ]

                try:
                        query = self.engine.Execute('''
                                SELECT * FROM Channel WHERE
                                        sub_sub_partition='%s'
                                        AND
                                        ft='%d'
                                        AND
                                        sl='%d'
                                        AND
                                        ch='%d'
                        ''' % (sub_sub_partition, ft, sl, ch))

                        for item in query:
                                L = [
                                        item['deviation1_h'],
                                        item['deviation1_m'],
                                        item['deviation1_l'],
                                        item['deviation2_h'],
                                        item['deviation2_m'],
                                        item['deviation2_l'],
                                        item['deviation3_h'],
                                        item['deviation3_m'],
                                        item['deviation3_l'],
                                        item['deviation4_h'],
                                        item['deviation4_m'],
                                        item['deviation4_l'],
                                        item['deviation5_h'],
                                        item['deviation5_m'],
                                        item['deviation5_l'],
                                        item['deviation6_h'],
                                        item['deviation6_m'],
                                        item['deviation6_l'],
                                        item['deviation7_h'],
                                        item['deviation7_m'],
                                        item['deviation7_l'],
                                        item['deviation8_h'],
                                        item['deviation8_m'],
                                        item['deviation8_l']
                                ]

                                sub_sub_partition = item['sub_sub_partition']

                                M = [
                                        thresholdDict[sub_sub_partition]['H'][4],
                                        thresholdDict[sub_sub_partition]['M'][4],
                                        thresholdDict[sub_sub_partition]['L'][4],
                                        1.0e+9,
                                        1.0e+9,
                                        1.0e+9,
                                        thresholdDict[sub_sub_partition]['H'][0],
                                        thresholdDict[sub_sub_partition]['M'][0],
                                        thresholdDict[sub_sub_partition]['L'][0],
                                        thresholdDict[sub_sub_partition]['H'][1],
                                        thresholdDict[sub_sub_partition]['M'][1],
                                        thresholdDict[sub_sub_partition]['L'][1],
                                        thresholdDict[sub_sub_partition]['H'][8],
                                        thresholdDict[sub_sub_partition]['M'][8],
                                        thresholdDict[sub_sub_partition]['L'][8],
                                        1.0e+9,
                                        1.0e+9,
                                        1.0e+9,
                                        thresholdDict[sub_sub_partition]['H'][6],
                                        thresholdDict[sub_sub_partition]['M'][6],
                                        thresholdDict[sub_sub_partition]['L'][6],
                                        1.0e+9,
                                        1.0e+9,
                                        1.0e+9
                                ]

                                for i in xrange(0, 24):
                                        if L[i] is None:
                                                L[i] = 0.0

                                        value = abs(L[i])
                                        threshold = M[i]

                                        if value > threshold:
                                                N[i][0] += 0x001
                                                N[i][1] += value

                        for i in xrange(0, 24):
                                N[i][1] /= N[i][0]
                                N[i][0] /= len(query) * 100.0

                except AttributeError:
                        raise Exception('DB not opened')

                return [
                        N[ 0: 2], N[ 3: 5], N[ 6: 8], N[ 9:11],
                        N[12:14], N[15:17], N[18:20], N[21:23]
                ]

#############################################################################

        def GetFebByCampaign(self, campaign, sub_sub_partition):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Feb WHERE
                                        campaign='%d'
                                        AND
                                        sub_sub_partition='%s'
                                ''' % (campaign, sub_sub_partition))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetFebByCampaignFT(self, campaign, sub_sub_partition, ft):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Feb WHERE
                                        campaign='%d'
                                        AND
                                        sub_sub_partition='%s'
                                        AND
                                        ft='%d'
                                ''' % (campaign, sub_sub_partition, ft))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def GetFebByCampaignSL(self, campaign, sub_sub_partition, ft, sl):
                result = []

                try:
                        result = self.engine.Execute('''
                                SELECT * FROM Feb WHERE
                                        campaign='%d'
                                        AND
                                        sub_sub_partition='%s'
                                        AND
                                        ft='%d'
                                        AND
                                        sl='%d'
                                ''' % (campaign, sub_sub_partition, ft, sl))

                except AttributeError:
                        raise Exception('DB not opened')

                return result

#############################################################################

        def CheckFeb(self, sub_sub_partition, ft, sl, toroid_status = None, solenoid_status = None):
                N = [
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0],
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0],
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0],
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0],
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0],
                        [0, 0.0], [0, 0.0], [0, 0.0], [0, 0.0]
                ]

                try:
                        query = self.engine.Execute('''
                                SELECT * FROM Feb WHERE
                                        sub_sub_partition='%s'
                                        AND
                                        ft='%d'
                                        AND
                                        sl='%d'
                        ''' % (sub_sub_partition, ft, sl))

                        for item in query:
                                L = [
                                        item['deviation1_h'],
                                        item['deviation1_m'],
                                        item['deviation1_l'],
                                        item['deviation2_h'],
                                        item['deviation2_m'],
                                        item['deviation2_l'],
                                        item['deviation3_h'],
                                        item['deviation3_m'],
                                        item['deviation3_l'],
                                        item['deviation4_h'],
                                        item['deviation4_m'],
                                        item['deviation4_l'],
                                        item['deviation5_h'],
                                        item['deviation5_m'],
                                        item['deviation5_l'],
                                        item['deviation6_h'],
                                        item['deviation6_m'],
                                        item['deviation6_l'],
                                        item['deviation7_h'],
                                        item['deviation7_m'],
                                        item['deviation7_l'],
                                        item['deviation8_h'],
                                        item['deviation8_m'],
                                        item['deviation8_l']
                                ]

                                sub_sub_partition = item['sub_sub_partition']

                                M = [
                                        thresholdDict[sub_sub_partition]['H'][5],
                                        thresholdDict[sub_sub_partition]['M'][5],
                                        thresholdDict[sub_sub_partition]['L'][5],
                                        1.0e+9,
                                        1.0e+9,
                                        1.0e+9,
                                        thresholdDict[sub_sub_partition]['H'][2],
                                        thresholdDict[sub_sub_partition]['M'][2],
                                        thresholdDict[sub_sub_partition]['L'][2],
                                        thresholdDict[sub_sub_partition]['H'][3],
                                        thresholdDict[sub_sub_partition]['M'][3],
                                        thresholdDict[sub_sub_partition]['L'][3],
                                        thresholdDict[sub_sub_partition]['H'][9],
                                        thresholdDict[sub_sub_partition]['M'][9],
                                        thresholdDict[sub_sub_partition]['L'][9],
                                        1.0e+9,
                                        1.0e+9,
                                        1.0e+9,
                                        thresholdDict[sub_sub_partition]['H'][7],
                                        thresholdDict[sub_sub_partition]['M'][7],
                                        thresholdDict[sub_sub_partition]['L'][7],
                                        1.0e+9,
                                        1.0e+9,
                                        1.0e+9
                                ]

                                for i in xrange(0, 24):
                                        if L[i] is None:
                                                L[i] = 0.0

                                        value = abs(L[i])
                                        threshold = M[i]

                                        if value > threshold:
                                                N[i][0] += 0x001
                                                N[i][1] += value

                        for i in xrange(0, 24):
                                N[i][1] /= N[i][0]
                                N[i][0] /= len(query) * 100.0

                except AttributeError:
                        raise Exception('DB not opened')

                return [
                        N[ 0: 2], N[ 3: 5], N[ 6: 8], N[ 9:11],
                        N[12:14], N[15:17], N[18:20], N[21:23]
                ]

#############################################################################

        def CampaignFormatInternal(self, item):
                print '%4d %4d %25s %6s %6s %8s %9s %9s %9s %7s     %s' % (
                        item['id'],
                        item['ref'],
                        time.strftime('%a, %d %b %Y %H:%M:%S', time.localtime(item['date'])),
                        item['type'],
                        item['toroid_status'],
                        item['solenoid_status'],
                        item['composite'],
                        item['dq_flag'],
                        item['flag'],
                        item['db'],
                        item['comment']
                )

#############################################################################

        def RunFormatInternal(self, item):
                print '%9d %4d %25s <-> %25s %8s %9s %4s % .5e % .5e %9s %9s' % (
                        item['id'],
                        item['campaign'],
                        time.strftime('%a, %d %b %Y %H:%M:%S', time.localtime(item['date_start'])),
                        time.strftime('%a, %d %b %Y %H:%M:%S', time.localtime(item['date_stop'])),
                        item['type'],
                        item['partition'],
                        item['gain'],
                        item['toroid'],
                        item['solenoid'],
                        item['dq_flag'],
                        item['flag']
                )

#############################################################################

        def ChannelFormatInternal(self, item):
                L = [
                        item['deviation1_h'],
                        item['deviation1_m'],
                        item['deviation1_l'],
                        item['deviation2_h'],
                        item['deviation2_m'],
                        item['deviation2_l'],
                        item['deviation3_h'],
                        item['deviation3_m'],
                        item['deviation3_l'],
                        item['deviation4_h'],
                        item['deviation4_m'],
                        item['deviation4_l'],
                        item['deviation5_h'],
                        item['deviation5_m'],
                        item['deviation5_l'],
                        item['deviation6_h'],
                        item['deviation6_m'],
                        item['deviation6_l'],
                        item['deviation7_h'],
                        item['deviation7_m'],
                        item['deviation7_l'],
                        item['deviation8_h'],
                        item['deviation8_m'],
                        item['deviation8_l'],
                        item['comment']
                ]

                sub_sub_partition = item['sub_sub_partition']

                M = [
                        thresholdDict[sub_sub_partition]['H'][4],
                        thresholdDict[sub_sub_partition]['M'][4],
                        thresholdDict[sub_sub_partition]['L'][4],
                        1.0e-9,
                        1.0e-9,
                        1.0e-9,
                        thresholdDict[sub_sub_partition]['H'][0],
                        thresholdDict[sub_sub_partition]['M'][0],
                        thresholdDict[sub_sub_partition]['L'][0],
                        thresholdDict[sub_sub_partition]['H'][1],
                        thresholdDict[sub_sub_partition]['M'][1],
                        thresholdDict[sub_sub_partition]['L'][1],
                        thresholdDict[sub_sub_partition]['H'][8],
                        thresholdDict[sub_sub_partition]['M'][8],
                        thresholdDict[sub_sub_partition]['L'][8],
                        1.0e-9,
                        1.0e-9,
                        1.0e-9,
                        thresholdDict[sub_sub_partition]['H'][6],
                        thresholdDict[sub_sub_partition]['M'][6],
                        thresholdDict[sub_sub_partition]['L'][6],
                        1.0e-9,
                        1.0e-9,
                        1.0e-9
                ]

                N = []

                for i in xrange(0, 24):
                        if L[i] is None:
                                L[i] = 0.0

                        value = abs(L[i])
                        threshold = M[i]

                        if   value > 3.0 * threshold:
                                color = '\033[31m'
                        elif value > 1.5 * threshold:
                                color = '\033[33m'
                        elif value > 0.0 * threshold:
                                color = '\033[34m'
                        else:
                                color = '\033[37m'

                        N.append(color + '% .6f' % L[i] + '\033[0m')

                print '%6s %2d %2d %3d %3d 0x%08X %s,%s,%s  %s,%s,%s  %s,%s,%s  %s,%s,%s     %s' % (
                        sub_sub_partition,
                        item['ft'],
                        item['sl'],
                        item['ch'],
                        item['cl'],
                        item['is_bad'],
                        N[ 0], N[ 1], N[ 2], N[ 3], N[ 4], N[ 5],
                        N[ 6], N[ 7], N[ 8], N[ 9], N[10], N[11],
                        item['comment']
                )

                print '                                %s,%s,%s  %s,%s,%s  %s,%s,%s  %s,%s,%s' % (
                        N[12], N[13], N[14], N[15], N[16], N[17],
                        N[18], N[19], N[20], N[21], N[22], N[23]
                )

                print ''

#############################################################################

        def FebFormatInternal(self, item):
                L = [
                        item['deviation1_h'],
                        item['deviation1_m'],
                        item['deviation1_l'],
                        item['deviation2_h'],
                        item['deviation2_m'],
                        item['deviation2_l'],
                        item['deviation3_h'],
                        item['deviation3_m'],
                        item['deviation3_l'],
                        item['deviation4_h'],
                        item['deviation4_m'],
                        item['deviation4_l'],
                        item['deviation5_h'],
                        item['deviation5_m'],
                        item['deviation5_l'],
                        item['deviation6_h'],
                        item['deviation6_m'],
                        item['deviation6_l'],
                        item['deviation7_h'],
                        item['deviation7_m'],
                        item['deviation7_l'],
                        item['deviation8_h'],
                        item['deviation8_m'],
                        item['deviation8_l'],
                        item['comment']
                ]

                sub_sub_partition = item['sub_sub_partition']

                M = [
                        thresholdDict[sub_sub_partition]['H'][5],
                        thresholdDict[sub_sub_partition]['M'][5],
                        thresholdDict[sub_sub_partition]['L'][5],
                        1.0e+9,
                        1.0e+9,
                        1.0e+9,
                        thresholdDict[sub_sub_partition]['H'][2],
                        thresholdDict[sub_sub_partition]['M'][2],
                        thresholdDict[sub_sub_partition]['L'][2],
                        thresholdDict[sub_sub_partition]['H'][3],
                        thresholdDict[sub_sub_partition]['M'][3],
                        thresholdDict[sub_sub_partition]['L'][3],
                        thresholdDict[sub_sub_partition]['H'][9],
                        thresholdDict[sub_sub_partition]['M'][9],
                        thresholdDict[sub_sub_partition]['L'][9],
                        1.0e+9,
                        1.0e+9,
                        1.0e+9,
                        thresholdDict[sub_sub_partition]['H'][7],
                        thresholdDict[sub_sub_partition]['M'][7],
                        thresholdDict[sub_sub_partition]['L'][7],
                        1.0e+9,
                        1.0e+9,
                        1.0e+9
                ]

                N = []

                for i in xrange(0, 24):
                        if L[i] is None:
                                L[i] = 0.0

                        value = abs(L[i])
                        threshold = M[i]

                        if   value > 3.0 * threshold:
                                color = '\033[31m'
                        elif value > 1.5 * threshold:
                                color = '\033[33m'
                        elif value > 0.0 * threshold:
                                color = '\033[34m'
                        else:
                                color = '\033[37m'

                        N.append(color + '% .6f' % L[i] + '\033[0m')

                print '%6s %2d %2d % .2e % .2e %s,%s,%s  %s,%s,%s  %s,%s,%s  %s,%s,%s     %s' % (
                        sub_sub_partition,
                        item['ft'],
                        item['sl'],
                        item['energy_m'],
                        item['temperature'],
                        N[ 0], N[ 1], N[ 2], N[ 3], N[ 4], N[ 5],
                        N[ 6], N[ 7], N[ 8], N[ 9], N[10], N[11],
                        item['comment']
                )

                print '                                 %s,%s,%s  %s,%s,%s  %s,%s,%s  %s,%s,%s' % (
                        N[12], N[13], N[14], N[15], N[16], N[17],
                        N[18], N[19], N[20], N[21], N[22], N[23]
                )

                print ''

#############################################################################

        def CampaignFormat(self, data):
                print '  ID  REF                      TIME   TYPE TOROID SOLENOID COMPOSITE   DQ_FLAG      FLAG      DB'

                if type(data).__name__ == 'list':
                        for item in data:
                                self.CampaignFormatInternal(item)
                elif type(data).__name__ == 'dict':
                        self.CampaignFormatInternal(data)

#############################################################################

        def RunFormat(self, data):
                print '       ID  REF                TIME_START                     TIME_STOP     TYPE PARTITION GAIN       TOROID     SOLENOID   DQ_FLAG      FLAG'

                if type(data).__name__ == 'list':
                        for item in data:
                                self.RunFormatInternal(item)
                elif type(data).__name__ == 'dict':
                        self.RunFormatInternal(data)

#############################################################################

        def ChannelFormat(self, data):
                print ' Part. Ft Sl  Ch  Cl      isBad           AUTOCORR1 (H, M, L)                  UNIMPLEMENTED             PEDESTAL (H, M, L)                NOISE (H, M, L)'
                print '                                               RAMP (H, M, L)                  UNIMPLEMENTED                DELAY (H, M, L)                  UNIMPLEMENTED'

                if type(data).__name__ == 'list':
                        for item in data:
                                self.ChannelFormatInternal(item)
                elif type(data).__name__ == 'dict':
                        self.ChannelFormatInternal(data)

#############################################################################

        def FebFormat(self, data):
                print ' Part. Ft Sl    Energy     Temp.           AUTOCORR1 (H, M, L)                  UNIMPLEMENTED             PEDESTAL (H, M, L)                NOISE (H, M, L)'
                print '                                                RAMP (H, M, L)                  UNIMPLEMENTED                DELAY (H, M, L)                  UNIMPLEMENTED'

                if type(data).__name__ == 'list':
                        for item in data:
                                self.FebFormatInternal(item)
                elif type(data).__name__ == 'dict':
                        self.FebFormatInternal(data)

#############################################################################
        def GetChannelHistoryByCampaingId( self, sub_sub_partition, ft, sl, ch, Id1, Id2 ):

           campaigns = []
           result    = []

           try:
               campaigns = self.engine.Execute('''
               SELECT * FROM Campaign WHERE
               id>='%d'
               AND
               id<='%d'
               ORDER BY id
               ''' % (Id1, Id2))

           except AttributeError:
               raise Exception('DB not opened')

           for c in campaigns:
               for channel in self.GetChannelByCampaignCH(c['id'], sub_sub_partition, ft, sl, ch):
                   devdict             = dict()
                   #...Do not account for H gain deviation in HEC
                   if sub_sub_partition[0:-1]=='HEC':
                       channel['deviation1_h'] = None
                       channel['deviation3_h'] = None
                       channel['deviation4_h'] = None
                       channel['deviation5_h'] = None
                       channel['deviation6_h'] = None
                       channel['deviation7_h'] = None
                       channel['deviation8_h'] = None

                   if channel['deviation1_h']!=None or channel['deviation1_m']!=None or channel['deviation1_l']: devdict['AUTOCORR']      = [ channel['deviation1_h'], channel['deviation1_m'], channel['deviation1_l'] ]
                   if channel['deviation3_h']!=None or channel['deviation3_m']!=None or channel['deviation3_l']: devdict['PEDESTAL']      = [ channel['deviation3_h'], channel['deviation3_m'], channel['deviation3_l'] ]
                   if channel['deviation4_h']!=None or channel['deviation4_m']!=None or channel['deviation4_l']: devdict['NOISE']         = [ channel['deviation4_h'], channel['deviation4_m'], channel['deviation4_l'] ]
                   if channel['deviation5_h']!=None or channel['deviation5_m']!=None or channel['deviation5_l']: devdict['RAMP']          = [ channel['deviation5_h'], channel['deviation5_m'], channel['deviation5_l'] ]
                   if channel['deviation6_h']!=None or channel['deviation6_m']!=None or channel['deviation6_l']: devdict['corrUndoRAMP']  = [ channel['deviation6_h'], channel['deviation6_m'], channel['deviation6_l'] ]
                   if channel['deviation7_h']!=None or channel['deviation7_m']!=None or channel['deviation7_l']: devdict['DELAY']         = [ channel['deviation7_h'], channel['deviation7_m'], channel['deviation7_l'] ]
                   if channel['deviation8_h']!=None or channel['deviation8_m']!=None or channel['deviation8_l']: devdict['corrUndoDELAY'] = [ channel['deviation8_h'], channel['deviation8_m'], channel['deviation8_l'] ]

                   if devdict.keys()!=[]:
                       for k in c.keys():
                           devdict[k]       = c[k]
                       result.append(devdict)

           return result

#############################################################################
        def GetChannelHistoryByDate( self, sub_sub_partition, ft, sl, ch, date1, date2 ):

           campaigns = []
           result    = []

           try:
               campaigns = self.engine.Execute('''
               SELECT * FROM Campaign WHERE
               date>='%d'
               AND
               date<='%d'
               ORDER BY date
               ''' % (date1, date2))

           except AttributeError:
               raise Exception('DB not opened')

           for c in campaigns:
               for channel in self.GetChannelByCampaignCH(c['id'], sub_sub_partition, ft, sl, ch):
                   devdict             = dict()
                   devdict['id']       = c['id']
                   devdict['date']     = c['date']
                   devdict['ref']      = c['ref']
                   if channel['deviation1_h']!=None or channel['deviation1_m']!=None or channel['deviation1_l']: devdict['AUTOCORR'] = [ channel['deviation1_h'], channel['deviation1_m'], channel['deviation1_l'] ]
                   if channel['deviation3_h']!=None or channel['deviation3_m']!=None or channel['deviation3_l']: devdict['PEDESTAL'] = [ channel['deviation3_h'], channel['deviation3_m'], channel['deviation3_l'] ]
                   if channel['deviation4_h']!=None or channel['deviation4_m']!=None or channel['deviation4_l']: devdict['NOISE']    = [ channel['deviation4_h'], channel['deviation4_m'], channel['deviation4_l'] ]
                   if channel['deviation5_h']!=None or channel['deviation5_m']!=None or channel['deviation5_l']: devdict['RAMP']     = [ channel['deviation5_h'], channel['deviation5_m'], channel['deviation5_l'] ]
                   if channel['deviation7_h']!=None or channel['deviation7_m']!=None or channel['deviation7_l']: devdict['DELAY']    = [ channel['deviation7_h'], channel['deviation7_m'], channel['deviation7_l'] ]
                   result.append(devdict)

           return result

#############################################################################
        def DrawPusleShape(self, sub_sub_partition, ft, sl ,ch, gain, campaignId=0, option=''):

            curListFile = []
            refListFile = []


            runNumber       = 0
            runNumberRef    = 0
            campaignDate    = 0
            campaignDateRef = 0
            refDate = 0
            campaingRef  = 0

            subPart = ''
            for k,j in listOfSubSubPart.iteritems():
                if sub_sub_partition in j:
                    subPart = k
            part = ''
            for k,j in listOfSubPart.iteritems():
                if subPart in j:
                    part = k

            try:
                query = self.engine.Execute("SELECT date  FROM Campaign WHERE id="+str(campaignId))
                if len(query) > 0:
                    campaignDate = query[0]['date']
            except AttributeError:
                raise Exception('DB not opened')

            try:
                query = {}

                backupId = campaignId
                if(campaignId==0):
                    query = self.engine.Execute("SELECT id, date,ref  FROM Campaign WHERE type='WEEKLY' ORDER BY date DESC" )
                else:
                    query = self.engine.Execute("SELECT id, date,ref  FROM Campaign WHERE type='WEEKLY' and date<="+str(campaignDate)+" ORDER BY date DESC" )

                if len(query) > 0:
                    i=0
                    isOk=False
                    while i<len(query) and isOk==False:
                        campaignId   = query[i]['id']
                        campaignRef  = query[i]['ref']
                        campaignDate = query[i]['date']

                        # Get files for comp campaign
                        curRunDict = self.engine.Execute("SELECT * FROM run WHERE campaign="+str(campaignId)+" and type='DELAY' and gain='"+gain+"' and partition='"+part+"'")
                        if len(curRunDict)>0 :
                            curRunDict=curRunDict[0]
                            runNumber=curRunDict['id']
                            if not('root_files_data' in curRunDict):
                                data, dqmf = GetRootFiles(curRunDict['id'], curRunDict['type'])
                                curRunDict['root_files_data'] = data
                            for curRunFile in curRunDict['root_files_data'] :
                                if subPart=='EMBPS':
                                    if 'EMB' in curRunFile.split('/')[len(curRunFile.split('/'))-1]: curListFile.append(curRunFile)
                                elif subPart in curRunFile.split('/')[len(curRunFile.split('/'))-1]: curListFile.append(curRunFile)
                            if curListFile!=[]: isOk=True
                        i+=1
                    if campaignId!=backupId:
                        print 'Warning : Campaign',campaignId,'is used instead of',backupId,'to draw the pulse shape. <br>'
            except AttributeError:
                raise Exception('DB not opened')


            # Get files for ref campaign
            refRunDict = self.engine.Execute("SELECT * FROM run WHERE campaign="+str(campaignRef)+" and type='DELAY' and gain='"+gain+"' and partition='"+part+"'")
            if len(refRunDict)>0 :
                refRunDict=refRunDict[0]
                runNumberRef=refRunDict['id']
                if not('root_files_data' in refRunDict):
                    data, dqmf = GetRootFiles(refRunDict['id'], refRunDict['type'])
                    refRunDict['root_files_data'] = data
                for refRunFile in refRunDict['root_files_data'] :
                    if subPart=='EMBPS':
                        if 'EMB' in refRunFile.split('/')[len(refRunFile.split('/'))-1]: refListFile.append(refRunFile)
                    elif subPart in refRunFile.split('/')[len(refRunFile.split('/'))-1]: refListFile.append(refRunFile)


            # Check if fig already exist
            if os.environ.has_key('ECalFigures'):
               figName= os.environ['ECalFigures'] + sub_sub_partition+'_ft'+str(ft)+'_sl'+str(sl)+'_ch'+str(ch)+'_'+gain+'_'+str(campaignId)
            else:
               figName = '/afs/cern.ch/user/l/larcalib/w0/data/Validation/Figures/'+sub_sub_partition+'_ft'+str(ft)+'_sl'+str(sl)+'_ch'+str(ch)+'_'+gain+'_'+str(campaignId)
            print 'FigName: ',figName
            if not('Refresh' in option):
                if not('/afs/cern.ch/user/l/larcalib/public/Validation_run/' in dbBasePath) : figName = figName+'_user'
                if not('display' in option) :
                    if os.path.exists(figName+'.png') and os.path.exists(figName+'.eps'):
                        return [figName+'.png',figName+'.eps']

            # Get tree for cur campaign
            if len(curListFile)==0: return [figName+'.png',figName+'.eps']
            curListFile.sort()
            curListFile.reverse()
            curFirst = curListFile[0]
            curTag = curFirst.split('/')[len(curFirst.split('/'))-3].split('_')[len(curFirst.split('/')[len(curFirst.split('/'))-3].split('_'))-1]

            curTree = ROOT.TChain('CALIWAVE')
            for curFile in curListFile:
                if curFile.split('/')[len(curFile.split('/'))-3].endswith('_'+curTag):
                    #Messages.Out('DEBUG', "Use "+curFile+' file for drawing pulse shape for cur file')
                    curTree.Add(curFile)

            # Get tree for ref campaign if exist
            refTree = ROOT.TChain('CALIWAVE')
            if len(refListFile)!=0:
                refListFile.sort()
                refListFile.reverse()
                refFirst = refListFile[0]
                refTag = refFirst.split('/')[len(refFirst.split('/'))-3].split('_')[len(refFirst.split('/')[len(refFirst.split('/'))-3].split('_'))-1]

                refTree = ROOT.TChain('CALIWAVE')
                for refFile in refListFile:
                    if refFile.split('/')[len(refFile.split('/'))-3].endswith('_'+refTag):
#                        print "Use "+refFile+' file for drawing pulse shape for ref file','<br>'
#                        Messages.Out('DEBUG', "Use "+refFile+' file for drawing pulse shape for ref file','<br>')
                        refTree.Add(refFile)


            # Create the plot and save it
            Fig = ROOT.TCanvas()
            barrel_ec = 1
            pos_neg   = 1
            if 'EMB' in sub_sub_partition       : barrel_ec=0
            if   sub_sub_partition.endswith("C"): pos_neg=0

            var = "Amplitude:Time"
            cut = 'barrel_ec==%d && pos_neg==%d && FT==%d && slot==%d && channel==%d && corrUndo==0' % (barrel_ec, pos_neg, ft, sl, ch)

            n1 = curTree.Draw(var, cut,"")

            curGraph =ROOT.TGraph()
            if n1!=0 : curGraph = ROOT.TGraph(ROOT.gPad.GetPrimitive("Graph"))
            n2 = curTree.Draw("Amplitude:Time","barrel_ec=="+str(barrel_ec)+" && pos_neg=="+str(pos_neg)+" && FT=="+str(ft)+" && slot=="+str(sl)+" && channel=="+str(ch)+" && corrUndo==1")
            curGraphcorrUndo = ROOT.TGraph()
            if n2!=0 : curGraphcorrUndo = ROOT.TGraph(ROOT.gPad.GetPrimitive("Graph"))
            if n1==0 and n2==0: curListFile=[]
            del curTree

            if len(refListFile)!=0:

                cut = 'barrel_ec==%d && pos_neg==%d && FT==%d && slot==%d && channel==%d && corrUndo==0' % (barrel_ec, pos_neg, ft, sl, ch)
                n3 = refTree.Draw(var, cut,"")
                refGraph = ROOT.TGraph()
                if n3!=0: refGraph = ROOT.TGraph(ROOT.gPad.GetPrimitive("Graph"))
                cut = 'barrel_ec==%d && pos_neg==%d && FT==%d && slot==%d && channel==%d && corrUndo==1' % (barrel_ec, pos_neg, ft, sl, ch)
                n2 = refTree.Draw(var, cut,"")
                refGraphcorrUndo = ROOT.TGraph()
                if n2!=0: refGraphcorrUndo = ROOT.TGraph(ROOT.gPad.GetPrimitive("Graph"))
                if n3==0 and n2==0: refListFile=[]
                del refTree


            leg = ROOT.TLegend(0.5,0.5,0.7,0.6)

            Fig.Clear()
            curGraph.SetMarkerColor(2)
            curGraph.SetMarkerStyle(26)
            curGraph.SetMarkerSize(0.6)
            curGraphcorrUndo.SetMarkerColor(2)
            curGraphcorrUndo.SetMarkerStyle(5)
            curGraphcorrUndo.SetMarkerSize(0.6)
            if len(refListFile)!=0:
                refGraph.SetMarkerColor(4)
                refGraph.SetMarkerStyle(26)
                refGraph.SetMarkerSize(0.8)
                refGraphcorrUndo.SetMarkerColor(4)
                refGraphcorrUndo.SetMarkerStyle(5)
                refGraphcorrUndo.SetMarkerSize(0.8)

            leg.AddEntry(curGraph,"Campaign "+str(campaignId),"p")
            if curGraphcorrUndo.GetN()!=0:
                leg.AddEntry(curGraphcorrUndo,"Campaign "+str(campaignId)+" corrUndo","p")

            if len(refListFile)!=0:
                refGraph.Draw("ap")
                leg.AddEntry(refGraph,"Reference "+str(campaignRef),"p")
                if refGraphcorrUndo.GetN()!=0:
                    refGraphcorrUndo.Draw("p")
                    leg.AddEntry(refGraphcorrUndo,"Reference "+str(campaignRef)+" corrUndo","p")
                min = refGraph.GetYaxis().GetXmin()
                if curGraph.GetYaxis().GetXmin()<min: min=curGraph.GetYaxis().GetXmin()
                max = refGraph.GetYaxis().GetXmax()
                if curGraph.GetYaxis().GetXmax()>max: max=curGraph.GetYaxis().GetXmax()
                refGraph.GetYaxis().SetRangeUser(min,max)
                curGraph.Draw("p")
                if curGraphcorrUndo.GetN()!=0:
                    curGraphcorrUndo.Draw("p")
            else:
               curGraph.Draw("ap")
               if curGraphcorrUndo.GetN()!=0:
                   curGraphcorrUndo.Draw("p")

            #...get the ref date
            try:
                query = self.engine.Execute("SELECT date  FROM Campaign WHERE id="+str(campaignRef))
                if len(query) > 0:
                    campaignDateRef = query[0]['date']
            except AttributeError:
                raise Exception('DB not opened')

            leg.Draw()
            texte = ROOT.TText()
            texte.SetTextSize(0.035)
            texte.DrawTextNDC(0.35,0.85,sub_sub_partition+'   FT='+str(ft)+'   Slot='+str(sl)+'   Channel='+str(ch)+'    '+gain+' gain')
            texte.DrawTextNDC(0.35,0.80,'Campaign '+str(campaignId)+' from '+time.strftime('%a, %d %b %H:%M', time.localtime(campaignDate))+'  run = '+str(runNumber))
            texte.DrawTextNDC(0.35,0.75,'Reference '+str(campaignRef)+' from '+time.strftime('%a, %d %b %H:%M', time.localtime(campaignDateRef))+'  run = '+str(runNumberRef))

            Fig.Update()
            Fig.SaveAs(figName+'.png')
            Fig.SaveAs(figName+'.eps')
            if 'Display' in option : raw_input("Press Enter To Continue: ")

            return [figName+'.png',figName+'.eps']

#############################################################################
        def DrawValidationPlots(self,  sub_sub_partition, type, gain, campaignId=0, option=''):

            subPart = ''
            for k,j in listOfSubSubPart.iteritems():
                if sub_sub_partition in j:
                    subPart = k
            part = ''
            if type=='AUTOCORR' or type=='PEDESTAL' or type=='NOISE':
               part = 'ALL'
            else:
              for k,j in listOfSubPart.iteritems():
                if subPart in j and k != 'ALL':
                    part = k

            # get the histo
            file = ''
            if type=='FEBTIME':        file = '%s/Campaign_%d/Figure_%s_%s_%d.root' % (resBasePath, campaignId, part, type, campaignId)
            else:                      file = '%s/Campaign_%d/Figure_%s_%s_%s_%d.root' % (resBasePath, campaignId, part, type, gain, campaignId)

            #print 'DrawValidationPlots file: ',file

            # Check if fig already exist
            if os.environ.has_key('ECalFigures'):
               figName= os.environ['ECalFigures'] +sub_sub_partition+'_validation_'+type+'_'+gain+'_'+str(campaignId)
            else:
               figName = '/afs/cern.ch/user/l/larcalib/w0/data/Validation/Figures/'+sub_sub_partition+'_validation_'+type+'_'+gain+'_'+str(campaignId)
            if not('/afs/cern.ch/user/l/larcalib/public/Validation_run/' in dbBasePath) : figName = figName+'_user'
            if not('display' in option):
                if os.path.exists(figName+'.png') and os.path.exists(figName+'.eps'):
                    return [figName+'.png',figName+'.eps']

            if not(os.path.exists(file)):
                Messages.Out('ERROR', "Can't get the validation histo. File"+file+' does not exist.')
                return [figName+'.png',figName+'.eps']

            # Get run info
            curRunDict = {}
            DelayType = ['FEBTIME','DELAY','PEAKTIME','OFC']
            if type in DelayType  : curRunDict = self.engine.Execute("SELECT * FROM run WHERE campaign="+str(campaignId)+" and type='"+typeRun['DELAY']+"' and gain='"+gain+"' and partition='"+part+"'")
            else                  : curRunDict = self.engine.Execute("SELECT * FROM run WHERE campaign="+str(campaignId)+" and type='"+typeRun[type]+"' and gain='"+gain+"' and partition='"+part+"'")
            if len(curRunDict)==0 : return [figName+'.png',figName+'.eps']
            curRunDict = curRunDict[0]
            runNumber = curRunDict['id']

            # Create the plot and save it
            Fig = ROOT.TCanvas()
            ROOT.gPad.SetLogy()
            rootFile = ROOT.TFile(file)

            if type=='OFC':
                nomHisto_cur = 'h_%s_cur_%s' % (type, sub_sub_partition)
                nomHisto_ref = 'h_%s_ref_%s' % (type, sub_sub_partition)
                histoValidation_cur = rootFile.Get(nomHisto_cur)
                histoValidation_ref = rootFile.Get(nomHisto_ref)
                histoValidation_cur.SetLineWidth(1)
                histoValidation_cur.SetLineColor(2)
                histoValidation_ref.SetLineColor(4)
                histoValidation_cur.Draw()
                histoValidation_ref.Draw('same')

##                 leg = ROOT.TLegend(0.45,0.2,0.7,0.3)
##                 leg.AddEntry(histoValidation_ref,"Reference","l")
##                 leg.AddEntry(histoValidation_cur,"Current","l")
##                 leg.Draw()

                texte = ROOT.TText()
                texte.SetTextSize(0.04)
                texte.DrawTextNDC(0.18,0.95,type+'   '+sub_sub_partition+'    '+gain+' gain')
                texte.DrawTextNDC(0.18,0.90,'Campaign '+str(campaignId))
                texte.DrawTextNDC(0.18,0.85,'run number = '+str(runNumber))

                texte_cur = ROOT.TText()
                texte_cur.SetTextSize(0.035)
                texte_cur.SetTextColor(2)
                texte_cur.DrawTextNDC(0.18,0.75,'Current campaign')
                texte_cur.DrawTextNDC(0.18,0.70,'Mean = %1.6f' % histoValidation_cur.GetMean())
                texte_cur.DrawTextNDC(0.18,0.65,'RMS  = %1.6f' % histoValidation_cur.GetRMS())
                texte_cur.DrawTextNDC(0.18,0.60,'Underflow  = %d' % histoValidation_cur.GetBinContent(0))
                texte_cur.DrawTextNDC(0.18,0.55,'Overflow   = %d' % histoValidation_cur.GetBinContent(histoValidation_cur.GetNbinsX()+1))

                texte_ref = ROOT.TText()
                texte_ref.SetTextSize(0.035)
                texte_ref.SetTextColor(4)
                texte_ref.DrawTextNDC(0.18,0.45,'Reference campaign')
                texte_ref.DrawTextNDC(0.18,0.40,'Mean = %1.6f' % histoValidation_ref.GetMean())
                texte_ref.DrawTextNDC(0.18,0.35,'RMS  = %1.6f' % histoValidation_ref.GetRMS())
                texte_ref.DrawTextNDC(0.18,0.30,'Underflow  = %d' % histoValidation_ref.GetBinContent(0))
                texte_ref.DrawTextNDC(0.18,0.25,'Overflow   = %d' % histoValidation_ref.GetBinContent(histoValidation_ref.GetNbinsX()+1))


            else:
                nomHisto = ''
                febgain = {'H':'HM','M':'ML'}
                if   type=='FEBTIME': nomHisto = 'h%s_Delta_%s_%s' % (febgain[gain], type, sub_sub_partition)
                else                : nomHisto = 'h_Delta_%s_%s' % (type, sub_sub_partition)

                histoValidation = rootFile.Get(nomHisto)
                if type=='PEDESTAL' or type=='NOISE':
                   histoValidation.SetXTitle('Delta '+type+' [ADC counts]')
                else:
                   histoValidation.SetXTitle('Delta '+type)
                histoValidation.SetYTitle('# channels')

                histoValidation.Draw()
                # check if swap febs histo exists
                nomHistoswaped = ''
                if   type=='FEBTIME': nomHistoswaped = 'h%s_SWAP_Delta_%s_%s' % (febgain[gain], type, sub_sub_partition)
                else                : nomHistoswaped = 'h_SWAP_Delta_%s_%s' % (type, sub_sub_partition)
                histoValidationswaped = rootFile.Get(nomHistoswaped)
                if histoValidationswaped and histoValidationswaped.GetEntries() > 0.:
                   histoValidationswaped.SetLineColor(419)
                   histoValidationswaped.Draw("same")
                   leg = ROOT.TLegend(0.8,0.8,0.98,0.98,"FEBs")
                   leg.AddEntry(histoValidation,"original","l")
                   leg.AddEntry(histoValidationswaped,"swapped","l")
                   leg.Draw()

                ligne = ROOT.TLine()
                ligne.SetLineColor(2)
                ligne.SetLineWidth(2)
                tresh = thresholdDict[sub_sub_partition][gain][thresholdOrder[type]]
                ligne.DrawLine(tresh,histoValidation.GetMinimum(),tresh,histoValidation.GetMaximum())
                ligne.DrawLine(-tresh,histoValidation.GetMinimum(),-tresh,histoValidation.GetMaximum())

                texte = ROOT.TText()
                texte.SetTextSize(0.04)
                if type=='FEBTIME':        texte.DrawTextNDC(0.18,0.95,type+'   '+sub_sub_partition+'    '+febgain[gain]+' gain')
                else              :        texte.DrawTextNDC(0.18,0.95,type+'   '+sub_sub_partition+'    '+gain+' gain')
                texte.DrawTextNDC(0.18,0.90,'Campaign '+str(campaignId))
                texte.DrawTextNDC(0.18,0.85,'run number = '+str(runNumber))

                texte.DrawTextNDC(0.18,0.70,'Mean = %1.6f' % histoValidation.GetMean())
                texte.DrawTextNDC(0.18,0.65,'RMS  = %1.6f' % histoValidation.GetRMS())
                texte.DrawTextNDC(0.18,0.60,'Underflow  = %d' % histoValidation.GetBinContent(0))
                texte.DrawTextNDC(0.18,0.55,'Overflow   = %d' % histoValidation.GetBinContent(histoValidation.GetNbinsX()+1))


            Fig.Update()
            Fig.SaveAs(figName+'.png')
            Fig.SaveAs(figName+'.eps')

            if 'display' in option : raw_input("Press Enter To Continue: ")
            return [figName+'.png',figName+'.eps']

#############################################################################
        def GetShift(self,  sub_sub_partition, type, gain, campaignId=0, FT=-1, slot=-1, channel=-1):
            subPart = ''
            for k,j in listOfSubSubPart.iteritems():
                if sub_sub_partition in j:
                    subPart = k
            part = ''
            for k,j in listOfSubPart.iteritems():
                if subPart in j:
                    part = k

            # get the histo
            file = '%s/Campaign_%d/Figure_%s_%s_%s_%d.root' % (resBasePath, campaignId, part, type, gain, campaignId)

            if not(os.path.exists(file)):
                Messages.Out('ERROR', "Can't get the validation histo. File"+file+' does not exist.')
                return None
            rootFile = ROOT.TFile(file)
            nomHisto = 'h_Delta_%s_%s' % (type, sub_sub_partition)

            histoValidation = rootFile.Get(nomHisto)

            # Create the plot and save it
            histoValidation.SetXTitle('Delta '+type)
            histoValidation.SetYTitle('# channels')


            return [histoValidation.GetMean(),histoValidation.GetRMS()]

#############################################################################
        def GetConstValue(self,  sub_sub_partition, type, gain, campaignId=0, FT=-1, slot=-1, channel=-1, option=''):
            VarDict = {'AUTOCORR':['Autocorr','Autocorr'],'PEDESTAL':['ped','Ped'],'NOISE':['NOISE','NOISE'],'RAMP':['RAMP','RAMP'],'DELAY':['DELAY','DELAY']}

            if type=='AUTOCORR': return [[0,0]]

            subPart = ''
            for k,j in listOfSubSubPart.iteritems():
                if sub_sub_partition in j:
                    subPart = k
            part = ''
            for k,j in listOfSubPart.iteritems():
                if subPart in j:
                    part = k

            # get the Ntuple
            file = '%s/Campaign_%d/Ntuple_%s_%s_%s_%d.root' % (resBasePath, campaignId, part, type, gain, campaignId)

            if not(os.path.exists(file)):
                #Messages.Out('ERROR', "Can't get the validation histo. File"+file+' does not exist.')
                return None
            rootFile = ROOT.TFile(file)
            nomNtuple = 'Ntuple_%s' % (sub_sub_partition)
            Ntuple = ROOT.TNtuple()
            Ntuple = rootFile.Get(nomNtuple)

            # Get the histo and return the mean value
            cuts = ''
            cuts += subSubPartSelector(sub_sub_partition)

            if 'GoodChannels' in option: cuts += ' && badChan==0'
            if type=='RAMP' : cuts += ' && corrUndo==0'
            if type=='DELAY': cuts += ' && corrUndo==0'
            if FT     !=-1  : cuts += ' && FT=='  +str(FT)
            if slot   !=-1  : cuts += ' && slot=='+str(slot)
            if channel!=-1  : cuts += ' && channel=='+str(channel)

            if not('PerFeb' in option):
                histo = ROOT.TH1D()
                if str(Ntuple.GetListOfBranches().FindObject(VarDict[type][0])) != '<ROOT.TObject object at 0x(nil)>' :
                    const = VarDict[type][0]
                    if 'Relative' in option:
                        const = 'Delta'+VarDict[type][1]
#                        if campaignId < 61 and type in ['NOISE','DELAY','PEDESTAL']: const = 'Delat'+VarDict[type][1]
                    c1 = ROOT.TCanvas()
                    n1 = Ntuple.Draw(const,cuts)
                    if n1!=0 :
                        histo = ROOT.gPad.GetPrimitive("htemp")
                        if 'Absolute' in option or 'Relative' in option:
                            return [[histo.GetMean(),0]]
                        else:
                            return [[histo.GetMean(),histo.GetRMS()]]

            else:
                graph = ROOT.TGraph()
                if str(Ntuple.GetListOfBranches().FindObject(VarDict[type][0])) != '<ROOT.TObject object at 0x(nil)>' :
                    const = VarDict[type][0]
                    if 'Relative' in option:
                        const = 'Delta'+VarDict[type][1]
#                        if campaignId < 61 and type in ['NOISE','DELAY','PEDESTAL']: const = 'Delat'+VarDict[type][1]
                        if 'Noise' in option :
                            if type == 'NOISE': const += '/' + VarDict[type][1]
                        if type=='NOISE' and 'Noise' in option: const = '1000*'+const
                        if type=='RAMP' : const = '1000*'+const
                        if type=='DELAY': const = '1000*'+const


                    c1 = ROOT.TCanvas()
                    n1 = Ntuple.Draw(const+':13*FT+slot-2',cuts)
                    if n1!=0 :
                        graph = ROOT.gPad.GetPrimitive("Graph");
                        list = {}
                        for i in range(graph.GetN()):
                            x = ROOT.Double(0)
                            y = ROOT.Double(0)
                            graph.GetPoint(i,x,y)
                            if not(x in list.keys()): list[x]=[]
                            list[x].append(y)
                        res = []
                        for x in list.keys():
                            m=0
                            for y in list[x]:
                                m += y
                            res.append([m/len(list[x]),0])
                        return res
            return None


#############################################################################
        def DrawFEBPlots(self,  sub_sub_partition, type, gain, campaignId, option=''):

            VarDict = {'AUTOCORR':['Autocorr','Autocorr'],'PEDESTAL':['ped','Ped'],'NOISE':['NOISE','NOISE'],'RAMP':['RAMP','RAMP'],'DELAY':['DELAY','DELAY']}
            thresh = thresholdDict[sub_sub_partition][gain][thresholdOrder[type+'FEB']]

            subPart = ''
            for k,j in listOfSubSubPart.iteritems():
                if sub_sub_partition in j:
                    subPart = k
            part = ''
            for k,j in listOfSubPart.iteritems():
                if subPart in j:
                    part = k

            # get the Ntuple
            file = '%s/Campaign_%d/Ntuple_%s_%s_%s_%d.root' % (resBasePath, campaignId, part, type, gain, campaignId)

            if not(os.path.exists(file)):
                #Messages.Out('ERROR', "Can't get the validation histo. File"+file+' does not exist.')
                return None
            rootFile = ROOT.TFile(file)
            nomNtuple = 'Ntuple_%s' % (sub_sub_partition)
            Ntuple = rootFile.Get(nomNtuple)

            campDict = self.engine.Execute("SELECT * FROM campaign WHERE id=%d" % campaignId)[0]

            if os.environ.has_key('ECalFigures'):
               figName= os.environ['ECalFigures'] + '/FEBPlot_%s_%s_%s_%dvs%d' % (sub_sub_partition, type, gain, campDict['id'],campDict['ref'])
            else:
               figName = '/afs/cern.ch/user/l/larcalib/w0/data/Validation/Figures/FEBPlot_%s_%s_%s_%dvs%d' % (sub_sub_partition, type, gain, campDict['id'],campDict['ref'])
            if not('/afs/cern.ch/user/l/larcalib/public/Validation_run/' in dbBasePath) : figName = figName+'_user'

            h_res = ROOT.TH2F()
            if 'Absolute' in option :
                h_res  = ROOT.TH2F("h_FEB_%s_%s_%s"%(sub_sub_partition, type, gain),"h_FEB_%s_%s_%s"%(sub_sub_partition, type, gain) , 416, 0.0, 416.0, 400, 800,1200)
            else:
                h_res  = ROOT.TH2F("h_FEB_%s_%s_%s"%(sub_sub_partition, type, gain),"h_FEB_%s_%s_%s"%(sub_sub_partition, type, gain) , 416, 0.0, 416.0, 500, -2 * thresh, +2 * thresh)

            histo = ROOT.TH1D()
            ok = str(Ntuple.GetListOfBranches().FindObject(VarDict[type][0])) != '<ROOT.TObject object at 0x(nil)>'
            const = 'Delta'+VarDict[type][1]
            if 'Absolute' in option : const = VarDict[type][0]

            part_val = []
            if ok :
                cuts = subSubPartSelector(sub_sub_partition)
                #                    cuts += ' && badChan==0'
                n1 = Ntuple.Draw(const,cuts)
                if n1!=0 :
                    histo = ROOT.gPad.GetPrimitive("htemp")
                    part_val = [histo.GetMean(),histo.GetRMS()]
                    #                    print '-->',part_val

            for islot in range(14):
                islot+=1
                for iFT in range(31):
                    iFT+=1

                    # Get the histo and return the mean value
                    cuts = subSubPartSelector(sub_sub_partition)
                    cuts += ' && FT=='  +str(iFT)
                    cuts += ' && slot=='+str(islot)
                    #                    cuts += ' && badChan==0'

                    if ok :
                        const = 'Delta'+VarDict[type][1]
                        if 'Absolute' in option : const = VarDict[type][0]
                        n1 = Ntuple.Draw(const,cuts)
                        if n1!=0 :
                            histo = ROOT.gPad.GetPrimitive("htemp")
                            val = [histo.GetMean(),histo.GetRMS()]
                            if 'Absolute' not in option:
                                if abs(val[0])>thresh:
                                    print ' %s FT%d slot%d deviates mean = %f (thresh = %f)' % (sub_sub_partition,iFT,islot,val[0],thresh)
                            h_res.Fill(13*iFT + islot-2,val[0])
                            #                            print '>',val


            Fig = ROOT.TCanvas()
            h_res.Draw()

            texte = ROOT.TText()
            texte.SetTextSize(0.04)
            texte.DrawTextNDC(0.38,0.85,type+'   '+sub_sub_partition+'    '+gain+' gain')
            texte.DrawTextNDC(0.35,0.80,'Campaign '+str(campDict['id']))
            texte.DrawTextNDC(0.35,0.75,'Reference '+str(campDict['ref']))

            #...draw the mean value of the parition
            line  = ROOT.TLine(0,part_val[0],416,part_val[0])
            linep = ROOT.TLine(0,part_val[0]+part_val[1],416,part_val[0]+part_val[1])
            linem = ROOT.TLine(0,part_val[0]-part_val[1],416,part_val[0]-part_val[1])
            line.Draw()
            for l in [linep,linem]:
                l.SetLineStyle(2)
                l.Draw()


            Fig.Update()

            Fig.SaveAs(figName+'.png')
            Fig.SaveAs(figName+'.eps')

            if 'display' in option : raw_input("Press Enter To Continue: ")

            return [figName+'.png',figName+'.eps']

#############################################################################
        def GetStabilityPlots(self,  sub_sub_partition, type, gain, campType='ALL', flag='GREEN', field='ALL', FT=-1, slot=-1, channel=-1, option='', idMin=-1, idMax=-1):

            if idMin==-1: idMin = 2
            if idMax==-1: idMax = self.GetCampaignId()

            #...Choose the thresh for axis range
            if sub_sub_partition in ['HEC','FCAL','EMBPS']:
                thresh = thresholdDict[sub_sub_partition+'A'][gain][thresholdOrder[type]]
            elif sub_sub_partition=="EM":
                thresh = thresholdDict[sub_sub_partition+'BA'][gain][thresholdOrder[type]]
            else:
                thresh = thresholdDict[sub_sub_partition][gain][thresholdOrder[type]]

            # Get the shifts corresponding to the campaings and type
            cuts = ''
            cuts += 'id > %d and id < %d ' % (idMin,idMax)
            if   flag=='ALL'   : cuts += ' and (flag="GREEN" or flag="YELLOW" or flag="RED")'
            elif flag=='GOOD'  : cuts += ' and (flag="GREEN" or flag="YELLOW")'
            else               : cuts += ' and (flag="%s")' % flag
            if campType=='ALL' : cuts += ' and (type="WEEKLY" or type="DAILY")'
            else               : cuts += ' and (type="%s")' % campType
            if field!='ALL'    : cuts += ' and (toroid_status="%s" and solenoid_status="%s")'%(field.split('-')[0],field.split('-')[1])

            campaignList = self.engine.Execute('SELECT id,flag,type,date FROM campaign WHERE '+cuts)

            groupedPartition = {'EM':['EMBA','EMBC','EMECA','EMECC','EMECPSA','EMECPSC'],
                                'EMBPS':['EMBPSA','EMBPSC'],
                                'HEC':['HECA','HECC'],
                                'FCAL':['FCALA','FCALC']}

            points = []
            for campaign in campaignList:
                nom = '%s_%s' % (campaign['type'], campaign['flag'])

#                if type=='DELAY' and campaign['type']=='DAILY': continue

                id   = campaign['id']
                date = campaign['date']

                Val = None
                if 'Absolute' in option or 'Relative' in option:
                    # Avoid troubles with composite campaign and delay runs in daily campaigns ...
                    if sub_sub_partition in groupedPartition.keys():
                        n=0
                        mean = 0
                        err  = 0
                        for sspart in groupedPartition[sub_sub_partition]:
                            if 'PerFeb' in option:
                                listVal = self.GetConstValue(sspart, type, gain, id, FT, slot, channel, option)
                                if listVal!=None:
                                    for tempVal in listVal:
                                        points.append([nom,id, tempVal[0],tempVal[1],date])
                            else:
                                tempVal = self.GetConstValue(sspart, type, gain, id, FT, slot, channel, option)
                                if tempVal != None:
                                    tempVal = tempVal[0]
                                    mean += tempVal[0]
                                    err  += tempVal[1]
                                    n += 1
                        if n>0:
                            Val = [mean/n, err/n]
                        else:
                            Val =None
                    else:
                        listVal = self.GetConstValue(sub_sub_partition, type, gain, id, FT, slot, channel, option)

                        if listVal!=None:

                            for tempVal in listVal:
                                points.append([nom,id, tempVal[0],tempVal[1],date])

                elif 'Shift' in option:
                     Val = self.GetShift(sub_sub_partition, type, gain, id)
                else :
                    Messages.Out('INFO', 'Shift option is used by default')
                    Val = self.GetShift(sub_sub_partition, type, gain, id)

                if Val!=None and not('PerFeb' in option):
                    if ('NoErr' in option) :
                        points.append([nom,id, Val[0],0,date])
                    else:
                        points.append([nom,id, Val[0],Val[1],date])

            return points

#############################################################################
        def DrawStabilityPlots(self,  sub_sub_partition, type, gain, campType='ALL', flag='GREEN', field='ALL', FT=-1, slot=-1, channel=-1, option='', idMin=-1, idMax=-1):

            #...Choose the thresh for axis range
            if sub_sub_partition in ['HEC','FCAL','EMBPS']:
                thresh = thresholdDict[sub_sub_partition+'A'][gain][thresholdOrder[type]]
            elif sub_sub_partition=="EM":
                thresh = thresholdDict[sub_sub_partition+'BA'][gain][thresholdOrder[type]]
            else:
                thresh = thresholdDict[sub_sub_partition][gain][thresholdOrder[type]]

            subPart = ''
            for k,j in listOfSubSubPart.iteritems():
                if sub_sub_partition in j:
                    subPart = k
            part = ''
            for k,j in listOfSubPart.iteritems():
                if subPart in j:
                    part = k


            if os.environ.has_key('ECalFigures'):
               figName= os.environ['ECalFigures'] + '/Stability_%s_%s_%s_%s_%s_%s_FT%d_Sl%d_Ch%d' % (sub_sub_partition, type, gain, campType, flag, field, FT, slot, channel)
            else:
               figName = '/afs/cern.ch/user/l/larcalib/w0/data/Validation/Figures/Stability_%s_%s_%s_%s_%s_%s_FT%d_Sl%d_Ch%d' % (sub_sub_partition, type, gain, campType, flag, field, FT, slot, channel)
            if not('/afs/cern.ch/user/l/larcalib/public/Validation_run/' in dbBasePath) : figName = figName+'_user'


            ##########################################################################################
            #... Get the points
            points = self.GetStabilityPlots(sub_sub_partition, type, gain, campType, flag, field, FT, slot, channel, option, idMin, idMax)
            yMin =  9e99
            yMax = -9e99
            xMin =  9e99
            xMax = -9e99
            for blob in points:
                id   = int(blob[1])
                val  = float(blob[2])
                date = blob[4]
                x=id
                if 'VsDate' in option: x=date
                if yMin>val: yMin=val
                if yMax<val: yMax=val
                if xMin>x  : xMin=x
                if xMax<x  : xMax=x


            Fig = ROOT.TCanvas()
            ##########################################################################################
            #... Normal drawing
            if not('Official' in option):
                #... Prepare the TGraph
                nb={}
                graph = {}
                graph['ALL'] = ROOT.TGraphErrors()
                nb['ALL']    = 0
                for blob in points:
                    nom  = blob[0]
                    id   = int(blob[1])
                    val  = float(blob[2])
                    err  = float(blob[3])
                    date = blob[4]

                    x=id
                    if 'VsDate' in option: x=date

                    if not(nom in graph.keys()):
                        graph[nom] = ROOT.TGraphErrors()
                        nb[nom]    = 0

                    graph[nom]  .SetPoint(nb[nom],x, val)
                    graph[nom]  .SetPointError(nb[nom],0, err)
                    graph['ALL'].SetPoint(nb['ALL'],x, val)
                    graph['ALL'].SetPointError(nb['ALL'],0, err)
                    nb[nom]  +=1
                    nb['ALL']+=1

                graph['ALL'].GetXaxis().SetTitle('Campaign Id')
                if not('Absolute' in option):
                    graph['ALL'].GetYaxis().SetRangeUser(-0.1*thresh,0.1*thresh)

                if 'Absolute' in option:
                    graph['ALL'].GetYaxis().SetTitle(type)
                else:
                    graph['ALL'].GetYaxis().SetTitle('Delta '+type)

                #...Plots the graph
                graph0 = graph['ALL']
                graph0.SetMarkerStyle(1)
                graph0.SetMarkerColor(0)
                graph0.Draw("AP")

                if 'VsDate' in option:
                    graph0.GetXaxis().SetTimeDisplay(1)
                    graph0.GetXaxis().SetTimeFormat("#splitline{%d\/%m}{%Y}")
                    graph0.GetXaxis().SetTimeOffset(0,"gmt");
                    graph0.GetYaxis().SetNdivisions(10 + 100*5 +10000*0)
                    graph0.GetXaxis().SetLabelSize(0.03)
                    graph0.GetXaxis().SetLabelOffset(0.017)
                    graph0.GetXaxis().SetTitle("Date")

                fact = 1
                if type in ['DELAY','RAMP']:
                    yMin = yMin-fact*thresh*yMin
                    yMax = yMax+fact*thresh*yMax
                elif 'Shift' in option:
                    if yMin>-0.05: yMin = -0.05
                    if yMax< 0.05: yMax =  0.05
                else:
                    yMin = yMin-fact*thresh
                    yMax = yMax+fact*thresh

                graph['ALL'].GetYaxis().SetRangeUser(yMin,yMax)
                graph['ALL'].GetXaxis().SetRangeUser(1,10000)

                for k,g in graph.iteritems():
                    if "WEEKLY" in k: g.SetMarkerStyle(20)
                    if "DAILY"  in k: g.SetMarkerStyle(23)
                    if "RED"    in k: g.SetMarkerColor(ROOT.kRed)
                    if "YELLOW" in k: g.SetMarkerColor(ROOT.kOrange)
                    if "GREEN"  in k: g.SetMarkerColor(ROOT.kGreen)
                    g.SetMarkerSize(1.5)
                    g.Draw("P")

                #... Draw the DB update
                updateList = self.engine.Execute('SELECT id,date FROM campaign WHERE (db="YES" or db="CURRENT") and id>2')
                for update in updateList:
                    x=update['id']
                    if 'VsDate' in option: x=update['date']
                    ligne = ROOT.TLine()
                    ligne.SetLineColor(2)
                    ligne.SetLineWidth(2)
                    if x>xMin and x<xMax:
                        ligne.DrawLine(x,yMin,x,yMax)

                texte = ROOT.TText()
                texte.SetTextSize(0.04)
                texte.DrawTextNDC(0.18,0.94,type+'   '+sub_sub_partition+'    '+gain+' gain')
                texte.DrawTextNDC(0.18,0.90,'Colors = campaign flags')
                texte.DrawTextNDC(0.18,0.86,'Triangle = Daily')
                texte.DrawTextNDC(0.18,0.82,'Circle = Weekly')
                if type in ['PEDESTAL','NOISE','RAMP']:
                    texte.DrawTextNDC(0.18,0.78,'Y range set to +-%d threshold'%fact)

            ##########################################################################################
            #... Official plots for approval
            else:

                nbiny = 200
                htmp = ROOT.TH1D("htmp","htmp",nbiny,yMin,yMax)
                for blob in points:
                    val  = float(blob[2])
                    htmp.Fill(val)

                #...Prepare the TH2D
                yMin = htmp.GetMean() - 8*htmp.GetRMS()
                yMax = htmp.GetMean() + 8*htmp.GetRMS()

                hprof   = ROOT.TProfile("hprof","hprof",200,xMin,xMax,yMin,yMax)
                histo2d = ROOT.TH2D("histo","histo",200,xMin,xMax,nbiny,yMin,yMax)
                nbFEB = {}
                for blob in points:
                    nom  = blob[0]
                    id   = int(blob[1])
                    val  = float(blob[2])
                    err  = float(blob[3])
                    date = blob[4]
                    if not(id in nbFEB.keys()): nbFEB[id]=0
                    nbFEB[id] += 1
                    x=id
                    if 'VsDate' in option: x=date
                    histo2d.Fill(x,val)
                    hprof.Fill(x,val)

                #...Count the number of FEB
                nbFEB[0] = 0
                for i in nbFEB.values():
                    nbFEB[0] += i
                print 'Count the number of FEB ',nbFEB[0],' ',len(nbFEB)

                nbFEB[0] = nbFEB[0] / (len(nbFEB)-1)

                p1 = ROOT.TPad("p1","p1",0,0,0.8,1)
                p1.Draw()
                p2 = ROOT.TPad("p2","p2",0.8,0,1,1)
                p2.Draw()

                #...Draw the graph
                p1.cd()
                ROOT.gStyle.SetPalette(1)
                ROOT.gPad.SetRightMargin(0.1)
                hprof.SetLineWidth(1)
                hprof.SetMarkerStyle(5)
                hprof.SetMarkerSize(1)
                histo2d.Draw("colz")
                hprof.Draw("same")
                histo2d.GetZaxis().SetLabelSize(0.025)

                if type=='DELAY':
                    histo2d.GetYaxis().SetTitle('#Delta %s/%s (per mil)'%(type,type))
                if type=='RAMP':
                    histo2d.GetYaxis().SetTitle('#Delta %s/%s (per mil)'%(type,type))
                elif type=='NOISE' and 'Noise' in option:
                    histo2d.GetYaxis().SetTitle('#Delta %s/%s (per mil)'%(type,type))
                else:
                    histo2d.GetYaxis().SetTitle('#Delta %s (ADC count)'%(type))
                histo2d.GetYaxis().SetTitleOffset(1.3)

                if 'VsDate' in option:
                    histo2d.GetXaxis().SetTimeDisplay(1)
                    histo2d.GetXaxis().SetTimeFormat("%d.%m")
                    histo2d.GetXaxis().SetTimeOffset(0,"gmt");
                    histo2d.GetYaxis().SetNdivisions(8 + 100*0 +10000*0)
                    histo2d.GetXaxis().SetLabelSize(0.03)
                    histo2d.GetXaxis().SetLabelOffset(0.017)
                    histo2d.GetXaxis().SetTitle("Date in 2011")

                AtlasStyle.DrawATLASLabel(0.17,0.15)
                texte = ROOT.TText()

                if 'FCAL' in sub_sub_partition:
                    sub_sub_partition = sub_sub_partition.replace('FCAL','FCal')

                texte.SetTextSize(0.04)
                texte.DrawTextNDC(0.17,0.88,'LAr %s'  % sub_sub_partition )
                texte.DrawTextNDC(0.67,0.88,'%s Gain' % {'H':'High','M':'Medium','L':'Low'}[gain] )
                texte.SetTextSize(0.032)
                texte.DrawTextNDC(0.17,0.84,'%d FEBs' % nbFEB[0] )

                #... Draw the DB update
                updateList = self.engine.Execute('SELECT id,date FROM campaign WHERE (db="YES" or db="CURRENT") and id>2')
                for update in updateList:
                    x=update['id']
                    if 'VsDate' in option: x=update['date']
                    ligne = ROOT.TLine()
                    ligne.SetLineColor(2)
                    ligne.SetLineWidth(2)
                    if x>xMin and x<xMax:
                        ligne.DrawLine(x,yMin,x,yMax)

                #... Draw the projection
                p2.cd()
                histo = ROOT.TH1D("proj","proj",nbiny,yMin,yMax)
                for blob in points:
                    val  = float(blob[2])
                    histo.Fill(val)
                histo.SetFillColor(4)
                if 'PerFeb' in option: histo.GetYaxis().SetTitle("Nb FEBs")
                histo.GetYaxis().SetNdivisions(3 + 100*5 +10000*0)
                histo.GetYaxis().SetLabelSize(0.09)
                histo.GetYaxis().SetLabelOffset(-0.03)
                histo.GetYaxis().SetTitleSize(0.09)
                histo.GetYaxis().SetTitleOffset(0.5)
                histo.Draw("hbar")
                texte.SetTextSize(0.075)
                texte.DrawTextNDC(0.27,0.88,'Overflow=%d' % histo.GetBinContent(nbiny+1))
                texte.DrawTextNDC(0.27,0.85,'Underflow=%d' % histo.GetBinContent(0))
                texte.SetTextFont(62)

                if type=='DELAY':
                    texte.DrawTextNDC(0.27,0.79,'RMS=%1.3f per mil' % histo.GetRMS())
                if type=='RAMP':
                    texte.DrawTextNDC(0.27,0.79,'RMS=%1.3f per mil' % histo.GetRMS())
                elif type=='NOISE' and 'Noise' in option:
                    texte.DrawTextNDC(0.27,0.79,'RMS=%1.3f per mil' % histo.GetRMS())
                else:
                    texte.DrawTextNDC(0.27,0.79,'RMS=%1.3f ADC' % histo.GetRMS())


            Fig.Update()
            Fig.SaveAs(figName+'.png')
            Fig.SaveAs(figName+'.eps')
            if 'display' in option or 'Display' in option : raw_input("Press Enter To Continue: ")
            return [figName+'.png',figName+'.eps']

###############################################################################################
        def GetCampaignPartition(self,Id):
            runList = self.engine.Execute('SELECT partition FROM run WHERE campaign=%d'%Id)
            part=''
            list=[]
            for i in runList:
                if not(i['partition'] in list) : list.append(str(i['partition']))
            list.sort()
            return list


################################################################################################
        def SuspiciousFebFinder(self, sub_sub_partition, nDevMin=0, flag='RED'):

            #...Count the number of campaigns including the sub_sub_partition
            CampDictList    = self.engine.Execute('SELECT id FROM Campaign  WHERE flag!="%s"'%flag)
            CampIdList = []
            SubsubPartitionList = []
            for campDict in CampDictList:
                id = campDict['id']
                PartitionList = self.GetCampaignPartition(id)

                for p in PartitionList:
                    for sp in listOfSubPart[p]:
                         for ssp in listOfSubSubPart[sp]:
                                SubsubPartitionList.append(ssp)

                if sub_sub_partition in SubsubPartitionList:
                       CampIdList.append(id)
                CampIdList.sort()

                  #...Get the deviations for the sub_sub_partition and count the occurence
            DelayPbList = {}
            ScaPbList   = {}
            for FT in range(0,32):
                for slot in range(1,16):
                    for id in CampIdList:
                        ChannelDictList  = self.engine.Execute('SELECT * FROM Channel WHERE sub_sub_partition="%s" and ft=%d and sl=%d and campaign=%d ORDER BY ch, campaign' % (sub_sub_partition,FT,slot,id))
                        #print ChannelDictList
                        for channel in ChannelDictList:
                            if len(ChannelDictList)>0: #... This feb deviate for this campaign
                                if not('%s_%d_%d_%d'%(sub_sub_partition,FT,slot,channel['ch']) in DelayPbList.keys()):
                                    DelayPbList['%s_%d_%d_%d'%(sub_sub_partition,FT,slot,channel['ch'])] = []
                                if not('%s_%d_%d_%d'%(sub_sub_partition,FT,slot,channel['ch']) in ScaPbList.keys())  :
                                    ScaPbList  ['%s_%d_%d_%d'%(sub_sub_partition,FT,slot,channel['ch'])] = []
                                #...How ?
                                Dev = {}
                                Dev['ped']   = []
                                Dev['noise'] = []
                                Dev['delay'] = []
                                Dev['ramp']  = []
                                Dev['auto']  = []

                                def computeDevMean(dev, thr, res):
                                    if dev!=None and dev!=0:
                                        sigma = abs(dev)/thr
                                        res.append(sigma)


                                #...H gain
                                if not('HEC' in sub_sub_partition):
                                    computeDevMean(channel['deviation1_h'],thresholdDict[sub_sub_partition]['H'][4],Dev['auto'])
                                    computeDevMean(channel['deviation3_h'],thresholdDict[sub_sub_partition]['H'][0],Dev['ped'])
                                    computeDevMean(channel['deviation4_h'],thresholdDict[sub_sub_partition]['H'][1],Dev['noise'])
                                    computeDevMean(channel['deviation5_h'],thresholdDict[sub_sub_partition]['H'][8],Dev['ramp'])
                                    computeDevMean(channel['deviation7_h'],thresholdDict[sub_sub_partition]['H'][6],Dev['delay'])
                                #...M gain
                                computeDevMean(channel['deviation1_m'],thresholdDict[sub_sub_partition]['M'][4],Dev['auto'])
                                computeDevMean(channel['deviation3_m'],thresholdDict[sub_sub_partition]['M'][0],Dev['ped'])
                                computeDevMean(channel['deviation4_m'],thresholdDict[sub_sub_partition]['M'][1],Dev['noise'])
                                computeDevMean(channel['deviation5_m'],thresholdDict[sub_sub_partition]['M'][8],Dev['ramp'])
                                computeDevMean(channel['deviation7_m'],thresholdDict[sub_sub_partition]['M'][6],Dev['delay'])
                                #...L gain
                                computeDevMean(channel['deviation1_l'],thresholdDict[sub_sub_partition]['L'][4],Dev['auto'])
                                computeDevMean(channel['deviation3_l'],thresholdDict[sub_sub_partition]['L'][0],Dev['ped'])
                                computeDevMean(channel['deviation4_l'],thresholdDict[sub_sub_partition]['L'][1],Dev['noise'])
                                computeDevMean(channel['deviation5_l'],thresholdDict[sub_sub_partition]['L'][8],Dev['ramp'])
                                computeDevMean(channel['deviation7_l'],thresholdDict[sub_sub_partition]['L'][6],Dev['delay'])

                                def ComputeMean(list):
                                    re = 0.0
                                    n  = 0
                                    for t in list:
                                        if Dev[t]!=[]:
                                            for d in Dev[t]:
                                                if d!=0:
                                                    re += d
                                                    n += 1
                                    re = re / n
                                    return re

                                ScaPbmean = 0
                                DelayPbmean = 0
                                if Dev['ped']!=[] or Dev['noise']!=[] or Dev['auto']!=[]:
                                    ScaPbmean = ComputeMean(['ped','noise','auto'])
                                    ScaPbList['%s_%d_%d_%d'%(sub_sub_partition,FT,slot,channel['ch'])].append([id,ScaPbmean])
                                if Dev['delay']!=[] or Dev['ramp']!=[]:
                                    DelayPbmean = ComputeMean(['delay','ramp'])
                                    DelayPbList['%s_%d_%d_%d'%(sub_sub_partition,FT,slot,channel['ch'])].append([id,DelayPbmean])
                                mean = max(ScaPbmean, DelayPbmean)


            #...compute the campaign frequency
            result = {}
            def FillRes(PbList, PbType):
                chList = PbList.keys()
                chList.sort()
                for channel in chList:
                    blob = PbList[channel]
                    ids   = [i[0] for i in blob]
                    if ids==[]: continue
                    ids.sort()

                    #...compute the average sigma for the deviation
                    means = [i[1] for i in blob]
                    mean = 0
                    for m in means: mean+=m
                    mean = mean / len(means)


                    #...compute the number of deviation
                    cmptmp = []
                    for i in CampIdList:
                        if i>=ids[0]: cmptmp.append(i)
                    if cmptmp==[]: continue
                    pc = float(len(ids)) / float(len(cmptmp))

                    #...select suspicious Febs
                    if len(ids) < nDevMin : continue
                    if (pc>0.3 and len(ids)>2) or (pc>0.1 and mean>20) or (pc>0.2 and mean>3.0):
                        FT   = int(channel.split('_')[1])
                        slot = int(channel.split('_')[2])
                        if not('%s_%d_%d'%(sub_sub_partition,FT,slot) in result.keys()):
                            ch ='%s_%d_%d'%(sub_sub_partition,FT,slot)
                            result[ch] = {}
                            result[ch]['ScaPb']  =[]
                            result[ch]['DelayPb']=[]

                        result['%s_%d_%d'%(sub_sub_partition,FT,slot)][PbType].append({'channel':channel,'devcamp':ids,'mean':mean,'frequency':pc,'campaigns':CampIdList})

            FillRes(DelayPbList,'DelayPb')
            FillRes(ScaPbList,'ScaPb')

            return result

#####################################################################################
        def SuspiciousFebFormat(self,data):
            for i,j in data.iteritems():
                print i
                for k in j['ScaPb']:   print 'ScaPb    %16s \t av. dev=%2.1f\t Freq=%2.1f pc.\tOcc.=%d / %d' %(k['channel'],k['mean'],k['frequency']*100,len(k['devcamp']),len(k['campaigns'])),'   ','https://lar-elec-automatic-validation.web.cern.ch/lar-elec-automatic-validation/cgi-bin/History.sh?sub_sub_partition=%s&ft=%s&sl=%s&ch=%s&printFormat=Ratio' % (k['channel'].split('_')[0],k['channel'].split('_')[1],k['channel'].split('_')[2],k['channel'].split('_')[3])
                for k in j['DelayPb']: print 'DelayPb  %16s \t av. dev=%2.1f\t Freq=%2.1f pc.\tOcc.=%d / %d' %(k['channel'],k['mean'],k['frequency']*100,len(k['devcamp']),len(k['campaigns'])),'   ','https://lar-elec-automatic-validation.web.cern.ch/lar-elec-automatic-validation/cgi-bin/History.sh?sub_sub_partition=%s&ft=%s&sl=%s&ch=%s&printFormat=Ratio' % (k['channel'].split('_')[0],k['channel'].split('_')[1],k['channel'].split('_')[2],k['channel'].split('_')[3])

#####################################################################################
        def MergeCampaignsFilesById( self, campaignIDs ):
            mergedCampaignIdFinal = max(campaignIDs)
            for Id in campaignIDs:
                if Id==mergedCampaignIdFinal: continue
                print resBasePath
                directory = '%s/Campaign_%d/' % (resBasePath,Id)
                for fileIn in os.listdir(directory):
                    fileIn = directory+fileIn
                    fileOut = fileIn.replace(str(Id),str(mergedCampaignIdFinal))
                    if not(os.path.exists(fileOut)):
                        command = 'mv %s %s' %(fileIn,fileOut)
                        os.system(command)
                    else:
                        print 'File',fileOut,'already exist.'
                os.system('rmdir '+directory)


#####################################################################################
        def MergeCampaignsById( self, campaignIDs ):

            self.MergeCampaignsFilesById(campaignIDs)

            mergedCampaignIdTemp = self.GetCampaignId()
            mergedCampaignIdFinal = max(campaignIDs)
            newCampaign = dict()
            campaigns = dict()

            import DQFlags

            for curId in campaignIDs:
                try:
                    campaigns[curId] =  self.engine.Execute('''
                        SELECT * FROM campaign WHERE
                        id = '%d'
                    ''' % (curId))
                except AttributeError:
                    raise Exception('DB not opened')

            for attribute, value in campaigns[campaignIDs[0]][0].iteritems():
                if(attribute == 'id'):
                    newCampaign[attribute] = mergedCampaignIdTemp
                elif(attribute == 'composite'):
                    newCampaign[attribute] = 'YES'
                else:
                    newCampaign[attribute] = value

            for curId in campaignIDs:
                for attribute, value in campaigns[curId][0].iteritems():
                    if(attribute == 'id' or attribute == 'composite'):
                        continue
                    elif(attribute == 'date' and value < newCampaign[attribute]):
                        newCampaign[attribute] = value
                    elif(attribute == 'ref' or attribute == 'type'):
                        if(newCampaign[attribute] != value):
                            print "Error: Cannot Merge campaigns with different", attribute
                            return
                    elif(attribute == 'db'):
                        if(value == 'CURRENT' or value == 'YES'):
                          newCampaign[attribute] = value
                    elif(attribute == 'flag' or attribute == 'dq_flag'):
                        if DQFlags.mesure[value] > DQFlags.mesure[newCampaign[attribute]]:
                            newCampaign[attribute] = value
                    elif(attribute == 'toroid_status' or attribute == 'solenoid_status' and newCampaign[attribute] != 'MIDDLE'):
                        if(value != newCampaign[attribute]):
                            newCampaign[attribute] = 'MIDDLE'


            self.AddCampaignEntry(newCampaign)
            for curId in campaignIDs:
                try:
                    channelList = self.engine.Execute('''
                        SELECT * FROM Channel WHERE
                            campaign = '%d'
                        ''' % (curId))

                except AttributeError:
                    raise Exception('DB not opened')

                for channel in channelList:
                    channel['campaign'] = mergedCampaignIdTemp
                    nonValueAttributes = list()

                    for attribute,value in channel.iteritems():
                        if(value == None):
                            nonValueAttributes.append(attribute)
                    for attribute in nonValueAttributes:
                        del channel[attribute]

                    self.AddChannelEntry(channel)

                try:
                    febList = self.engine.Execute('''
                        SELECT * FROM Feb WHERE
                            campaign = '%d'
                        ''' % (curId))
                except AttributeError:
                    raise Exception('DB not opened')

                for feb in febList:
                    feb['campaign'] = mergedCampaignIdTemp
                    nonValueAttributes = list()

                    for attribute,value in feb.iteritems():
                        if(value == None):
                            nonValueAttributes.append(attribute)
                    for attribute in nonValueAttributes:
                        del feb[attribute]

                    self.AddFebEntry(feb)


                try:
                    self.engine.Execute('''
                        UPDATE Run SET campaign = '%d' WHERE
                            campaign = '%d'
                        ''' % (mergedCampaignIdTemp, curId))
                except AttributeError:
                    raise Exception('DB not opened')

                self.RemoveCampaignEntryById(curId)


            try:
                self.engine.Execute('''
                    UPDATE campaign SET id = '%d' WHERE
                        id = '%d'
                    ''' % (mergedCampaignIdFinal, mergedCampaignIdTemp))
            except AttributeError:
                raise Exception('DB not opened')


            try:
                self.engine.Execute('''
                    UPDATE Run SET campaign = '%d' WHERE
                        campaign = '%d'
                    ''' % (mergedCampaignIdFinal, mergedCampaignIdTemp))
            except AttributeError:
                raise Exception('DB not opened')

            try:
                self.engine.Execute('''
                    UPDATE Feb SET campaign = '%d' WHERE
                        campaign = '%d'
                    ''' % (mergedCampaignIdFinal, mergedCampaignIdTemp))
            except AttributeError:
                raise Exception('DB not opened')

            try:
                self.engine.Execute('''
                    UPDATE Channel SET campaign = '%d' WHERE
                        campaign = '%d'
                    ''' % (mergedCampaignIdFinal, mergedCampaignIdTemp))
            except AttributeError:
                raise Exception('DB not opened')

            return mergedCampaignIdFinal
