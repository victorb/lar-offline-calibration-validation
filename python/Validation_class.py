
# ----- System -----
import time
import commands
import subprocess
import sqlite3
import sys
import os
from datetime import datetime
from multiprocessing import Process, current_process

# ----- Locale -----
from ECalDB_core import ECalDB
from ECalDB_definitions import *
from ECalDB_utils import *
from Validation_definitions import *
from Validation_validator import Validator
from FebManage_class import FebManage

# ===== Validation =====
class Validation:
	def __init__(self, _verbose):
		# set verbosity
		self.verbose = _verbose

		self.val_python_dir = os.environ["VALIDATION_PYTHON_DIR"]
		self.name_lock_file = self.val_python_dir + "/validation.in_progress"
		self.name_time_file = self.val_python_dir + "/validation.timestamp"
		self.name_runs_file = self.val_python_dir + "/validation.last_runs"

		self.num_prev_runs = {"min":None, "max":None}
		self.num_curr_runs = {"min":None, "max":None}
		self.module_name = None
		self.module_runs = []
		self.list_last_runs = []

		self.val_ap_dir = os.environ["VALIDATION_APCONFIG_DIR"]
		self.val_db_dir = os.environ["VALIDATION_DATABASE_DIR"]

		self.found_camp = False
		self.with_ofc   = False
		self.mode_test  = False
		self.mode_force = False

		self.febs_swapped_list     = list()
		self.channels_swapped_dict = dict()
		self.mode_febs = False


	# ----- GetNameLockFile() -----
	def GetNameLockFile(self):
		return self.name_lock_file

	# ----- FonudCampaign() -----
	def FoundCampaign(self):
		return self.found_camp

	# ----- SetModeTest() -----
	def SetModeTest(self):
		self.mode_test = True

	# ----- SetForceMode() -----
	def SetForceMode(self):
		self.mode_force = True

	# ----- WithOFC() -----
	def WithOFC(self):
		self.with_ofc = True

	# ----- SetModeFeb() -----
	def SetModeFeb(self, _file):
		self.MakeSwapChannelsDict(_file)
		if len(self.channels_swapped_dict)>0:
			self.mode_febs = True


	# ----- UpdateRunInfoFile() -----
	def UpdateRunInfoFile(self):
		_file = open(self.name_runs_file, "w")
		_file.write("%d %d\n" % (int(self.num_curr_runs["min"]), int(self.num_curr_runs["max"])))
		_file.close()


	# ----- MakeTimeFile() -----
	def MakeTimeFile(self):
		if os.path.isfile(self.name_time_file):
			os.remove(self.name_time_file)
		_time_str = time.strftime(HUMAN_TIME_FORMAT, time.localtime()) + "\n"
		_file = open(self.name_time_file, 'w')
		_file.write(_time_str)
		_file.close()


	# ----- GetPrevRuns() -----
	def GetPrevRuns(self):
		if not os.path.isfile(self.name_runs_file):
			print LOW_INDENT,"ERROR: not found file %s" % self.name_runs_file
			return False

		if os.path.getsize(self.name_runs_file)>0:
			_file = open(self.name_runs_file, "r")
			self.num_prev_runs["min"], self.num_prev_runs["max"] = _file.readline().strip().split(' ')
			_file.close()
		else:
			print LOW_INDENT,"ERROR: the file %s is empty" % self.name_runs_file
			return False

		return True


	# ----- MakeRunList() -----
	def MakeRunList(self):
		_module_runs = {}

		try:
			_module = __import__(self.module_name[0:-3])
		except:
			print LOW_INDENT,"ERROR: could not import module %s" % self.module_name
			return False

		try:
			_reco_run_list = _module.sRecoRunList
		except:
			print LOW_INDENT,"ERROR: do not have sRecoRunList in the module %s" % _module
			return False

		if len(_reco_run_list)<2:
			print LOW_INDENT,"ERROR: short list of reco runs <-> %s" % _reco_run_list
			return False

		# now we construct the run list
		for _partGain, _runs in _reco_run_list:
			if _partGain[1] in _module_runs:
				_module_runs[_partGain[1]] += _runs.split("_")
			else:
				_module_runs[_partGain[1]] = _runs.split("_")

		if len(_module_runs)<1:
			print LOW_INDENT,"ERROR: zero length set <-> %s" % _module_runs
			return False

		# now we have to reorder the runs, and construct the integer list
		for _gain in ["HIGH","MEDIUM","LOW"]:
			if not _module_runs.has_key(_gain): continue # no LOW for EMBPS only
			for _i in range(0, len(_module_runs[_gain])):
				if not int(_module_runs[_gain][_i]) in self.module_runs:
					self.module_runs.append(int(_module_runs[_gain][_i]))

		return True

	# ----- CheckInputModule() -----
	def CheckInputModule(self, reco_file_out):
		if self.verbose["low"]:
			print LOW_INDENT,"Starting CheckInputModule() for ", reco_file_out
		if (not reco_file_out.startswith("CALIB")) or (not reco_file_out.endswith("Production_DONE.out")):
			print LOW_INDENT, "Wrong filename, should start with CALIB and end with Production_DONE.out"
			return False
		#_nFile = self.val_ap_dir + "/" + reco_file_out
		# CALIB_22-Oct-2018_09-25-34_EM-EMPS-HECFCAL_Daily_run_from_00364116_to_00364138_run1_Production_DONE.out
		#  (0)       (1)       (2)      (3)            (4) (5)  (6)   (7)    (8)   (9)   (10)    (11)      (12)
		reco_file_out_split = reco_file_out.split("_")
		self.num_curr_runs["min"] = reco_file_out_split[7]
		self.num_curr_runs["max"] = reco_file_out_split[9]
		# - check prevoius runs
		validation_done = False
		if not self.mode_force:
			if self.GetPrevRuns()==False:
				return False
			if self.verbose["med"]:
				print MED_INDENT,"found previous calibration campaign: nRunMin %s, nRunMax %s" \
					% (self.num_prev_runs["min"], self.num_prev_runs["max"])
			if int(self.num_prev_runs["min"])>=int(self.num_curr_runs["min"]):
				if int(self.num_prev_runs["max"])>=int(self.num_curr_runs["max"]):
					validation_done = True
		if not validation_done:
			self.found_camp = True
			_tmp_module = reco_file_out_split[0:-2]
			_module_name = ""
			for _part in _tmp_module:
				_module_name += _part + "_"
			self.module_name = _module_name + "lock.py"
			if self.MakeRunList()==False:
				return False
		if self.verbose["low"] and self.found_camp:
			print LOW_INDENT,"Calibration campaign was founded, range of runs is from %d to %d" \
				% (int(self.num_curr_runs["min"]), int(self.num_curr_runs["max"]))
			print MED_INDENT,"Calib module is %s" % self.module_name
			print MED_INDENT,"The calib module has %d runs: from %d to %d" \
				% (len(self.module_runs), min(self.module_runs), max(self.module_runs))
		return True


	# ----- MakeSwapChannelsDict() -----
	def MakeSwapChannelsDict(self, _name_file):
		try:
			_file = open(_name_file, 'r')
		except IOError:
			_file.close()
			raise ValueError("GetSwapChannelsDict(): ERROR file '%s' does not appear to exist" % _name_file)

		_swap_febs_cnt = 0
		for _line in _file:
			_tmp = _line.strip().split()
			if len(_tmp)>0:
				_new_feb_id = _tmp[0] # new (current) FEB online ID in a slot <-> LS2
				_old_feb_id = _tmp[1] # old (reference) FEB online ID in the same slot <-> Run2

				for _ch in range(0, 128):
					_new_ch_id = int(_new_feb_id, base=16) + (_ch<<8)
					_old_ch_id = int(_old_feb_id, base=16) + (_ch<<8)
					self.channels_swapped_dict[_new_ch_id] = _old_ch_id

				_swap_febs_cnt += 1

		if self.verbose["low"]:
			print LOW_INDENT,"So, %d swapped FEBS (%d channels) were found from %s" % (_swap_febs_cnt, len(self.channels_swapped_dict), _name_file)

		_file.close()


	# ----- FindNewCampaign() -----
	def FindNewCampaign(self):
		if self.verbose["low"]:
			print LOW_INDENT,"Starting FindNewCampaign()"

		if self.GetPrevRuns()==False:
			return False
		if self.verbose["med"]:
			print MED_INDENT,"found previous calibration campaign: nRunMin %s, nRunMax %s" \
				% (self.num_prev_runs["min"], self.num_prev_runs["max"])

		_out_dict = {}
		for _nFile in os.listdir(self.val_ap_dir):
			if os.path.isfile(_nFile): continue
			if not _nFile.startswith("CALIB"): continue
			if not _nFile.endswith("Production_DONE.out"): continue

			# CALIB_22-Oct-2018_09-25-34_EM-EMPS-HECFCAL_Daily_run_from_00364116_to_00364138_run1_Production_DONE.out
			#  (0)       (1)       (2)      (3)            (4) (5)  (6)   (7)    (8)   (9)   (10)    (11)      (12)
			_tmp = _nFile.split("_")
			#if _tmp[4]=='Daily': continue # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			_timeStamp = time.mktime(time.strptime(_tmp[1] + " " + _tmp[2], "%d-%b-%Y %H-%M-%S"))
			_out_dict[_timeStamp] = _nFile

		_keys = _out_dict.keys()
		_keys.sort()
		_out_list = [_out_dict[_key] for _key in _keys]

		_tmp_last = _out_list[-1].split("_")
		self.num_curr_runs["min"] = _tmp_last[7]
		self.num_curr_runs["max"] = _tmp_last[9]

		if self.verbose["low"]:
			_isBigList = False
			if len(_keys)>10: _isBigList = True;
			_loop = 1
			print LOW_INDENT,"Next *.out were found (show last 10 and without FAILED jobs)"
			print LOW_INDENT,"-"*120
			for _key in _keys:
				_isPrintOK = False
				if _isBigList:
					if _key>=_keys[-10]:
						_isPrintOK = True
				else:
					_isPrintOK = True
				if _isPrintOK:
					print LOW_INDENT,"%2d. %d %s" % (_loop, _key, _out_dict[_key])
					_loop += 1
			print LOW_INDENT,"-"*120
			print LOW_INDENT,"So, last campaign is \"%s\", which was taken \"%s %s\"" % (_tmp_last[4], _tmp_last[1], _tmp_last[2])
			print MED_INDENT,"MIN: run %d" % int(self.num_curr_runs["min"])
			print MED_INDENT,"MAX: run %d" % int(self.num_curr_runs["max"])

		if int(self.num_prev_runs["min"])<int(self.num_curr_runs["min"]):
			if int(self.num_prev_runs["max"])<int(self.num_curr_runs["max"]):

				self.found_camp = True

				_tmp_module = _tmp_last[0:-2]
				_module_name = ""
				for _part in _tmp_module:
					_module_name += _part + "_"
				self.module_name = _module_name + "lock.py"

				if self.MakeRunList()==False:
					return False

		if self.verbose["low"]:
			if self.found_camp:
				print LOW_INDENT,"New calibration campaign found, range of runs is from %d to %d" \
					% (int(self.num_curr_runs["min"]), int(self.num_curr_runs["max"]))
				print MED_INDENT,"New calib module is %s" % self.module_name
				print MED_INDENT,"New calib module has %d runs: from %d to %d" % (len(self.module_runs), min(self.module_runs), max(self.module_runs))
			else:
				print LOW_INDENT,"No found new calibration campaign"
			if self.verbose["med"]:
				print MED_INDENT,"Previous calibration campaign has range of runs from %d to %d" \
					% (int(self.num_prev_runs["min"]), int(self.num_prev_runs["max"]))

		return True


	# ----- GetCampaignByRangeRuns() -----
	def GetCampaignByRangeRuns(self, _range_runs):
		_id_first_run = int(_range_runs[0])
		_id_last_run  = int(_range_runs[1])
		if self.verbose["low"]:
			print LOW_INDENT,"Starting GetCampaignByRangeRuns() for runs in range from %d to %d" % (_id_first_run, _id_last_run)
		_out_list = list()
		for _nFile in os.listdir(self.val_ap_dir):
			if os.path.isfile(_nFile): continue
			if not _nFile.startswith("CALIB"): continue
			if not _nFile.endswith("Production_DONE.out"): continue
			# CALIB_22-Oct-2018_09-25-34_EM-EMPS-HECFCAL_Daily_run_from_00364116_to_00364138_run1_Production_DONE.out
			#  (0)       (1)       (2)      (3)            (4) (5)  (6)   (7)    (8)   (9)   (10)    (11)      (12)
			_tmp = _nFile.split("_")
			if _id_first_run==int(_tmp[7]) and _id_last_run==int(_tmp[9]):
				#_timeStamp = time.mktime(time.strptime(_tmp[1] + " " + _tmp[2], "%d-%b-%Y %H-%M-%S"))
				_out_list.append(_nFile)
		if len(_out_list)==0:
			print LOW_INDENT,"Calibration campaign with runs from %d to %d did not found" % (_id_first_run, _id_last_run)
			return True
		_tmp_last = _out_list[0].split("_")
		self.num_curr_runs["min"] = _tmp_last[7]
		self.num_curr_runs["max"] = _tmp_last[9]
		# - check prevoius runs
		validation_done = False
		if not self.mode_force:
			if self.GetPrevRuns()==False:
				return False
			if self.verbose["med"]:
				print MED_INDENT,"found previous calibration campaign: nRunMin %s, nRunMax %s" \
					% (self.num_prev_runs["min"], self.num_prev_runs["max"])
			if int(self.num_prev_runs["min"])>=int(self.num_curr_runs["min"]):
				if int(self.num_prev_runs["max"])>=int(self.num_curr_runs["max"]):
					validation_done = True
		if not validation_done:
			self.found_camp = True
			_tmp_module = _tmp_last[0:-2]
			_module_name = ""
			for _part in _tmp_module:
				_module_name += _part + "_"
			self.module_name = _module_name + "lock.py"
			if self.MakeRunList()==False:
				return False
		if self.verbose["low"] and self.found_camp:
			print LOW_INDENT,"Calibration campaign was founded, range of runs is from %d to %d" \
				% (int(self.num_curr_runs["min"]), int(self.num_curr_runs["max"]))
			print MED_INDENT,"Calib module is %s" % self.module_name
			print MED_INDENT,"The calib module has %d runs: from %d to %d" \
				% (len(self.module_runs), min(self.module_runs), max(self.module_runs))
		return True


	# ----- RunValidation() -----
	def RunValidation(self, _gains_list, _id_ref_camp):
		if self.verbose["low"]:
			print LOW_INDENT,"Starting RunValidation()"

		_flags = (self.with_ofc, self.mode_test, _id_ref_camp)
		_validator = Validator()

		_proc_list = []
		for _gain in _gains_list:
			_desc_proc = "%s gain" % _gain
			_proc = Process(target=_validator, name=_desc_proc, args=(self.verbose, self.module_name, _gain, self.channels_swapped_dict, _flags))
			_proc_list.append(_proc)

		for _proc in _proc_list:
			print LOW_INDENT,"Process %s starting" % _proc
			_proc.start()

		for _proc in _proc_list:
			_proc.join()
			print LOW_INDENT,"Process %s done" % _proc

		return True


	# ----- ShowCampaigns() -----
	def ShowCampaigns(self, _id_camp, _show_runs=False):
		if self.verbose["low"]:
			print LOW_INDENT,"Starting ShowCampaigns()"

		db = ECalDB('H', self.verbose)
		db.Open(self.val_db_dir)

		if _id_camp==-1:
			_id_camp = db.GetCampaignMaxId()
		_list_camp = db.GetCampaignList(_id_camp)

		_list_run = []
		for _camp in _list_camp:
			_run = db.GetRunByCampaignId(_camp['id'])
			_list_run.append(_run)
		db.Close()

		if _show_runs:
			db.PrintCampaignRun(_list_camp, _list_run)
		else:
			db.PrintCampaign(_list_camp)


	# ----- ShowDeviations -----
	def ShowDeviations(self, _id_part, _id_camp, _verify=False):
		if self.verbose["low"]:
			print LOW_INDENT,"Starting ShowDeviations()"

		# Getting number max campaign id
		db = ECalDB('H', self.verbose)
		db.Open(self.val_db_dir)
		if _id_camp==-1:
			_id_camp = db.GetCampaignMaxId()

		camp = db.GetCampaignById(_id_camp)
		self.list_last_runs = db.GetRunByCampaignId(_id_camp)

		db.Close()

		_lar_part = __import__('validation_config_partitions').LAR_PARTITION
		# Cycle by gain
		channelsByGain = dict([])
		for _gain in GAIN_SHORT_TAG:
			db = ECalDB(_gain, self.verbose)
			db.Open(self.val_db_dir)
			channelsByGain.update({_gain:[]})
			if _id_part!='ALL':
				channelsByGain[_gain] = db.GetChannelByCampaignAndSubPartition(_id_camp, _id_part)
			else:
				for _part in _lar_part:
					channelsByGain[_gain] += db.GetChannelByCampaignAndSubPartition(_id_camp, _part)
			db.Close()

		# Initialization channels by HighG
		metaChannels = channelsByGain['H']

		# Getting data from MediumG & LowG
		for _gain in ('M','L'):
			_channels = []

			for _ch_g in channelsByGain[_gain]:
				_key_g = 5000*_ch_g['ft'] + 128*_ch_g['sl'] + _ch_g['ch']
				_found = False

				for _ch in metaChannels:
					_key = 5000*_ch['ft'] + 128*_ch['sl'] + _ch['ch']

					if _key==_key_g:
						_found = True
						for _dev in range(1, DB_NUM_DEV_CHANNEL+1):
							_devItem = 'deviation%d_%s' % (_dev, _gain.lower())
							_ch[_devItem] = _ch_g[_devItem]

				if not _found:
					_channels.append(_ch_g)

			metaChannels += _channels;

		# Cheking "true" FT, Slot and Channels
		channels = []
		for _ch in metaChannels:
			_catch = True

			# a) FCAL - only in FT 6
			if "FCAL" in _ch['sub_sub_partition']:
				if not _ch['ft'] in FCAL_FT:
					_catch = False

			# b)  HEC - only in FT 3,10,16 and 22 AND high gain skip too
			if "HEC" in _ch['sub_sub_partition']:
				for _dev in range(1, DB_NUM_DEV_CHANNEL+1):
					_ch['deviation%d_h'%_dev] = None
				if not _ch['ft'] in HEC_FT:
					_catch = False
				elif (_ch['ft'] in HEC_FT) and (not _ch['sl'] in HEC_SLOT):
					_catch = False

			# c) EMEC - skip wrongly inserted HEC and FCAL noise in EMEC partition
			if "EMEC" in _ch['sub_sub_partition']:
				if (_ch['ft'] in HEC_FT) or (_ch['ft'] in FCAL_FT):
					_catch = False

			if _catch:
				channels.append(_ch)

		channels.sort(key=SortByFtSlotCh)
		_isOk = True
		if _verify:
			_isOk = self.VerifyCampaign()

		print ""
		if _isOk:
			_camp_info = "%s (%s, %s, runs %d - %d)" \
				% (camp[0]['id'], camp[0]['type'], GetDateTime(camp[0]['date']), self.list_last_runs[0]['id'], self.list_last_runs[-1]['id'])
			print MED_INDENT,"Deviation for campaign %s and LAr partition %s" % (_camp_info, _id_part)
			db.PrintChannel(channels, camp[0]['type'])
			return True
		else:
			print MED_INDENT,"Deviation for campaign %s and LAr partition %s not found" % (camp[0]['id'], _id_part)
			return False


	# ----- VerifyCampaign() -----
	def VerifyCampaign(self):
		_isOk = False

		_id_run = []
		for _run in self.list_last_runs:
			_id_run.append(_run['id'])
		_id_run.sort()
		if _id_run[0]==int(self.num_curr_runs['min']) and _id_run[-1]==int(self.num_curr_runs['max']):
			_isOk = True

		return _isOk
