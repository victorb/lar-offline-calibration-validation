
# ----- System -----
import sys, argparse

# ---- Locale -----
from FebManage_class import FebManage


# ----- CreateParser() -----
def CreateParser():
	parser = argparse.ArgumentParser(description='Manage FEB information', 
								  formatter_class=argparse.RawTextHelpFormatter,
								  usage=' %(prog)s  [options]')
	
	parser.add_argument('--showFeb', default='empty', dest='nameFeb', metavar="NAME_FEB",
					 help='only search and show FEB with %(metavar)s\n\n')
	
	parser.add_argument('--showAllFebs', default=False, action='store_const', const=True,
					 help='only show list all FEBs from DB (by default %(default)s)\n\n')
	
	parser.add_argument('--file', default='empty', dest='nameFile', metavar="NAME_FILE",
					 help='update DB from %(metavar)s\n\n')
	
	parser.add_argument('--feb', default=('empty', -1, -1), dest='nameFeb', nargs=3, metavar=('NAME_FEB', 'NEW_SLOT', 'OLD_SLOT'),
					 help='manually update DB\n\n')
	
	parser.add_argument('--verbose', type=int, default=0, dest='ver_level', 
					 help='increase output verbosity: 0 - quiet, 1 - low, 2 - medium, 3 - high (by default %(default)d)\n\n')
	
	return parser


# ----- Exit() -----
def Exit(_prog):
	print "%s: is finished" % _prog
	sys.exit(0)


# ================================================
#                     MAIN
# ================================================
if __name__=="__main__":
	_name_prog = sys.argv[0]
	
	# get the job input parameters
	parser = CreateParser()
	argSpace = parser.parse_args(sys.argv[1:])
	
	# set verbosity
	_verbose = {"low":False, "med":False, "high":False}
	if argSpace.ver_level==1:
		_verbose["low"] = True
	if argSpace.ver_level==2:
		_verbose["low"] = True
		_verbose["med"] = True
	if argSpace.ver_level==3:
		_verbose["low"]  = True
		_verbose["med"]  = True
		_verbose["high"] = True

	print "%s: is started" % _name_prog
	
	# Create FebManage class & setup
	feb_manage = FebManage(_verbose)
	
	# Show FEB
	if argSpace.nameFeb!='empty':
		print "%s: option \"showFeb\" was selected with NAME_FEB=%s" % (_name_prog, argSpace.nameFeb)
		try:
			feb_manage.ShowFeb(argSpace.nameFeb)
		except ValueError as _err:
			print "%s: %s" % (_name_prog, _err)
		Exit(_name_prog)
	
	# Show list all FEBs
	if argSpace.showAllFebs:
		print "%s: option \"showAllFebs\" was selected" % _name_prog
		try:
			feb_manage.ShowTable()
		except ValueError as _err:
			print "%s: %s" % (_name_prog, _err)
		Exit(_name_prog)
		
	# Update from file
	if argSpace.nameFile!='empty':
		print "%s: option for update from \"file\" was selected with NAME_FILE=%s" % (_name_prog, argSpace.nameFile)
		try:
			feb_manage.AddFromFile(argSpace.nameFile)
		except ValueError as _err:
			print "%s: %s" % (_name_prog, _err)
		Exit(_name_prog)
		
	# Manually update
	if argSpace.nameFeb[0]!='empty' and int(argSpace.nameFeb[1])>-1:
		print "%s: option for manually update \"feb\" was selected with NAME_FEB=%s, NEW_SLOT=%s and OLD_SLOT=%s" \
			% (_name_prog, argSpace.nameFeb[0], argSpace.nameFeb[1], argSpace.nameFeb[2])
		try:
			feb_manage.AddFebManually(argSpace.nameFeb)
		except ValueError as _err:
			print "%s: %s" % (_name_prog, _err)
		Exit(_name_prog)
	elif argSpace.nameFeb[0]!='empty' and int(argSpace.nameFeb[1])==-1:
		print "%s: option for manually update \"feb\" was selected, but NEW_SLOT not specified -> exit" % _name_prog
		Exit(_name_prog)















