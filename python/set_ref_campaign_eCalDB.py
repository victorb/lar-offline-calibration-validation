#!/usr/bin/python

# ----- System -----
import sqlite3
import sys, os, argparse

# ----- Locale -----
from ECalDB_core import ECalDB
from ECalDB_definitions import *

# ----- CreateParser() -----
def CreateParser():
	parser = argparse.ArgumentParser(description='Setting reference campaign', 
								  formatter_class=argparse.RawTextHelpFormatter,
								  usage=' %(prog)s  [options]')
	
	parser.add_argument('--id_ref_camp', type=int, required=True, help='<Required> New reference campaign ID\n\n')
	
	return parser
	

# ----- MAIN -----
if __name__ == "__main__":
	_name_prog = sys.argv[0]
	
	# get the job input parameters
	parser = CreateParser()
	argSpace = parser.parse_args(sys.argv[1:])
	
	if argSpace.id_ref_camp<1:
		print "%s: ERROR wrong value of ID reference campaign (ID>=1)" % _name_prog
		sys.exit()
	
	_verbose = {"low":True, "med":True, "high":True}
	_db_dir_name = os.environ["VALIDATION_DATABASE_DIR"]

	for _gain in ['H', 'M', 'L']:
		db = ECalDB(_gain, _verbose)
		db.Open(_db_dir_name)
		print "Set campaign with ID %d as reference for gain %s" % (argSpace.id_ref_camp, _gain)
		db.SetReferenceByCampaignId(argSpace.id_ref_camp)
		db.Close()




















    



