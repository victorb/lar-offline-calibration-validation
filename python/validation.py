
# ----- System -----
import sys, argparse

# ---- Locale -----
from Validation_definitions import *
from Validation_utils import ValidationIsRunnig, MakeLockFile, RemoveLockFile
from Validation_class import Validation

_list_lar_partitions = "EMBA, EMBC, EMBPSA, EMBPSC, EMECA, EMECC, EMECPSA, EMECPSC, FCALA, FCALC, HECA, HECC or ALL"

# ----- CreateParser() -----
def CreateParser():
	parser = argparse.ArgumentParser(description='Python module for checking LAr calibration runs and displaying results also',
		formatter_class=argparse.RawTextHelpFormatter,
		usage=' %(prog)s  [options]')

	# ----- Show options -----
	parser.add_argument('--show_camps', type=int, default=-999, dest='camp_id', metavar="ID_CAMPAIGN",
		help='only show (not run) list of campaigns from DB starting with campaign %(metavar)s,\nif %(metavar)s=-1 last campaign will be taken\n\n')

	parser.add_argument('--show_runs', default=False, action='store_const', const=True,
		help='additional option for --show_camps, show runs info too (by default \'%(default)s\')\n\n')

	parser.add_argument('--show_devs', default=('unknown', -999), dest='part_id', nargs=2, metavar=('ID_PARTITION', 'ID_CAMPAIGN'),
		help='only show (not run) deviations in LAr ID_PARTITION from DB for campaign with ID_CAMPAIGN,\npossible ID_PARTITION: %s,\nif ID_CAMPAIGN=-1 last campaign will be taken\n\n'%_list_lar_partitions)

	# ----- Validation options -----
	parser.add_argument('--run', default=False, action='store_const', const=True,
		help='start validation if new runs were take (by default %(default)s)\n\n')

	parser.add_argument('--force', default=False, action='store_const', const=True,
		help='force start validation even if old calibration campaign will be supplied, ' +\
			'\nit is designed to use together with options --range_runs and --input_module (by default %(default)s)\n\n')

	parser.add_argument('--range_runs', nargs=2, default=(-999, -999),
		help='validation will be made for campaign which have runs in range from range_runs[0] to range_runs[1],' +\
			'\nif not present - last campaign will be used\n\n')

	parser.add_argument('--input_module', default='unknown', dest='input_module', metavar='IN_MODULE',
		help='input module describing calibration campaign CALIB*.out,\nif not present - last campaign will be used\n\n')

	parser.add_argument('--id_ref_camp', nargs=1, default=[-999], type=int,
		help='validation will be made with using the reference campaign\n\n')

	parser.add_argument('--mode_test', default=False, action='store_const', const=True,
		help='use in case if you want not change DB and update of \'validation.last_runs\' file (by default %(default)s)\n\n')

	parser.add_argument('--with_ofc', default=False, action='store_const', const=True,
		help='make validation with OFC calculation (by default %(default)s)\n\n')

	parser.add_argument('--gains', nargs='+', default=('H', 'M', 'L'),
		help='set list of gains (by default all gains H M L)\n\n')

	parser.add_argument('--mode_febs', nargs='?', const=FEBS_SWAPPED_FILE, default=False,
		help='take in account shift in FEBS position from file,\nvery slowly (by default file is \'%s\')\n\n' % FEBS_SWAPPED_FILE)

	parser.add_argument('--verb', type=int, default=0, dest='verbose',
		help='increase output verbosity: 0 - quiet, 1 - low, 2 - medium, 3 - high (by default %(default)d)\n\n')

	return parser


# ----- Exit() -----
def Exit(_prog, _name):
	RemoveLockFile(_name)
	print "%s: is finished" % _prog
	sys.exit()


# ================================================
#                     MAIN
# ================================================
if __name__=="__main__":
	_name_prog = sys.argv[0]

	# get the job input parameters
	parser = CreateParser()
	argSpace = parser.parse_args(sys.argv[1:])

	# set verbosity
	_verbose = {"low":False, "med":False, "high":False}
	if argSpace.verbose==1:
		_verbose["low"] = True
	if argSpace.verbose==2:
		_verbose["low"] = True
		_verbose["med"] = True
	if argSpace.verbose==3:
		_verbose["low"]  = True
		_verbose["med"]  = True
		_verbose["high"] = True

	print "%s: is started" % _name_prog

	# create class & setup
	val = Validation(_verbose)
	_name_lock_file = val.GetNameLockFile()


	# ===== Show options =====
	# - campaigns and runs
	if argSpace.camp_id!=-999:
		print "%s: option \"show_camps\" was selected with ID_CAMPAIGN=%d" % (_name_prog, argSpace.camp_id)
		if argSpace.show_runs:
			print "%s: option \"show_runs\" was selected" % _name_prog
		val.ShowCampaigns(argSpace.camp_id, argSpace.show_runs)
		Exit(_name_prog, _name_lock_file)

	# - deviations
	if argSpace.part_id[0]!='unknown' and argSpace.part_id[1]!=-999:
		print "%s: option \"show_devs\" was selected with ID_PARTITION=%s and ID_CAMPAIGN=%s" % (_name_prog, argSpace.part_id[0], argSpace.part_id[1])
		val.ShowDeviations(argSpace.part_id[0], int(argSpace.part_id[1]))
		Exit(_name_prog, _name_lock_file)


	# ===== Validation options =====
	if argSpace.run:
		print "%s: option \"run\" was selected" % _name_prog

		# - check if validation not running already
		if ValidationIsRunnig(_name_lock_file):
			if _verbose["low"]:
				print LOW_INDENT,"previous validation is still running -> exit"
			sys.exit()

		# - set lock file
		if not MakeLockFile(_name_lock_file):
			Exit(_name_prog, _name_lock_file)

		# - mode_test
		if argSpace.mode_test:
			print "%s: option \"mode_test\" was selected" % _name_prog
			val.SetModeTest()

		# - validation with OFC calculation
		if argSpace.with_ofc:
			print "%s: option \"with_ofc\" was selected" % _name_prog
			val.WithOFC()

		# - mode_febs
		if argSpace.mode_febs:
			print "%s: option \"mode_febs\" was selected and file is %s" % (_name_prog, argSpace.mode_febs)
			val.SetModeFeb(argSpace.mode_febs)

		# - force_mode
		if argSpace.force:
			print "%s: option \"force\" was selected" % _name_prog
			val.SetForceMode()

		if int(argSpace.range_runs[0])>=1:
			# - validation for campagns which has runs into the range
			print "%s: option \"range_runs\" was selected" % _name_prog
			if not val.GetCampaignByRangeRuns(argSpace.range_runs):
				Exit(_name_prog, _name_lock_file)
		elif argSpace.input_module!='unknown':
			# - validation for campaign where python description is defined
			print "%s: option \"input module\" was selected" % _name_prog
			if not val.CheckInputModule(argSpace.input_module):
				Exit(_name_prog, _name_lock_file)
		else:
			# - find new calibration campaign
			if not val.FindNewCampaign():
				Exit(_name_prog, _name_lock_file)

		# - if new campaign was founded -> run validation, show deviations and update validation.last_runs
		if val.FoundCampaign():
			try:
				val.RunValidation(argSpace.gains, int(argSpace.id_ref_camp[0]))
			except ValueError as _err:
				print LOW_INDENT,"RunValidation() was terminated <-> %s" % _err
				Exit(_name_prog, _name_lock_file)

			if not argSpace.mode_test:
				# - show results of the validation
				# val.ShowDeviations('ALL', -1, True)
				# - update validation.last_runs file
				val.UpdateRunInfoFile()

			# - make timestamp file
			val.MakeTimeFile()

		Exit(_name_prog, _name_lock_file)
