
# ----- System -----
import time
import subprocess
import sys
import string
import os, multiprocessing
from datetime import datetime, timedelta

import libPyROOT
libPyROOT.gROOT.ProcessLine('gErrorIgnoreLevel = 0;')
from ROOT import TH1F, TH2F, TH2I, TNtuple, TFile, TChain

# ----- Locale -----
import ECalDBLib
from Validation_definitions import *
from Validation_utils import SelectedChannel, BadChanInterpreter, InList
from Validation_coolDB import GetTimesCoolDB, GetMagneticFieldsCoolDB
from ECalDB_utils import GetDateTime

ecalDB = ECalDBLib.ECalDBLib()

# ==========================
#     Validator
# ==========================
class Validator:
	def __call__(self, _verbose=None, _module=None, _gain=None, _channels_swapped_dict=None, _flags=(False, False, None)):
		self.verbose = dict()
		if not _verbose:
			self.verbose["low"]  = False
			self.verbose["med"]  = False
			self.verbose["high"] = False
		else:
			self.verbose = _verbose

		self.module = _module
		self.gain   = _gain

		self.id_name = "Gain unknown (pid %d)" % os.getpid()
		if self.gain:
			self.id_name = "Gain %s (pid %d)" % (self.gain, os.getpid())
		if self.verbose["low"]:
			print MED_INDENT,"%s: start Validator().__call__ for \"%s\"" % (self.id_name, self.module)

		if not self.module:
			raise ValueError("%s: Validator().__call__ ERROR <-> MODULE.PY not supplied" % self.id_name)
		if not self.gain:
			raise ValueError("%s: Validator().__call__ ERROR <-> GAIN not supplied" % self.id_name)

		# febs mode
		self.channels_swapped_dict = _channels_swapped_dict
		self.mode_febs = False
		if self.channels_swapped_dict:
			if len(self.channels_swapped_dict)>0:
				self.mode_febs = True
				if self.verbose["low"]:
					print MED_INDENT,"%s: set \"mode_febs\"" % self.id_name
		self.is_swapped = False

		self.with_ofc, self.mode_test, self.id_ref_camp = _flags

		self.db_name_pat = DB_NAME_PATTERN
		self.db_path     = os.environ["VALIDATION_DATABASE_DIR"]
		self.time_begin  = time.time()

		self.run_dict_list_ref = {}
		self.run_dict_list     = {}
		self.campaign_dict     = {}

		self.resultFeb        = []
		self.deviatingFebList = []
		self.deviatingFebDict = {}

		self.resultChannel        = []
		self.deviatingChannelList = []
		self.deviatingChannelDict = {}

		self.results_dir = os.environ["VALIDATION_RESULTS_DIR"]

		self.db_names = {'orig':None, 'copy':None}

		if os.path.exists(self.db_path):
			self.db_names['orig'] = self.db_path + '/' + self.db_name_pat + self.gain + '.db'
			self.db_names['copy'] = self.db_path + '/' + self.db_name_pat + 'copy_' + self.gain + '.db'
			if self.verbose["med"]:
				_len_name = len(self.id_name)
				print MED_INDENT,"%s: copy database %s" % (self.id_name, self.db_names['orig'])
				print BIG_INDENT," "*_len_name,"        to %s" % self.db_names['copy']
			os.system('cp ' + self.db_names['orig'] + ' ' + self.db_names['copy'])

			ecalDB.Open('sqlite://' + self.db_names['copy'])

			if self.verbose["med"]:
				print MED_INDENT,"%s: get campaign info from \"%s\"" % (self.id_name, self.module)

			self.run_dict_list = self.ParseCampaignModule()
			if self.verbose["high"]:
				for _run in self.run_dict_list:
					print MED_INDENT,"%s:     %17s - %d"   % (self.id_name, 'run_number', _run['id'])
					print MED_INDENT,"%s:     %17s - %s"   % (self.id_name, 'date_start', GetDateTime(_run['date_start']))
					print MED_INDENT,"%s:     %17s - %s"   % (self.id_name, 'date_stop', GetDateTime(_run['date_stop']))
					print MED_INDENT,"%s:     %17s - %s"   % (self.id_name, 'valid_type', _run['type'])
					print MED_INDENT,"%s:     %17s - %s"   % (self.id_name, 'run_partition', _run['partition'])
					print MED_INDENT,"%s:     %17s - %s"   % (self.id_name, 'run_gain', _run['gain'])
					print MED_INDENT,"%s:     %17s - %7.1f" % (self.id_name, 'toroid', _run['toroid'])
					print MED_INDENT,"%s:     %17s - %7.1f" % (self.id_name, 'solenoid', _run['solenoid'])
					print MED_INDENT,"%s:     %17s - %7.1f" % (self.id_name, 'expected_toroid', _run['expected_toroid'])
					print MED_INDENT,"%s:     %17s - %7.1f" % (self.id_name, 'expected_solenoid', _run['expected_solenoid'])
					print MED_INDENT,"%s:     %17s - %s"   % (self.id_name, 'dq_flag', _run['dq_flag'])
					print MED_INDENT,"%s:     %17s - %s"   % (self.id_name, 'flag', _run['flag'])

					print MED_INDENT,"%s:     %17s - " % (self.id_name, 'root_files_data')
					for _id, _root_file in enumerate(_run['root_files_data']):
						print MED_INDENT,"%s:    %22s. %s" % (self.id_name, _id+1, _root_file.split('/')[-1])

					print MED_INDENT,"%s:     %17s - " % (self.id_name, 'root_files_dqmf')
					for _id, _root_file in enumerate(_run['root_files_dqmf']):
						print MED_INDENT,"%s:    %22s. %s" % (self.id_name, _id+1, _root_file.split('/')[-1])

					print ""

			_type_campaign = 'unknown'
			if 'Daily' in self.module:
				_type_campaign = 'DAILY'
			elif 'Weekly' in self.module:
				_type_campaign = 'WEEKLY'
			if _type_campaign=='unknown':
				raise ValueError("%s: Validator().__call__ ERROR <-> cannot find Daily or Weekly in the python module" % self.id_name)
			self.BuildCampaignDict(_type_campaign, 'NO')

			ecalDB.AddCampaignEntry(self.campaign_dict)
			for _run_dict in self.run_dict_list:
				ecalDB.AddRunEntry(_run_dict)

			# Get reference campaign
			_id_ref_camp = self.id_ref_camp if self.id_ref_camp>=1 else self.campaign_dict['ref']
			if self.verbose["med"]:
				print MED_INDENT,"%s: will be compare with reference campaign %d" % (self.id_name, _id_ref_camp)
			self.run_dict_list_ref = ecalDB.GetRunByCampaign(_id_ref_camp)

			ecalDB.Commit()
			ecalDB.Close()

			# Make results dir
			self.results_dir += '/Campaign_' + str(self.run_dict_list[0]['campaign'])
			if not os.path.exists(self.results_dir): os.mkdir(self.results_dir)

			# Compare the two campaign
			self.ValidProcess()

			if not self.mode_test:
				if self.verbose["med"]:
					_len_name = len(self.id_name)
					print MED_INDENT,"%s: reverse copy database %s" % (self.id_name, self.db_names['copy'])
					print BIG_INDENT," "*_len_name,"                to %s" % self.db_names['orig']
				os.system('cp ' + self.db_names['copy'] + ' ' + self.db_names['orig'])

				if self.verbose["med"]:
					print MED_INDENT,"%s: remove database %s\n" % (self.id_name, self.db_names['copy'])
				os.system('rm -f ' + self.db_names['copy'])
		else:
			raise ValueError("%s: Validator().__call__ ERROR <-> database path is wrong: %s" % (self.id_name, self.db_path))


	# =========================================================
	# a) UTILS PART
	# =========================================================

	# ----- BuildCampaignDict() -----
	def BuildCampaignDict(self, _camp_type, db='NO', id=0, ref=0):
		try:
			if id==0:
				id = ecalDB.GetCampaignId()
		except Exception, _err_msg:
			raise ValueError("%s: BuildCampaignDict() ERROR <-> database problem \"%s\"" % (self.id_name, _err_msg))

		toroidStatusList = []
		solenoidStatusList = []

		toroid_status = 'ON'
		solenoid_status = 'ON'

		dq_flag = 'GREEN'
		timeCampaign = 0

		for _run_dict in self.run_dict_list:
			_run_dict['campaign'] = id

			# TOROID
			curr_toroid = _run_dict['toroid']
			curr_expected_toroid = _run_dict['expected_toroid']
			if curr_expected_toroid==0:
				toroidStatusList.append('OFF')
			elif (abs(curr_expected_toroid - curr_toroid)/curr_expected_toroid)<0.3:
				toroidStatusList.append('ON')
			else:
				toroidStatusList.append('OFF')

			# SOLENOID
			curr_solenoid = _run_dict['solenoid']
			curr_expected_solenoid = _run_dict['expected_solenoid']
			if curr_expected_solenoid==0:
				solenoidStatusList.append('OFF')
			elif (abs(curr_expected_solenoid - curr_solenoid)/curr_expected_solenoid)<0.3:
				solenoidStatusList.append('ON')
			else:
				solenoidStatusList.append('OFF')
			# DQ
			curr_dq_flag = _run_dict['dq_flag']
			if DQ_FLAG[dq_flag]<DQ_FLAG[curr_dq_flag]:
				dq_flag = curr_dq_flag
			# TIME
			timeCampaign = timeCampaign + int(_run_dict['date_start'])
		timeCampaign = int( timeCampaign/len(self.run_dict_list) )

		if len(set(toroidStatusList))==1:
			toroid_status = toroidStatusList[0]
		elif len(set(toroidStatusList))==2:
			toroid_status = 'MIDDLE'

		if len(set(solenoidStatusList))==1:
			solenoid_status = solenoidStatusList[0]
		elif len(set(solenoidStatusList))==2:
			solenoid_status = 'MIDDLE'

		try:
			if ref==0:
				ref = ecalDB.GetReferenceId(toroid_status, solenoid_status)
		except Exception, _err_msg:
			raise ValueError("%s: BuildCampaignDict() ERROR <-> database problem \"%s\"" % (self.id_name, _err_msg))

		self.campaign_dict['ref'            ] = ref
		self.campaign_dict['date'           ] = timeCampaign
		self.campaign_dict['type'           ] = _camp_type
		self.campaign_dict['ntuple'         ] = ''
		self.campaign_dict['toroid_status'  ] = toroid_status
		self.campaign_dict['solenoid_status'] = solenoid_status
		self.campaign_dict['composite'      ] = 'NO'
		self.campaign_dict['dq_flag'        ] = dq_flag
		self.campaign_dict['flag'           ] = 'RED'
		self.campaign_dict['db'             ] = db
		self.campaign_dict['comment'        ] = ''


	# ----- MakeFigureFileName() -----
	def MakeFigureFileName(self, _part, _label, _gain, _camp):
		return "%s/Figure_%s_%s_%s_%s.root" % (self.results_dir, _part, _label, _gain, _camp)


	# ----- MakeNtupleFileName() -----
	def MakeNtupleFileName(self, _part, _label, _gain, _camp):
		return "%s/Ntuple_%s_%s_%s_%s.root" % (self.results_dir, _part, _label, _gain, _camp)


	# ----- MakeShiftFebsFileName() -----
	def MakeShiftFebsFileName(self, _part, _label, _gain, _camp):
		return "%s/SwappedFebs_%s_%s_%s_%s.txt" % (self.results_dir, _part, _label, _gain, _camp)


	# ----- MakeFebNewFileName() -----
	def MakeFebNewFileName(self, _part, _label, _gain, _camp):
		return "%s/FebNew_%s_%s_%s_%s.txt" % (self.results_dir, _part, _label, _gain, _camp)


	# ----- GetTreeForPartition()
	def GetTreeForPartition(self, _name_tree, _ref_run_dict, _cur_run_dict, _dev_type=''):
		_part = _cur_run_dict['partition']
		_ref_file_list = {}
		_cur_file_list = {}

		for _sub_part in LIST_OF_PART[_part]:
			_ref_file_list[_sub_part] = []
			_cur_file_list[_sub_part] = []

		if _dev_type=='':
			_dev_type = _ref_run_dict['type']

		# Get root files for reference run
		if not('root_files_data' in _ref_run_dict) or _dev_type in OTHER_DEV_NAME_LIST:
			_files_data, _dqmf = ECalDBLib.GetRootFiles(_ref_run_dict['id'], _dev_type)
			_ref_run_dict['root_files_data'] = _files_data

		for _ref_run in _ref_run_dict['root_files_data']:
			for _sub_part in LIST_OF_PART[_part]:
				#CHANGE for ALL pedestals
				if _dev_type=='PEDESTAL':
					if _sub_part=='EMB' and 'root' in _ref_run.split('/')[len(_ref_run.split('/'))-1]:
						_ref_file_list[_sub_part].append(_ref_run)
				elif _sub_part=='EMBPS':
					if 'EB-PS' in _ref_run.split('/')[len(_ref_run.split('/'))-1]:
						_ref_file_list[_sub_part].append(_ref_run)
				elif _sub_part in _ref_run.split('/')[len(_ref_run.split('/'))-1] :
					_ref_file_list[_sub_part].append(_ref_run)

		# Get root files for current run
		if not('root_files_data' in _cur_run_dict) or _dev_type in OTHER_DEV_NAME_LIST:
			_files_data, _dqmf = ECalDBLib.GetRootFiles(_cur_run_dict['id'], _dev_type)
			_cur_run_dict['root_files_data'] = _files_data

		for _cur_run in _cur_run_dict['root_files_data']:
			for _sub_part in LIST_OF_PART[_part]:
				#CHANGE for ALL pedestals
				if _dev_type=='PEDESTAL':
					if _sub_part=='EMB' and 'root' in _cur_run.split('/')[len(_cur_run.split('/'))-1]:
						_cur_file_list[_sub_part].append(_cur_run)
				elif _sub_part=='EMBPS':
					if 'EB-PS' in _cur_run.split('/')[len(_cur_run.split('/'))-1]:
						_cur_file_list[_sub_part].append(_cur_run)
				elif _sub_part in _cur_run.split('/')[len(_cur_run.split('/'))-1] : _cur_file_list[_sub_part].append(_cur_run)

		# get the trees in a TChain with the right sort
		_ref_tree = {}
		_cur_tree = {}

		for _sub_part in LIST_OF_PART[_part]:
			if len(_ref_file_list[_sub_part])==0: continue
			if len(_cur_file_list[_sub_part])==0: continue

			_ref_file_list[_sub_part].sort()
			_ref_file_list[_sub_part].reverse()
			_ref_first = _ref_file_list[_sub_part][0]
			_ref_tag = _ref_first.split('/')[len(_ref_first.split('/'))-3].split('_')[len(_ref_first.split('/')[len(_ref_first.split('/'))-3].split('_'))-1]

			_cur_file_list[_sub_part].sort()
			_cur_file_list[_sub_part].reverse()
			_cur_first = _cur_file_list[_sub_part][0]
			_cur_tag = _cur_first.split('/')[len(_cur_first.split('/'))-3].split('_')[len(_cur_first.split('/')[len(_cur_first.split('/'))-3].split('_'))-1]

			_ref_tree[_sub_part] = TChain(_name_tree)
			_cur_tree[_sub_part] = TChain(_name_tree)
			for _ref_file in _ref_file_list[_sub_part]:
				if _ref_file.split('/')[len(_ref_file.split('/'))-3].endswith('_'+_ref_tag):
					_det = _ref_file.split('/')[len(_ref_file.split('/'))-1].split('.')[0]
					_det = _det.split('_')[2]

					if ECalDBLib.fileTag[_dev_type] in _ref_file:
						_ref_tree[_sub_part].Add(_ref_file)

					for _cur_file in _cur_file_list[_sub_part]:
						if _cur_file.split('/')[len(_cur_file.split('/'))-3].endswith('_'+_cur_tag):
							if (_dev_type=='PEDESTAL') or (_det in _cur_file):
								if ECalDBLib.fileTag[_dev_type] in _cur_file:
									_cur_tree[_sub_part].Add(_cur_file)

		return _ref_tree, _cur_tree


	# ----- ParseCampaignModule() -----
	def ParseCampaignModule(self):
		_module_name_locale = self.module
		_module_name_full   = self.module
		if "/" in _module_name_full:
			_tmp = _module_name_locale.split('/')
			_module_name_locale = _tmp[len(_tmp)-1]
			os.system('cp ' + _module_name_full + ' ' + _module_name_locale)

		if _module_name_locale.endswith('.py'):
			_module_name_locale = _module_name_locale.split('.py')[0]
		_module = __import__(_module_name_locale)

		_run_list = []
		_run_num_list = []
		for _run in _module.sRecoRunList:
			_run_num  = string.rstrip(_run[1])
			_run_num  = _run_num.split('_')
			_run_part = DICT_PARTITION[_run[0][0]];
			_run_gain = DICT_GAIN[_run[0][1]]

			for _valid_type, _id_type in DICT_RUN.iteritems():
				_int_run_num = int(_run_num[_id_type])
				if _int_run_num==0:
					continue
				if _int_run_num in _run_num_list:
					continue

				# CHANGE for ALL pedestals
				if _valid_type=='PEDESTAL':
					_run_param = ['run', _valid_type, 'ALL', _run_gain]
				else:
					_run_param = ['run', _valid_type, _run_part, _run_gain]

				_run_num_list.append(_int_run_num)
				_run_dict = self.BuildRunDict(_run_param, _int_run_num)
				if len(_run_dict):
					_run_list.append(_run_dict)

		if "/" in _module_name_full:
			os.system('rm -f ' + _module_name_locale + '.py')
			os.system('rm -f ' + _module_name_locale + '.pyc')

		return _run_list


	# ----- BuildRunDict() -----
	def BuildRunDict(self, _run_param, _run_num):
		times = GetTimesCoolDB(_run_num)
		magneticFields1 = GetMagneticFieldsCoolDB(times[0])
		magneticFields2 = GetMagneticFieldsCoolDB(times[1])

		_run_type  = _run_param[1]
		_partition = _run_param[2]

		result = dict()

		data, dqmf = ECalDBLib.GetRootFiles(_run_num, _run_type)
		if data==[] or dqmf==[]:
			if _run_type == 'PEDESTAL' and _partition == 'ALL':
				raise ValueError("%s: BuildRunDict() ERROR <-> cannot find any root files for run %d (%s) and partition %s" % (self.id_name, _run_num, _run_type, _partition))
			else:
				print MED_INDENT,"%s: BuildRunDict() WARNING <-> cannot find any root files for run %d (%s) and partition %s" % (self.id_name, _run_num, _run_type, _partition)
		else:
			result['id'               ] = _run_num
			result['date_start'       ] = times[0]
			result['date_stop'        ] = times[1]
			result['type'             ] = _run_param[1]
			result['partition'        ] = _run_param[2]
			result['gain'             ] = _run_param[3]
			result['toroid'           ] = (magneticFields1[0] + magneticFields2[0])/2.0 # mean field during the data taking
			result['solenoid'         ] = (magneticFields1[1] + magneticFields2[1])/2.0 # mean field during the data taking
			result['expected_toroid'  ] = (magneticFields1[2] + magneticFields2[2])/2.0 # mean field during the data taking
			result['expected_solenoid'] = (magneticFields1[3] + magneticFields2[3])/2.0 # mean field during the data taking
			result['dq_flag'          ] = 'UNDEFINED'
			result['flag'             ] = 'RED'
			result['root_files_data'  ] = data
			result['root_files_dqmf'  ] = dqmf

		return result


	# ----- GetSwappedChannel() -----
	def GetSwappedChannel(self, _cur_ch_id):
		self.is_swapped = True
		_ref_ch_id = self.channels_swapped_dict.get(_cur_ch_id, -999)

		if _ref_ch_id<0:
			_ref_ch_id = _cur_ch_id
			self.is_swapped = False

		return _ref_ch_id



	# =========================================================
	# b) PROCESS PART
	# =========================================================

	# ----- ValidProcess() -----
	def ValidProcess(self):
		# FILE process
		if self.verbose["low"]:
			print "\n",MED_INDENT,"%s: starting FILE process" % self.id_name
		_begin_file_time = datetime.now()

		_tot_num_process = 0
		for _ref_run in self.run_dict_list_ref:
			if _ref_run['gain']!=self.gain:
				continue
			for _run in self.run_dict_list:
				if _ref_run['gain']      != _run['gain']: continue
				if _ref_run['partition'] != _run['partition']: continue
				if _ref_run['type']      != _run['type']: continue

				if 'PEDESTAL' in _run['type']:
					_tot_num_process += 3
				elif 'RAMP' in _run['type']:
					_tot_num_process += 1
				elif 'DELAY' in _run['type']:
					_tot_num_process += 2
					if self.with_ofc:
						_tot_num_process += 1

		_num_process = 0
		for _ref_run in self.run_dict_list_ref:
			if _ref_run['gain']!=self.gain:
				continue
			for _run in self.run_dict_list:
				if _ref_run['gain']      != _run['gain']: continue
				if _ref_run['partition'] != _run['partition']: continue
				if _ref_run['type']      != _run['type']: continue

				# PEDESTAL validation
				if _run['type']=='PEDESTAL':

					_num_process += 1
					if self.verbose["low"]:
						print MED_INDENT,"%s:     processing %-8s for %-7s (%2d/%2d)" % (self.id_name, 'AUTOCORR', _run['partition'], _num_process, _tot_num_process)
					# - auto
					if self.mode_febs:
						self.ValidationAutoFEBs(_ref_run, _run)
					else:
						self.ValidationAuto(_ref_run, _run)

					_num_process += 1
					if self.verbose["low"]:
						print MED_INDENT,"%s:     processing %-8s for %-7s (%2d/%2d)" % (self.id_name, 'PEDESTAL', _run['partition'], _num_process, _tot_num_process)
					# - pedestal
					if self.mode_febs:
						self.ValidationPedFEBs(_ref_run, _run)
					else:
						self.ValidationPed(_ref_run, _run)

					_num_process += 1
					if self.verbose["low"]:
						print MED_INDENT,"%s:     processing %-8s for %-7s (%2d/%2d)" % (self.id_name, 'NOISE', _run['partition'], _num_process, _tot_num_process)
					# - noise (aka rms)
					if self.mode_febs:
						self.ValidationNoiseFEBs(_ref_run, _run)
					else:
						self.ValidationNoise(_ref_run, _run)

				# RAMP validation
				if _run['type']=='RAMP':

					_num_process += 1
					if self.verbose["med"]:
						print MED_INDENT,"%s:     processing %-8s for %-7s (%2d/%2d)" % (self.id_name, 'RAMP', _run['partition'], _num_process, _tot_num_process)
					# - ramp
					if self.mode_febs:
						self.ValidationRampFEBs(_ref_run, _run)
					else:
						self.ValidationRamp(_ref_run, _run)

				# DELAY validation
				if _run['type']=='DELAY':

					_num_process += 1
					if self.verbose["low"]:
						print MED_INDENT,"%s:     processing %-8s for %-7s (%2d/%2d)" % (self.id_name, 'PEAKTIME', _run['partition'], _num_process, _tot_num_process)
					# - peak time
					if self.mode_febs:
						self.ValidationPeakTimeFEBs(_ref_run, _run)
					else:
						self.ValidationPeakTime(_ref_run, _run)

					_num_process += 1
					if self.verbose["low"]:
						print MED_INDENT,"%s:     processing %-8s for %-7s (%2d/%2d)" % (self.id_name, 'DELAY', _run['partition'], _num_process, _tot_num_process)
					# - delay
					if self.mode_febs:
						self.ValidationDelayFEBs(_ref_run, _run)
					else:
						self.ValidationDelay(_ref_run, _run)

					if self.with_ofc:
						_num_process += 1
						if self.verbose["low"]:
							print MED_INDENT,"%s:     processing %-8s for %-7s (%2d/%2d)" % (self.id_name, 'OFC', _run['partition'], _num_process, _tot_num_process)
						# - ofc
						if self.mode_febs:
							self.ValidationOFC(_ref_run, _run)
						else:
							self.ValidationOFC(_ref_run, _run)

		_file_time_delta = datetime.now() - _begin_file_time
		if self.verbose["low"]:
			print MED_INDENT,"%s: elapsed time on FILE process %s" % (self.id_name, timedelta(seconds=_file_time_delta.seconds))
		number_process = 1
		# FEB process
		if self.verbose["low"]:
			print "\n",MED_INDENT,"%s: starting FEB process" % self.id_name
		_begin_feb_time = datetime.now()
		self.BuildFebDict()
		if number_process>1:
			size_dict_feb = len(self.resultFeb)// number_process
			list_dict_feb = []
			list_dict_feb.append(self.resultFeb[0:size_dict_feb])
			for i in range(1,number_process-1) :
				d1 = self.resultFeb[size_dict_feb*i:(size_dict_feb)*(i+1)]
				list_dict_feb.append(d1)
			list_dict_feb.append(self.resultFeb[size_dict_feb*(number_process-1):])
			list_process = []
			for _i in range(0, number_process):
				p = multiprocessing.Process(target=self.BuildFebDictProcess, args=(list_dict_feb[_i],))
				p.start()
				list_process.append(p)
			for job in list_process:
				job.join()
		else:
			self.BuildFebDictProcess(self.resultFeb)
		_feb_time_delta = datetime.now() - _begin_feb_time
		if self.verbose["low"]:
			print MED_INDENT,"%s: elapsed time on FEB process %s" % (self.id_name, timedelta(seconds=_feb_time_delta.seconds))
		# CHANNEL process
		if self.verbose["low"]:
			print "\n",MED_INDENT,"%s: starting CHANNEL process" % self.id_name
		_begin_channel_time = datetime.now()
		self.BuildChannelDict()
		if number_process>1:
			size_dict_ch = len(self.resultChannel)// number_process
			list_dict_ch = []
			list_dict_ch.append(self.resultChannel[0:size_dict_ch])
			for i in range(1,number_process-1):
				d1 = self.resultChannel[size_dict_ch*i:(size_dict_ch)*(i+1)]
				list_dict_ch.append(d1)
			list_dict_ch.append(self.resultChannel[size_dict_ch*(number_process-1):])
			list_process_ch = []
			for _i in range(0, number_process):
				if self.verbose['med']:
					print MED_INDENT,"%s: loop_id %d. Build channel dict in progress, deviation channel array has %d items" \
						% (self.id_name, _i+1, len(list_dict_ch[_i]))
				p = multiprocessing.Process(target=self.BuildChannelDictProcess, args=(list_dict_ch[_i], _i+1,))
				p.start()
				list_process_ch.append(p)
			for job in list_process_ch:
				job.join()
		else:
			self.BuildChannelDictProcess(self.resultChannel)
		_channel_time_delta = datetime.now() - _begin_channel_time
		if self.verbose["low"]:
			print MED_INDENT,"%s: elapsed time on CHANNEL process %s" % (self.id_name, timedelta(seconds=_channel_time_delta.seconds))
		return True


	# ----- BuildFebDict() -----
	def BuildFebDict(self):
		for _name, _deviations in self.deviatingFebDict.iteritems():
			deviation = {}
			average   = {}
			for _dev_name in FEB_DEV_NAME_LIST:
				deviation[_dev_name] = {}
				average[_dev_name]   = {}
				for _gain_name in FEB_GAIN_NAME_LIST:
					deviation[_dev_name][_gain_name] = 0
					average[_dev_name][_gain_name]   = 0

			for _dev in _deviations:
				deviation[_dev[0]][_dev[1]] = _dev[2]
				if _dev[0]=='PEDESTAL':
					average[_dev[0]][_dev[1]] = _dev[3]

			_res = {}

			_res['campaign'         ] = int(_name.split('_')[3])
			_res['sub_sub_partition'] =     _name.split('_')[0]
			_res['ft'               ] = int(_name.split('_')[1])
			_res['sl'               ] = int(_name.split('_')[2])
			_res['temperature'      ] = 1.0
			_res['energy_h'         ] = float(average['PEDESTAL']['H'])
			_res['energy_m'         ] = float(average['PEDESTAL']['M'])
			_res['energy_l'         ] = float(average['PEDESTAL']['L'])

			_res['deviation1_h'] = float(deviation['AUTOCORR']['H'])
			_res['deviation1_m'] = float(deviation['AUTOCORR']['M'])
			_res['deviation1_l'] = float(deviation['AUTOCORR']['L'])

			_res['deviation2_h'] = float(deviation['FEBTIME']['HM'])
			_res['deviation2_m'] = float(deviation['FEBTIME']['ML'])

			_res['deviation3_h'] = float(deviation['PEDESTAL']['H'])
			_res['deviation3_m'] = float(deviation['PEDESTAL']['M'])
			_res['deviation3_l'] = float(deviation['PEDESTAL']['L'])

			_res['deviation4_h'] = float(deviation['NOISE']['H'])
			_res['deviation4_m'] = float(deviation['NOISE']['M'])
			_res['deviation4_l'] = float(deviation['NOISE']['L'])

			_res['deviation5_h'] = float(deviation['RAMP']['H'])
			_res['deviation5_m'] = float(deviation['RAMP']['M'])
			_res['deviation5_l'] = float(deviation['RAMP']['L'])

			_res['deviation7_h'] = float(deviation['DELAY']['H'])
			_res['deviation7_m'] = float(deviation['DELAY']['M'])
			_res['deviation7_l'] = float(deviation['DELAY']['L'])

			_res['deviation9_h'] = float(deviation['PEAKTIME']['H'])
			_res['deviation9_m'] = float(deviation['PEAKTIME']['M'])
			_res['deviation9_l'] = float(deviation['PEAKTIME']['L'])

			self.resultFeb.append(_res)


	# ----- BuildFebDictProcess() -----
	def BuildFebDictProcess(self, dev_feb_dict):
		_lib = ECalDBLib.ECalDBLib()
		# _lib.Open('sqlite://' + self.db_names['copy'])
		# _lib.Execute("BEGIN TRANSACTION")
		# _resultFeb = []
		# for _dict_feb in dev_feb_dict:
		# 	# just add comment
		# 	if 'comment' not in _dict_feb.keys():
		# 		_prevComment = '';
		# 		_list_comment = _lib.GetFebComments(_dict_feb['sub_sub_partition'], _dict_feb['ft'], _dict_feb['sl'])
		# 		if _list_comment!=[]:
		# 			_list_comment.reverse()
		# 			for _comment in _list_comment:
		# 				if _comment['campaign']<_dict_feb['campaign']:
		# 					_prevComment = _comment['comment']
		# 					break
		# 		_dict_feb['comment'] = _prevComment

		# 	_resultFeb.append(_dict_feb)
		# _lib.Close()
		_lib = ECalDBLib.ECalDBLib()
		_lib.Open('sqlite://' + self.db_names['copy'])
		# for _dict_feb in _resultFeb:
		for dev_feb in dev_feb_dict:
			if 'comment' not in dev_feb.keys():
				dev_feb['comment'] = ''
			_lib.AddFebEntry(dev_feb)
		_lib.Commit()
		_lib.Close()


	# ----- BuildChannelDict() -----
	def BuildChannelDict(self):
		for _name, _deviations in self.deviatingChannelDict.iteritems():
			badChan = 0
			cl = 0
			deviation={}
			for _dev_name in CHANNEL_DEV_NAME_LIST:
				deviation[_dev_name] = {}
				for _gain_name in CHANNEL_GAIN_NAME_LIST:
					deviation[_dev_name][_gain_name] = 0

			for _dev in _deviations:
				badChan = int(_dev[0])
				cl = int(_dev[4])
				deviation[_dev[1]][_dev[2]] = _dev[3]

			_res = {}
			_res['campaign'         ] = int(_name.split('_')[4])
			_res['sub_sub_partition'] =     _name.split('_')[0]
			_res['ft'               ] = int(_name.split('_')[1])
			_res['sl'               ] = int(_name.split('_')[2])
			_res['ch'               ] = int(_name.split('_')[3])
			_res['cl'               ] = cl
			_res['is_bad'           ] = badChan

			if float(deviation['AUTOCORR']['H'])!=0: _res['deviation1_h'] = float(deviation['AUTOCORR']['H'])
			if float(deviation['AUTOCORR']['M'])!=0: _res['deviation1_m'] = float(deviation['AUTOCORR']['M'])
			if float(deviation['AUTOCORR']['L'])!=0: _res['deviation1_l'] = float(deviation['AUTOCORR']['L'])

			if float(deviation['PEDESTAL']['H'])!=0: _res['deviation3_h'] = float(deviation['PEDESTAL']['H'])
			if float(deviation['PEDESTAL']['M'])!=0: _res['deviation3_m'] = float(deviation['PEDESTAL']['M'])
			if float(deviation['PEDESTAL']['L'])!=0: _res['deviation3_l'] = float(deviation['PEDESTAL']['L'])

			if float(deviation['NOISE']['H'])!=0: _res['deviation4_h'] = float(deviation['NOISE']['H'])
			if float(deviation['NOISE']['M'])!=0: _res['deviation4_m'] = float(deviation['NOISE']['M'])
			if float(deviation['NOISE']['L'])!=0: _res['deviation4_l'] = float(deviation['NOISE']['L'])

			if float(deviation['RAMP']['H'])!=0: _res['deviation5_h'] = float(deviation['RAMP']['H'])
			if float(deviation['RAMP']['M'])!=0: _res['deviation5_m'] = float(deviation['RAMP']['M'])
			if float(deviation['RAMP']['L'])!=0: _res['deviation5_l'] = float(deviation['RAMP']['L'])

			if float(deviation['corrUndoRAMP']['H'])!=0: _res['deviation6_h'] = float(deviation['corrUndoRAMP']['H'])
			if float(deviation['corrUndoRAMP']['M'])!=0: _res['deviation6_m'] = float(deviation['corrUndoRAMP']['M'])
			if float(deviation['corrUndoRAMP']['L'])!=0: _res['deviation6_l'] = float(deviation['corrUndoRAMP']['L'])

			if float(deviation['DELAY']['H'])!=0: _res['deviation7_h'] = float(deviation['DELAY']['H'])
			if float(deviation['DELAY']['M'])!=0: _res['deviation7_m'] = float(deviation['DELAY']['M'])
			if float(deviation['DELAY']['L'])!=0: _res['deviation7_l'] = float(deviation['DELAY']['L'])

			if float(deviation['corrUndoDELAY']['H'])!=0: _res['deviation8_h'] = float(deviation['corrUndoDELAY']['H'])
			if float(deviation['corrUndoDELAY']['M'])!=0: _res['deviation8_m'] = float(deviation['corrUndoDELAY']['M'])
			if float(deviation['corrUndoDELAY']['L'])!=0: _res['deviation8_l'] = float(deviation['corrUndoDELAY']['L'])

			if float(deviation['PEAKTIME']['H'])!=0: _res['deviation9_h'] = float(deviation['PEAKTIME']['H'])
			if float(deviation['PEAKTIME']['M'])!=0: _res['deviation9_m'] = float(deviation['PEAKTIME']['M'])
			if float(deviation['PEAKTIME']['L'])!=0: _res['deviation9_l'] = float(deviation['PEAKTIME']['L'])

			if float(deviation['PEAKTIME']['H'])!=0: _res['deviation10_h'] = float(deviation['corrUndoPEAKTIME']['H'])
			if float(deviation['PEAKTIME']['M'])!=0: _res['deviation10_m'] = float(deviation['corrUndoPEAKTIME']['M'])
			if float(deviation['PEAKTIME']['L'])!=0: _res['deviation10_l'] = float(deviation['corrUndoPEAKTIME']['L'])

			self.resultChannel.append(_res)


	# ----- BuildChannelDictProcess() -----
	def BuildChannelDictProcess(self, dev_ch_dict, loop_id=0):
		# - append channel dictionary
		_lib = ECalDBLib.ECalDBLib()
		# _lib.Open('sqlite://' + self.db_names['copy'])
		# _lib.Execute("BEGIN TRANSACTION")
		# if self.verbose["high"]:
		# 	print MED_INDENT,"%s: loop_id %d. \tDB was opened as %s for apending channel dict" \
		# 		% (self.id_name, loop_id, self.db_names['copy'])
		# _resultChannel = []
		# dev_ch_list_size = len(dev_ch_dict)
		# dev_print_percent = int(0.1*dev_ch_list_size + 1)
		# for num_dev_ch, _ch_dict in enumerate(dev_ch_dict):
		# 	# - just add comment
		# 	if 'comment' not in _ch_dict.keys():
		# 		_prev_comment = '';
		# 		_comment_list = _lib.GetChannelComments(_ch_dict['sub_sub_partition'], _ch_dict['ft'], _ch_dict['sl'], _ch_dict['ch'])
		# 		if _comment_list!=[]:
		# 			_comment_list.reverse()
		# 			for _comment in _comment_list:
		# 				if _comment['campaign']<_ch_dict['campaign']:
		# 					_prev_comment = _comment['comment']
		# 					break
		# 		_ch_dict['comment'] = _prev_comment
		# 		print _prev_comment
		# 	_resultChannel.append(_ch_dict)
		# 	if self.verbose["high"] and num_dev_ch%dev_print_percent==0:
		# 		dev_percent_tmp = 100*num_dev_ch//dev_ch_list_size
		# 		if dev_percent_tmp>0:
		# 			print MED_INDENT,"%s: loop_id %d. \t\t%3d%% deviation channels were processed" \
		# 				% (self.id_name, loop_id, dev_percent_tmp)
		# _lib.Close()
		# - fill channel
		_lib = ECalDBLib.ECalDBLib()
		# _lib.Execute("BEGIN TRANSACTION")
		_lib.Open('sqlite://' + self.db_names['copy'])
		if self.verbose["high"] and loop_id!=0:
			print MED_INDENT,"%s: loop_id %d. \tDB was opened as %s for filling channel dict" \
				% (self.id_name, loop_id, self.db_names['copy'])
		# ch_list_size = len(_resultChannel)
		# ch_list_size = len(dev_ch_dict)
		# print_percent = int(0.1*ch_list_size + 1)
		# for num_ch, _res in enumerate(_resultChannel):
		for num_ch, dev_ch in enumerate(dev_ch_dict):
			if 'comment' not in dev_ch.keys():
				dev_ch['comment'] = ''
			_lib.AddChannelEntry(dev_ch)
			# if self.verbose["high"] and num_ch%print_percent==0:
			# 	percent_tmp = 100*num_ch//ch_list_size
			# 	if percent_tmp>0:
			# 		print MED_INDENT,"%s: loop_id %d. \t\t%3d%% deviation channel were added" \
			# 			% (self.id_name, loop_id, percent_tmp)
		_lib.Commit()
		_lib.Close()


	# ----- ParseOnlineID() ------
	def ParseOnlineID(self, _id):
		_ch   = (_id>>8)&0x7F
		#_slot = (_id>>15)&0xF + 1
		_slot = (_id>>15)&0xF
		_ft   = (_id>>19)&0x1F
		_side = (_id>>24)&0x1
		_be   = (_id>>25)&0x1
		return _ch, _slot, _ft, _side, _be


	# =========================================================
	# c) ALGORITHM PART simultaneous cycles
	# =========================================================

	# ----- 1. ValidationAuto() -----
	def ValidationAuto(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'AUTOCORR'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		# definition of the histograms
		ntuple          = dict()
		his_autocorr    = dict()
		his_autocorr_ft = dict()
		his_autocorr_cl = dict()
		his_occupancy   = dict()
		his_feb_mean    = dict()
		h_temp = TH1F("h_temp", "h_temp", 100, 0, 0)
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][4]

				ntuple[_sub_part] = TNtuple("Ntuple_" + _sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:Autocorr:DeltaAutocorr:threshold")
				his_autocorr[_sub_part] = TH1F("h_Delta_AUTOCORR_" + _sub_part, \
					"AutoCorr relative differ ", 500, -2*_threshold, +2*_threshold)
				his_autocorr_ft[_sub_part] = TH2F("h_Delta_AUTOCORRVSft_" + _sub_part, \
					"AutoCorr relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_autocorr_cl[_sub_part] = TH2F("h_Delta_AUTOCORRVScl_" + _sub_part, \
					"AutoCorr relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" + _sub_part, \
					"h_Occupancy", 33, -16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_AUTOCORR_" +_sub_part, "Mean Auto per FEB", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_AUTOCORR, refRunDict, curRunDict)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue

			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_ref_total_entries = _ref_tree.GetEntries()
			_cur_total_entries = _cur_tree.GetEntries()
			if (_ref_total_entries!=_cur_total_entries) and self.verbose["high"]:
				print MED_INDENT,"%s: WARNING: %s reference and current trees do not have the same number of entries" %\
					(self.id_name, _valid_type)

			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, 'ALL')
			_print_percent = int(0.2*_cur_total_entries + 1)

			_ref_entry =  0
			_cur_entry =  0

			_next_ft   = -999
			_next_slot = -999

			while (_ref_entry<_ref_total_entries) and (_cur_entry<_cur_total_entries):
				if self.verbose["med"] and _cur_entry%_print_percent==0:
					_percent = 100*_cur_entry//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, 'ALL')

				# reference data
				_ref_tree.GetEntry(_ref_entry)
				_ref_ch_id     = _ref_tree.channelId
				_ref_autoCorr  = _ref_tree.covr[0]
				_ref_calibLine = _ref_tree.calibLine
				_ref_badChan   = _ref_tree.badChan

				# compared data
				_cur_tree.GetEntry(_cur_entry)
				_cur_ch_id     = _cur_tree.channelId
				_cur_autoCorr  = _cur_tree.covr[0]
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan

				# compare the two tree
				if _ref_ch_id==_cur_ch_id:
					# compute the variable
					_delta_autoCorr = _ref_autoCorr - _cur_autoCorr

					# loop over the different partition
					for _part_all in LIST_OF_PART[_run_part]: # because PEDESTAL runs have all partitions
						for _sub_part in LIST_OF_SUB_PART[_part_all]:
							if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):
								# threshold from DB
								_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][4]
								# fill the corresponding histograms
								ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
									_cur_ft, _cur_slot, _cur_channel,_cur_calibLine, _cur_badChan, _cur_autoCorr, \
									_delta_autoCorr, _db_treshold)

								# look at the FEB
								if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
									# fill occupancy
									_coeff = 1 if _cur_pos_neg==1 else -1
									his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

									# get the mean for the FEB
									if h_temp.GetEntries()!=0:
										FebAverage = h_temp.GetMean()
										his_feb_mean[_sub_part].Fill(FebAverage)
										# flag deviating FEB
										if abs(FebAverage)>ECalDBLib.thresholdDict[_sub_part][_run_gain][5]:
											febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
											if febName in self.deviatingFebList:
												self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
											else:
												self.deviatingFebDict[febName] = []
												self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
												self.deviatingFebList.append(febName)
									_next_ft   = _cur_ft
									_next_slot = _cur_slot
									h_temp.Reset()

								# look at the channels
								if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
									if not('deadCalib' in BadChanInterpreter(_cur_badChan)):
										continue
								if _ref_calibLine<0 or _cur_calibLine<0:
									continue
								h_temp.Fill(_delta_autoCorr)

								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_autocorr[_sub_part].Fill(_delta_autoCorr)
								his_autocorr_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_autoCorr)
								his_autocorr_cl[_sub_part].Fill(_cur_calibLine, _delta_autoCorr)

								# flag deviating channels
								if abs(_delta_autoCorr)>_db_treshold:
									channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + str(_cur_channel) + '_' + _run_camp
									if channelName in self.deviatingChannelList:
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_autoCorr, _cur_calibLine])
									else:
										self.deviatingChannelDict[channelName] = []
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_autoCorr, _cur_calibLine])
										self.deviatingChannelList.append(channelName)

					_ref_entry += 1
					_cur_entry += 1

				# sync the tree entries if needed
				elif _ref_ch_id>_cur_ch_id:
					_cur_entry += 1
				elif _ref_ch_id<_cur_ch_id:
					_ref_entry += 1

			if self.verbose["med"]:
				_percent = 100*_cur_entry//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
					(self.id_name, _percent, _valid_type, 'ALL')

		# save otput to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_autocorr[_sub_part].Write()
				his_autocorr_ft[_sub_part].Write()
				his_autocorr_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 2. ValidationPed() -----
	def ValidationPed(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'PEDESTAL'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		# definition of the histograms
		ntuple          = dict()
		his_pedestal    = dict()
		his_pedestal_ft = dict()
		his_pedestal_cl = dict()
		his_occupancy   = dict()
		his_feb_mean    = dict()

		h_temp          = TH1F("h_temp", "h_temp", 100, 0, 0);
		h_temp_absolute = TH1F("h_temp_abs", "h_temp_abs", 10000, -1000, -1000);
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][0]

				ntuple[_sub_part] = TNtuple("Ntuple_"+_sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:ped:DeltaPed:threshold")
				his_pedestal[_sub_part] = TH1F("h_Delta_PEDESTAL_" + _sub_part, \
					"PEDESTAL relative differ ", 500, -2*_threshold, +2*_threshold)
				his_pedestal_ft[_sub_part] = TH2F("h_Delta_PEDESTALVSft_" + _sub_part, \
					"PEDESTAL relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_pedestal_cl[_sub_part] = TH2F("h_Delta_PEDESTALVScl_" + _sub_part, \
					"PEDESTAL relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" + _sub_part, \
					"h_Occupancy", 33, -16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_PEDESTAL_" + _sub_part, "Mean Ped per FEB", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_PEDESTAL, refRunDict, curRunDict)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue
			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_ref_total_entries = _ref_tree.GetEntries()
			_cur_total_entries = _cur_tree.GetEntries()
			if (_ref_total_entries!=_cur_total_entries) and self.verbose["high"]:
				print MED_INDENT,"%s: WARNING: %s reference and current trees do not have the same number of entries" %\
					(self.id_name, _valid_type)

			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, 'ALL')
			_print_percent = int(0.2*_cur_total_entries + 1)

			_ref_entry  =  0
			_cur_entry  =  0

			_next_ft   = -999
			_next_slot = -999

			while (_ref_entry<_ref_total_entries) and (_cur_entry<_cur_total_entries):
				if self.verbose["med"] and _cur_entry%_print_percent==0:
					_percent = 100*_cur_entry//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, 'ALL')

				# reference data
				_ref_tree.GetEntry(_ref_entry)
				_ref_ch_id     = _ref_tree.channelId
				_ref_ped       = _ref_tree.ped
				_ref_calibLine = _ref_tree.calibLine
				_ref_badChan   = _ref_tree.badChan

				# compared data
				_cur_tree.GetEntry(_cur_entry)
				_cur_ch_id     = _cur_tree.channelId
				_cur_ped       = _cur_tree.ped
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan

				# compare the two tree
				if _ref_ch_id==_cur_ch_id:
					# compute the variable
					_delta_ped = _ref_ped - _cur_ped;

					# loop over the different partition
					for _part_all in LIST_OF_PART[_run_part]:  # because PEDESTAL runs have all partitions
						for _sub_part in LIST_OF_SUB_PART[_part_all]:
							if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):
								# threshold from DB
								_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][0]
								# fill the corresponding histograms
								ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
									_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_ped, \
									_delta_ped, _db_treshold);

								# look at the FEB
								if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
									# fill occupancy
									_coeff = 1 if _cur_pos_neg==1 else -1
									his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

									# get the mean deviation for the FEB
									FebAverage=0
									if h_temp.GetEntries()!=0:
										FebAverage = h_temp.GetMean()

									if abs(FebAverage)<ECalDBLib.thresholdDict[_sub_part][_run_gain][2]:
										FebAverage=0 # avr = 0 if no deviation
									his_feb_mean[_sub_part].Fill(FebAverage);

									# get the mean absolute ped for the FEB
									FebPed = 0
									if h_temp_absolute.GetEntries()!=0:
										FebPed = h_temp_absolute.GetMean()

									# store FEB info
									febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
									if febName in self.deviatingFebList:
										self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage, FebPed])
									else:
										self.deviatingFebDict[febName] = []
										self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage, FebPed])
										self.deviatingFebList.append(febName)

									_next_ft   = _cur_ft
									_next_slot = _cur_slot
									h_temp.Reset()
									h_temp_absolute.Reset()

								# look at the channels
								if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
									if not('deadCalib' in BadChanInterpreter(_cur_badChan)):
										continue

								if _ref_calibLine<0 or _cur_calibLine<0:
									continue

								h_temp.Fill(_delta_ped)
								h_temp_absolute.Fill(_cur_tree.ped)

								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_pedestal[_sub_part].Fill(_delta_ped)
								his_pedestal_ft[_sub_part].Fill(_cur_ft * 13 + _cur_slot - 2, _delta_ped)
								his_pedestal_cl[_sub_part].Fill(_cur_calibLine, _delta_ped)

								# flag deviating channels
								if abs(_delta_ped)>_db_treshold:
									channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + str(_cur_channel) + '_' + _run_camp
									if channelName in self.deviatingChannelList:
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_ped, _cur_calibLine])
									else:
										self.deviatingChannelDict[channelName] = []
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_ped, _cur_calibLine])
										self.deviatingChannelList.append(channelName)

					_ref_entry += 1
					_cur_entry += 1

				# sync the tree entries if needed
				elif _ref_ch_id > _cur_ch_id:
					_cur_entry += 1
				elif _ref_ch_id < _cur_ch_id:
					_ref_entry += 1

			if self.verbose["med"]:
				_percent = 100*_cur_entry//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
					(self.id_name, _percent, _valid_type, 'ALL')

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_pedestal[_sub_part].Write()
				his_pedestal_ft[_sub_part].Write()
				his_pedestal_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 3. ValidationNoise() ------
	def ValidationNoise(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'NOISE'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		# definition of the histograms
		ntuple        = dict()
		his_noise     = dict()
		his_noise_ft  = dict()
		his_noise_cl  = dict()
		his_occupancy = dict()
		his_feb_mean  = dict()

		h_temp = TH1F("h_temp","h_temp", 100, 0, 0)
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][1]

				ntuple[_sub_part] = TNtuple("Ntuple_" + _sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:NOISE:DeltaNOISE:threshold")
				his_noise[_sub_part] = TH1F("h_Delta_NOISE_" + _sub_part, \
					"NOISE relative differ ", 500, -2*_threshold, +2*_threshold)
				his_noise_ft[_sub_part] = TH2F("h_Delta_NOISEVSft_" +_sub_part, \
					"NOISE relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_noise_cl[_sub_part] = TH2F("h_Delta_NOISEVScl_" +_sub_part, \
					"NOISE relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" + _sub_part, \
					"h_Occupancy", 33, -16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_NOISE_" + _sub_part, "Mean NOISE per FEB", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_NOISE, refRunDict, curRunDict)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue

			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_ref_total_entries = _ref_tree.GetEntries()
			_cur_total_entries = _cur_tree.GetEntries()
			if (_ref_total_entries!=_cur_total_entries) and self.verbose["high"]:
				print MED_INDENT,"%s: WARNING: %s reference and current trees do not have the same number of entries" %\
					(self.id_name, _valid_type)

			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries, _valid_type, 'ALL')
			_print_percent = int(0.2*_cur_total_entries + 1)

			_ref_entry  =  0
			_cur_entry  =  0

			_next_ft   = -999
			_next_slot = -999

			while (_ref_entry<_ref_total_entries) and (_cur_entry<_cur_total_entries):
				if self.verbose["med"] and _cur_entry%_print_percent==0:
					_percent = 100*_cur_entry//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, 'ALL')

				# reference data
				_ref_tree.GetEntry(_ref_entry)
				_ref_ch_id     = _ref_tree.channelId
				_ref_rms       = _ref_tree.rms
				_ref_calibLine = _ref_tree.calibLine
				_ref_badChan   = _ref_tree.badChan

				# compared data
				_cur_tree.GetEntry(_cur_entry)
				_cur_ch_id     = _cur_tree.channelId
				_cur_rms       = _cur_tree.rms
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan

				if _ref_ch_id==_cur_ch_id:
					# compute the variable
					_delta_rms = _ref_rms - _cur_rms;

					# loop over the different partition
					for _part_all in LIST_OF_PART[_run_part]: # because PEDESTAL runs have all partitions
						for _sub_part in LIST_OF_SUB_PART[_part_all]:
							if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):
								# threshold from DB
								_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][1]
								# fill the corresponding histograms
								ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
									_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_rms, \
									_delta_rms, _db_treshold)

								# look at the FEB
								if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
									# fill occupancy
									_coeff = 1 if _cur_pos_neg==1 else -1
									his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

									# get the mean for the FEB
									if h_temp.GetEntries()!=0:
										FebAverage = h_temp.GetMean();
										his_feb_mean[_sub_part].Fill(FebAverage);

										# flag deviating FEB
										if abs(FebAverage)>ECalDBLib.thresholdDict[_sub_part][_run_gain][3]:
											febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
											if febName in self.deviatingFebList:
												self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
											else:
												self.deviatingFebDict[febName] = []
												self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
												self.deviatingFebList.append(febName)

									_next_ft   = _cur_ft
									_next_slot = _cur_slot
									h_temp.Reset()

								# look at the channels
								if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
									if not('deadCalib' in BadChanInterpreter(_cur_badChan)):
										continue
								if _ref_calibLine<0 or _cur_calibLine<0:
									continue

								h_temp.Fill(_delta_rms)

								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_noise[_sub_part].Fill(_delta_rms)
								his_noise_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_rms)
								his_noise_cl[_sub_part].Fill(_cur_calibLine, _delta_rms)

								# flag deviating channels
								if abs(_delta_rms)>_db_treshold:
									channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + str(_cur_channel) + '_' + _run_camp
									if channelName in self.deviatingChannelList:
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_rms, _cur_calibLine])
									else:
										self.deviatingChannelDict[channelName] = []
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_rms, _cur_calibLine])
										self.deviatingChannelList.append(channelName)

					_ref_entry += 1
					_cur_entry += 1

				# sync the tree entries if needed
				elif _ref_ch_id>_cur_ch_id:
					_cur_entry += 1
				elif _ref_ch_id<_cur_ch_id:
					_ref_entry += 1

			if self.verbose["med"]:
				_percent = 100*_cur_entry//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
					(self.id_name, _percent, _valid_type, 'ALL')

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_noise[_sub_part].Write()
				his_noise_ft[_sub_part].Write()
				his_noise_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 4. ValidationRamp() -----
	def ValidationRamp(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'RAMP'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		# definition of the histograms
		ntuple        = dict()
		his_ramp      = dict()
		his_ramp_ft   = dict()
		his_ramp_cl   = dict()
		his_occupancy = dict()
		his_feb_mean  = dict()

		h_temp = TH1F("h_temp", "h_temp", 100, 0, 0)
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][8]
				ntuple[_sub_part] = TNtuple("Ntuple_" + _sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:corrUndo:RAMP:DeltaRAMP:threshold")
				his_ramp[_sub_part] = TH1F("h_Delta_RAMP_" + _sub_part, \
					"RAMP relative differ ", 500, -2*_threshold, +2*_threshold)
				his_ramp_ft[_sub_part] = TH2F("h_Delta_RAMPVSft_" + _sub_part, \
					"RAMP relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_ramp_cl[_sub_part] = TH2F("h_Delta_RAMPVScl_" + _sub_part, \
					"RAMP relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" +_sub_part, \
					"h_Occupancy", 33 ,-16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_RAMP_" +_sub_part, "Mean RAMP per FEB", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_RAMP, refRunDict, curRunDict)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue

			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_ref_total_entries = _ref_tree.GetEntries()
			_cur_total_entries = _cur_tree.GetEntries()
			if (_ref_total_entries!=_cur_total_entries) and self.verbose["high"]:
				print MED_INDENT,"%s: WARNING: %s reference and current trees do not have the same number of entries" %\
					(self.id_name, _valid_type)

			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, _part)
			_print_percent = int(0.2*_cur_total_entries + 1)

			_ref_entry  =  0
			_cur_entry  =  0

			_next_ft   = -999
			_next_slot = -999

			while (_ref_entry<_ref_total_entries) and (_cur_entry<_cur_total_entries):
				if self.verbose["med"] and _cur_entry%_print_percent==0:
					_percent = 100*_cur_entry//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, _part)

				_ref_tree.GetEntry(_ref_entry)
				_cur_tree.GetEntry(_cur_entry)

				# check if X[1] exist
				if len(_ref_tree.X)==0:
					_ref_entry += 1
					continue
				elif _ref_tree.X[1]==0:
					_ref_entry += 1
					continue

				if len(_cur_tree.X)==0:
					_cur_entry += 1
					continue
				elif _cur_tree.X[1]==0:
					_cur_entry += 1
					continue

				# reference data
				_ref_ch_id     = _ref_tree.channelId
				_ref_X1        = _ref_tree.X[1]
				_ref_calibLine = _ref_tree.calibLine
				_ref_badChan   = _ref_tree.badChan
				_ref_corrUndo  = _ref_tree.corrUndo

				# compared data
				_cur_ch_id     = _cur_tree.channelId
				_cur_X1        = _cur_tree.X[1]
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan
				_cur_corrUndo  = _cur_tree.corrUndo

				# compare the two tree
				if _ref_ch_id == _cur_ch_id:
					# compute the variable
					_delta_ramp = (_ref_X1 - _cur_X1)/_ref_X1

					# loop over the different partition
					for _sub_part in LIST_OF_SUB_PART[_part]:
						if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):
							# threshold from DB
							_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][8]

							# fill the corresponding histograms
							ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
								_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_corrUndo, \
									_cur_X1, _delta_ramp, _db_treshold)

							# look at the FEB
							if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
								# fill occupancy
								_coeff = 1 if _cur_pos_neg==1 else -1
								his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

								# get the mean for the FEB
								if h_temp.GetEntries()!=0:
									FebAverage = h_temp.GetMean();
									his_feb_mean[_sub_part].Fill(FebAverage);

									# flag deviating FEB
									if abs(FebAverage)>ECalDBLib.thresholdDict[_sub_part][_run_gain][9]:
										febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
										if febName in self.deviatingFebList:
											self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
										else:
											self.deviatingFebDict[febName] = []
											self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
											self.deviatingFebList.append(febName)

								_next_ft   = _cur_ft
								_next_slot = _cur_slot
								h_temp.Reset()

							# look at the channels
							if not(InList(_ref_badChan, PATCHED_BAD_CHANNEL)) and not(InList(_cur_badChan,PATCHED_BAD_CHANNEL)):
								if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
									continue
							if _ref_calibLine<0 or _cur_calibLine<0:
								continue
							if _ref_corrUndo!=_cur_corrUndo:
								continue

							if (_ref_corrUndo==0 and _cur_corrUndo==0):
								h_temp.Fill(_delta_ramp)

								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_ramp[_sub_part].Fill(_delta_ramp)
								his_ramp_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_ramp)
								his_ramp_cl[_sub_part].Fill(_cur_calibLine, _delta_ramp)

							# flag deviating channels
							if abs(_delta_ramp)>_db_treshold:
								channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + str(_cur_channel) + '_' + _run_camp
								if channelName in self.deviatingChannelList:
									if (_ref_corrUndo==0 and _cur_corrUndo==0):
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_ramp, _cur_calibLine])
									elif (_ref_corrUndo==1 and _cur_corrUndo==1):
										self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_ramp, _cur_calibLine])
								else:
									self.deviatingChannelDict[channelName] = []
									if (_ref_corrUndo==0 and _cur_corrUndo==0):
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_ramp, _cur_calibLine])
									elif (_ref_corrUndo==1 and _cur_corrUndo==1):
										self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_ramp, _cur_calibLine])
									self.deviatingChannelList.append(channelName)

					_ref_entry += 1
					_cur_entry += 1

				# sync the tree entries if needed
				elif _ref_ch_id>_cur_ch_id:
					_cur_entry += 1
				elif _ref_ch_id<_cur_ch_id:
					_ref_entry += 1

			if self.verbose["med"]:
				_percent = 100*_cur_entry//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
					(self.id_name, _percent, _valid_type, _part)

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_ramp[_sub_part].Write()
				his_ramp_ft[_sub_part].Write()
				his_ramp_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 5. ValidationPeakTime() -----
	def ValidationPeakTime(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'PEAKTIME'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		# definition of the histograms
		ntuple          = dict()
		his_peaktime    = dict()
		his_peaktime_ft = dict()
		his_peaktime_cl = dict()
		his_occupancy   = dict()
		his_feb_mean    = dict()

		h_temp = TH1F("h_temp", "h_temp", 100, 0, 0)
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][11]

				ntuple[_sub_part] = TNtuple("Ntuple_" + _sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:corrUndo:PEAKTIME:DeltaPEAKTIME:threshold")
				his_peaktime[_sub_part] = TH1F("h_Delta_PEAKTIME_" + _sub_part, \
					"PEAKTIME relative differ ", 500, -2*_threshold, +2*_threshold)
				his_peaktime_ft[_sub_part] = TH2F("h_Delta_PEAKTIMEVSft_" + _sub_part, \
					"PEAKTIME relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_peaktime_cl[_sub_part] = TH2F("h_Delta_PEAKTIMEVScl_" + _sub_part, \
					"PEAKTIME relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" + _sub_part, \
					"h_Occupancy", 33, -16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_PEAKTIME_" + _sub_part, "Mean PEAKTIME per FEB", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_PEAKTIME, refRunDict, curRunDict, _valid_type)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue

			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_ref_total_entries = _ref_tree.GetEntries()
			_cur_total_entries = _cur_tree.GetEntries()
			if (_ref_total_entries!=_cur_total_entries) and self.verbose["high"]:
				print MED_INDENT,"%s: WARNING: %s reference and current trees do not have the same number of entries" %\
					(self.id_name, _valid_type)

			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, _part)
			_print_percent = int(0.2*_cur_total_entries + 1)

			_ref_entry  =  0
			_cur_entry  =  0

			_next_ft   = -999
			_next_slot = -999

			while (_ref_entry<_ref_total_entries) and (_cur_entry<_cur_total_entries):
				if self.verbose["med"] and _cur_entry%_print_percent==0:
					_percent = 100*_cur_entry//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, _part)

				_ref_tree.GetEntry(_ref_entry)
				_cur_tree.GetEntry(_cur_entry)

				# check if TmaxAmp !=0
				if _ref_tree.TmaxAmp==0:
					_ref_entry += 1
					continue
				if _cur_tree.TmaxAmp==0:
					_cur_entry += 1
					continue

				# reference data
				_ref_ch_id     = _ref_tree.channelId
				_ref_TmaxAmp   = _ref_tree.TmaxAmp
				_ref_calibLine = _ref_tree.calibLine
				_ref_badChan   = _ref_tree.badChan
				_ref_corrUndo  = _ref_tree.corrUndo

				# compared data
				_cur_ch_id     = _cur_tree.channelId
				_cur_TmaxAmp   = _cur_tree.TmaxAmp
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan
				_cur_corrUndo  = _cur_tree.corrUndo

				# compare the two tree
				if _ref_ch_id==_cur_ch_id:
					# compute the variable
					_delta_peakTime = _ref_TmaxAmp - _cur_TmaxAmp

					# loop over the different partition
					for _sub_part in LIST_OF_SUB_PART[_part]:
						if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft,_cur_slot):
							# threshold from DB
							_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][11]

							# fill the corresponding histograms
							ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
								_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_corrUndo, \
								_cur_TmaxAmp, _delta_peakTime, _db_treshold)

							# look at the FEB
							if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
								# fill occupancy
								_coeff = 1 if _cur_pos_neg==1 else -1
								his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

								# get the mean for the FEB
								if h_temp.GetEntries()!=0:
									FebAverage = h_temp.GetMean();
									his_feb_mean[_sub_part].Fill(FebAverage);

									# flag deviating FEB
									if abs(FebAverage)>_db_treshold:
										febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
										if febName in self.deviatingFebList:
											self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
										else:
											self.deviatingFebDict[febName] = []
											self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
											self.deviatingFebList.append(febName)

								_next_ft   = _cur_ft
								_next_slot = _cur_slot
								h_temp.Reset()

							# look at the channels
							if not(InList(_ref_badChan, PATCHED_BAD_CHANNEL)) and not(InList(_cur_badChan, PATCHED_BAD_CHANNEL)):
								if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
									continue
							if _ref_calibLine<0 or _cur_calibLine<0: continue
							if _ref_corrUndo!=_cur_corrUndo: continue

							if (_ref_corrUndo==0 and _cur_corrUndo==0):
								h_temp.Fill(_delta_peakTime)

								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_peaktime[_sub_part].Fill(_delta_peakTime)
								his_peaktime_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_peakTime)
								his_peaktime_cl[_sub_part].Fill(_cur_calibLine, _delta_peakTime)

							# flag deviating channels
							if abs(_delta_peakTime)>_db_treshold:
								channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_'+str(_cur_channel) + '_' + _run_camp
								if channelName in self.deviatingChannelList:
									if (_ref_corrUndo==0 and _cur_corrUndo==0):
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_peakTime, _cur_calibLine])
									elif (_ref_corrUndo==1 and _cur_corrUndo==1):
										self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_peakTime, _cur_calibLine])
								else:
									self.deviatingChannelDict[channelName] = []
									if (_ref_corrUndo==0 and _cur_corrUndo==0):
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_peakTime, _cur_calibLine])
									elif (_ref_corrUndo==1 and _cur_corrUndo==1):
										self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_peakTime, _cur_calibLine])
									self.deviatingChannelList.append(channelName)

					_ref_entry += 1
					_cur_entry += 1

				# sync the tree entries if needed
				elif _ref_ch_id>_cur_ch_id:
					_cur_entry += 1
				elif _ref_ch_id<_cur_ch_id:
					_ref_entry += 1

			if self.verbose["med"]:
				_percent = 100*_cur_entry//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
					(self.id_name, _percent, _valid_type, _part)

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_peaktime[_sub_part].Write()
				his_peaktime_ft[_sub_part].Write()
				his_peaktime_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 6. ValidationDelay() -----
	def ValidationDelay(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'DELAY'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		# definition of the histograms
		ntuple        = dict()
		his_delay     = dict()
		his_delay_ft  = dict()
		his_delay_cl  = dict()
		his_occupancy = dict()
		his_feb_mean  = dict()

		h_temp = TH1F("h_temp", "h_temp", 100, 0, 0)
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][6]

				ntuple[_sub_part] = TNtuple("Ntuple_" + _sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:corrUndo:DELAY:DeltaDELAY:threshold")
				his_delay[_sub_part] = TH1F("h_Delta_DELAY_" + _sub_part, \
					"DELAY relative differ ", 500, -2*_threshold, +2*_threshold)
				his_delay_ft[_sub_part] = TH2F("h_Delta_DELAYVSft_" + _sub_part, \
					"DELAY relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_delay_cl[_sub_part] = TH2F("h_Delta_DELAYVScl_" + _sub_part, \
					"DELAY relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" + _sub_part, \
					"h_Occupancy", 33, -16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_DELAY_" + _sub_part, "Mean DELAY per FEB", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_DELAY, refRunDict, curRunDict)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue
			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_ref_total_entries = _ref_tree.GetEntries()
			_cur_total_entries = _cur_tree.GetEntries()
			if (_ref_total_entries!=_cur_total_entries) and self.verbose["high"]:
				print MED_INDENT,"%s: WARNING: %s reference and current trees do not have the same number of entries" %\
					(self.id_name, _valid_type)

			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, _part)
			_print_percent = int(0.2*_cur_total_entries + 1)

			_ref_entry  =  0
			_cur_entry  =  0

			_next_ft   = -999
			_next_slot = -999

			while (_ref_entry<_ref_total_entries) and (_cur_entry<_cur_total_entries):
				if self.verbose["med"] and _cur_entry%_print_percent==0:
					_percent = 100*_cur_entry//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, _part)

				_ref_tree.GetEntry(_ref_entry)
				_cur_tree.GetEntry(_cur_entry)

				# check if MaxAmp !=0
				if _ref_tree.MaxAmp==0:
					_ref_entry += 1
					continue
				if _cur_tree.MaxAmp==0:
					_cur_entry += 1
					continue

				# reference data
				_ref_ch_id     = _ref_tree.channelId
				_ref_MaxAmp    = _ref_tree.MaxAmp
				_ref_calibLine = _ref_tree.calibLine
				_ref_badChan   = _ref_tree.badChan
				_ref_corrUndo  = _ref_tree.corrUndo

				# compared data
				_cur_ch_id     = _cur_tree.channelId
				_cur_MaxAmp    = _cur_tree.MaxAmp
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan
				_cur_corrUndo  = _cur_tree.corrUndo

				# compare the two tree
				if _ref_ch_id == _cur_ch_id:
					# compute the variable
					_delta_delay = (_ref_MaxAmp - _cur_MaxAmp)/_ref_MaxAmp

					# loop over the different partition
					for _sub_part in LIST_OF_SUB_PART[_part]:
						if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):
							# threshold from DB
							_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][6]

							# fill the corresponding histograms
							ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
								_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_corrUndo, \
								_cur_MaxAmp, _delta_delay, _db_treshold)

							# look at the FEB
							if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
								# fill occupancy
								_coeff = 1 if _cur_pos_neg==1 else -1
								his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

								# get the mean for the FEB
								if h_temp.GetEntries()!=0:
									FebAverage = h_temp.GetMean();
									his_feb_mean[_sub_part].Fill(FebAverage);

									# flag deviating FEB
									if abs(FebAverage)>ECalDBLib.thresholdDict[_sub_part][_run_gain][7]:
										febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
										if febName in self.deviatingFebList:
											self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
										else:
											self.deviatingFebDict[febName] = []
											self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
											self.deviatingFebList.append(febName)

								_next_ft   = _cur_ft
								_next_slot = _cur_slot
								h_temp.Reset()

							# look at the channels
							if not(InList(_ref_badChan, PATCHED_BAD_CHANNEL)) and not(InList(_cur_badChan, PATCHED_BAD_CHANNEL)):
								if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
									continue
							if _ref_calibLine<0 or _cur_calibLine<0:
								continue
							if _ref_corrUndo!=_cur_corrUndo:
								continue

							if (_ref_corrUndo==0 and _cur_corrUndo==0):
								h_temp.Fill(_delta_delay)

								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_delay[_sub_part].Fill(_delta_delay)
								his_delay_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_delay)
								his_delay_cl[_sub_part].Fill(_cur_calibLine, _delta_delay)

							# flag deviating channels
							if abs(_delta_delay)>_db_treshold:
								channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + str(_cur_channel) + '_' + _run_camp
								if channelName in self.deviatingChannelList:
									if (_ref_corrUndo==0 and _cur_corrUndo==0):
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_delay, _cur_calibLine])
									elif (_ref_corrUndo==1 and _cur_corrUndo==1):
										self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_delay, _cur_calibLine])
								else:
									self.deviatingChannelDict[channelName] = []
									if (_ref_corrUndo==0 and _cur_corrUndo==0):
										self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_delay, _cur_calibLine])
									elif (_ref_corrUndo==1 and _cur_corrUndo==1):
										self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_delay, _cur_calibLine])
									self.deviatingChannelList.append(channelName)

					_ref_entry += 1
					_cur_entry += 1

				# sync the tree entries if needed
				elif _ref_ch_id>_cur_ch_id:
					_cur_entry += 1
				elif _ref_ch_id<_cur_ch_id:
					_ref_entry += 1

			if self.verbose["med"]:
				_percent = 100*_cur_entry//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
					(self.id_name, _percent, _valid_type, _part)

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_delay[_sub_part].Write()
				his_delay_ft[_sub_part].Write()
				his_delay_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 7. ValidationOFC() -----
	def ValidationOFC(self, refRunDict, curRunDict):
		_run_camp   = curRunDict['campaign']
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'OFC'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		# definition of the histograms
		h_OFC_cur = {}
		h_OFC_ref = {}
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				h_OFC_cur[_sub_part] = TH1F("h_OFC_cur_" + _sub_part, "OFCa for the current campaign ", 500, -0.5, 1)
				h_OFC_ref[_sub_part] = TH1F("h_OFC_ref_" + _sub_part, "OFCa for the refrent campaign ", 500, -0.5, 1)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_OFC, refRunDict, curRunDict, _valid_type)

		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue
			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_ref_total_entries = _ref_tree.GetEntries()
			_cur_total_entries = _cur_tree.GetEntries()
			if (_ref_total_entries!=_cur_total_entries) and self.verbose["high"]:
				print MED_INDENT,"%s: WARNING: %s reference and current trees do not have the same number of entries" %\
					(self.id_name, _valid_type)

			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, _part)
			_print_percent = int(0.2*_cur_total_entries + 1)

			_ref_entry  = 0
			_cur_entry  = 0

			while (_ref_entry<_ref_total_entries) and (_cur_entry<_cur_total_entries):
				if self.verbose["med"] and _cur_entry%_print_percent==0:
					_percent = 100*_cur_entry//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, _part)

				_ref_tree.GetEntry(_ref_entry)
				_cur_tree.GetEntry(_cur_entry)

				# reference data
				_ref_ch_id     = _ref_tree.channelId
				_ref_ofc_a     = _ref_tree.OFCa
				_ref_calibLine = _ref_tree.calibLine
				_ref_badChan   = _ref_tree.badChan

				# compared data
				_cur_ch_id     = _cur_tree.channelId
				_cur_ofc_a     = _cur_tree.OFCa
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan

				# compare the two tree
				if _ref_ch_id==_cur_ch_id:
					# loop over the different partition
					for _sub_part in LIST_OF_SUB_PART[_part]:
						if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):

							# look at the channels
							if not(InList(_ref_badChan, PATCHED_BAD_CHANNEL)) and not(InList(_cur_badChan, PATCHED_BAD_CHANNEL)):
								if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
									continue

							if _ref_calibLine<0 or _cur_calibLine<0:
								continue

							for _ofc in _cur_ofc_a:
								h_OFC_cur[_sub_part].Fill(_ofc)
							for _ofc in _ref_ofc_a:
								h_OFC_ref[_sub_part].Fill(_ofc)

					_ref_entry += 1
					_cur_entry += 1

				# sync the tree entries if needed
				elif _ref_ch_id>_cur_ch_id:
					_cur_entry += 1
				elif _ref_ch_id<_cur_ch_id:
					_ref_entry += 1

			if self.verbose["med"]:
				_percent = 100*_cur_entry//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
					(self.id_name, _percent, _valid_type, _part)

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				h_OFC_cur[_sub_part].Write()
				h_OFC_ref[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# =========================================================
	# d) ALGORITHM PART mode FEBs cycles
	# =========================================================

	# ----- 1. ValidationAutoFEBs() -----
	def ValidationAutoFEBs(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'AUTOCORR'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		_cnt_shift_channels = {'id':0, '1s':0, '3s':0}
		_file_shift_febs    = {'name':None, 'file':None}
		_file_shift_febs['name'] = self.MakeShiftFebsFileName(_run_part, _valid_type, _run_gain, _run_camp)
		_file_shift_febs['file'] = open(_file_shift_febs['name'], 'w')

		# definition of the histograms
		ntuple          = dict()
		his_autocorr    = dict()
		his_autocorr_ft = dict()
		his_autocorr_cl = dict()
		his_occupancy   = dict()
		his_feb_mean    = dict()
		# - feb swapped part
		swap_ntuple          = dict()
		swap_his_autocorr    = dict()
		swap_his_autocorr_ft = dict()
		swap_his_autocorr_cl = dict()
		swap_his_occupancy   = dict()
		swap_his_feb_mean    = dict()

		h_temp = TH1F("h_temp", "h_temp", 100, 0, 0)
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][4]

				ntuple[_sub_part] = TNtuple("Ntuple_" + _sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:Autocorr:DeltaAutocorr:threshold")
				his_autocorr[_sub_part] = TH1F("h_Delta_AUTOCORR_" + _sub_part, \
					"AutoCorr relative differ ", 500, -2*_threshold, +2*_threshold)
				his_autocorr_ft[_sub_part] = TH2F("h_Delta_AUTOCORRVSft_" + _sub_part, \
					"AutoCorr relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_autocorr_cl[_sub_part] = TH2F("h_Delta_AUTOCORRVScl_" + _sub_part, \
					"AutoCorr relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" + _sub_part, \
					"h_Occupancy", 33, -16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_AUTOCORR_" +_sub_part, "Mean Auto per FEB", 50, 0, 0)
				# - feb swapped part
				swap_ntuple[_sub_part] = TNtuple("Ntuple_SWAP_" + _sub_part, "Ntuple (swap)", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:Autocorr:DeltaAutocorr:threshold")
				swap_his_autocorr[_sub_part] = TH1F("h_SWAP_Delta_AUTOCORR_" + _sub_part, \
					"AutoCorr relative differ (swap)", 500, -2*_threshold, +2*_threshold)
				swap_his_autocorr_ft[_sub_part] = TH2F("h_SWAP_Delta_AUTOCORRVSft_" + _sub_part, \
					"AutoCorr relative differ vs FT-SL (swap)", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				swap_his_autocorr_cl[_sub_part] = TH2F("h_SWAP_Delta_AUTOCORRVScl_" + _sub_part, \
					"AutoCorr relative differ vs CL (swap)", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				swap_his_occupancy[_sub_part] = TH2I("h_SWAP_Occupancy" + _sub_part, \
					"h_Occupancy (swap)", 33, -16.5, 16.5, 32, -0.5, 31.5)
				swap_his_feb_mean[_sub_part] = TH1F("h_SWAP_feb_AUTOCORR_" +_sub_part, \
					"Mean Auto per FEB (swap)", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_AUTOCORR, refRunDict, curRunDict)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue

			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_next_ft   = -999
			_next_slot = -999

			_cur_total_entries = _cur_tree.GetEntries()
			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, 'ALL')
			_print_percent = int(0.1*_cur_total_entries + 1)

			for _entry_id in range(0, _cur_total_entries):
				if self.verbose["med"] and _entry_id%_print_percent==0:
					_percent = 100*_entry_id//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, 'ALL')

				# compared data
				_cur_tree.GetEntry(_entry_id)
				_cur_ch_id     = _cur_tree.channelId
				_cur_autoCorr  = _cur_tree.covr[0]
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan

				# reference data
				_old_ch_id = self.GetSwappedChannel(_cur_ch_id)
				_sel_query = "channelId==%d" % _old_ch_id
				_res_query = _ref_tree.Query("covr[0]:calibLine:badChan", _sel_query)

				if _res_query.GetRowCount()==0:
					print MED_INDENT,"%s:\t WARNING ref_tree doesn`t have event with ch_id %d (%s, %s)" % (self.id_name, _old_ch_id, _valid_type, 'ALL')
					continue
				if _res_query.GetRowCount()>1:
					print MED_INDENT,"%s:\t WARNING ref_tree have few events for ch_id %d (%s, %s)" % (self.id_name, _old_ch_id, _valid_type, 'ALL')
					continue

				_row_query     = _res_query.Next()
				_ref_autoCorr  = float(_row_query.GetField(0))
				_ref_calibLine = int(_row_query.GetField(1))
				_ref_badChan   = int(_row_query.GetField(2))

				# compute the variable
				_delta_autoCorr = _ref_autoCorr - _cur_autoCorr

				# loop over the different partition
				for _part_all in LIST_OF_PART[_run_part]: # because PEDESTAL runs have all partitions
					for _sub_part in LIST_OF_SUB_PART[_part_all]:
						if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):
							# threshold from DB
							_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][4]

							# - fill the corresponding histograms
							if self.is_swapped:
								swap_ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, \
									_cur_pos_neg, _cur_ft, _cur_slot, _cur_channel,_cur_calibLine, \
									_cur_badChan, _cur_autoCorr, _delta_autoCorr, _db_treshold)
							else:
								ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, \
									_cur_pos_neg, _cur_ft, _cur_slot, _cur_channel,_cur_calibLine, \
									_cur_badChan, _cur_autoCorr, _delta_autoCorr, _db_treshold)

							# look at the FEB
							if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
								# fill occupancy
								_coeff = 1 if _cur_pos_neg==1 else -1
								if self.is_swapped:
									swap_his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)
								else:
									his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

								# get the mean for the FEB
								if h_temp.GetEntries()!=0:
									FebAverage = h_temp.GetMean()
									if self.is_swapped:
										swap_his_feb_mean[_sub_part].Fill(FebAverage)
									else:
										his_feb_mean[_sub_part].Fill(FebAverage)
									# flag deviating FEB
									if abs(FebAverage)>ECalDBLib.thresholdDict[_sub_part][_run_gain][5]:
										febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
										if febName in self.deviatingFebList:
											self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
										else:
											self.deviatingFebDict[febName] = []
											self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
											self.deviatingFebList.append(febName)
								_next_ft   = _cur_ft
								_next_slot = _cur_slot
								h_temp.Reset()

							# look at the channels
							if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
								if not('deadCalib' in BadChanInterpreter(_cur_badChan)):
									continue
							if _ref_calibLine<0 or _cur_calibLine<0:
								continue
							h_temp.Fill(_delta_autoCorr)
							if self.is_swapped:
								swap_his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								swap_his_autocorr[_sub_part].Fill(_delta_autoCorr)
								swap_his_autocorr_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_autoCorr)
								swap_his_autocorr_cl[_sub_part].Fill(_cur_calibLine, _delta_autoCorr)
							else:
								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_autocorr[_sub_part].Fill(_delta_autoCorr)
								his_autocorr_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_autoCorr)
								his_autocorr_cl[_sub_part].Fill(_cur_calibLine, _delta_autoCorr)

							# flag deviating channels
							if abs(_delta_autoCorr)>_db_treshold:
								channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + str(_cur_channel) + '_' + _run_camp
								if channelName in self.deviatingChannelList:
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_autoCorr, _cur_calibLine])
								else:
									self.deviatingChannelDict[channelName] = []
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_autoCorr, _cur_calibLine])
									self.deviatingChannelList.append(channelName)

								# swapped febs
								if self.is_swapped:
									_delta = _delta_autoCorr/_db_treshold
									_abs_delta = abs(_delta)
									_delta_sigma_label = ''

									if _abs_delta>=1 and _abs_delta<3:
										_delta_sigma_label = '1sig'
									elif _abs_delta>=3:
										_delta_sigma_label = '3sig'

									_ch_info  = "part %s: FT %2d, slot %2d, channel %3d\n" % (_sub_part, _cur_ft, _cur_slot, _cur_channel)
									_dev_info = "\t -> badChan %3d, type %s, gain %s, calibLine %3d, delta %9.4f (%s)\n\n" \
										% (_cur_badChan, _valid_type, _run_gain, _cur_calibLine, _delta, _delta_sigma_label)

									_cnt_shift_channels['id'] += 1
									_ch_info = "%4d. %s" % (_cnt_shift_channels['id'], _ch_info)
									_file_shift_febs['file'].write(_ch_info)
									_file_shift_febs['file'].write(_dev_info)

									if _delta_sigma_label=='1sig':
										_cnt_shift_channels['1s'] += 1
									elif _delta_sigma_label=='3sig':
										_cnt_shift_channels['3s'] += 1

			if self.verbose["med"]:
				_percent = 100*(_entry_id+1)//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % (self.id_name, _percent, _valid_type, 'ALL')

		# save otput for swapped febs
		if _file_shift_febs['file']!=None:
			_sum_move  = "Total 1sig: %4d\n" % _cnt_shift_channels['1s']
			_sum_move += "Total 3sig: %4d\n" % _cnt_shift_channels['3s']
			_file_shift_febs['file'].write(_sum_move)
			_file_shift_febs['file'].close()
			if _cnt_shift_channels['1s']==0 and _cnt_shift_channels['3s']==0:
				if os.path.isfile(_file_shift_febs['name']):
					os.remove(_file_shift_febs['name'])

		# save otput to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_autocorr[_sub_part].Write()
				his_autocorr_ft[_sub_part].Write()
				his_autocorr_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()
				# - swap part
				swap_his_autocorr[_sub_part].Write()
				swap_his_autocorr_ft[_sub_part].Write()
				swap_his_autocorr_cl[_sub_part].Write()
				swap_his_occupancy[_sub_part].Write()
				swap_his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()
				swap_ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 2. ValidationPedFEBs() mode FEBs -----
	def ValidationPedFEBs(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'PEDESTAL'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		_cnt_shift_channels = {'id':0, '1s':0, '3s':0}
		_file_shift_febs    = {'name':None, 'file':None}
		_file_shift_febs['name'] = self.MakeShiftFebsFileName(_run_part, _valid_type, _run_gain, _run_camp)
		_file_shift_febs['file'] = open(_file_shift_febs['name'], 'w')

		# definition of the histograms
		ntuple          = dict()
		his_pedestal    = dict()
		his_pedestal_ft = dict()
		his_pedestal_cl = dict()
		his_occupancy   = dict()
		his_feb_mean    = dict()
		# - swap part
		swap_ntuple          = dict()
		swap_his_pedestal    = dict()
		swap_his_pedestal_ft = dict()
		swap_his_pedestal_cl = dict()
		swap_his_occupancy   = dict()
		swap_his_feb_mean    = dict()

		h_temp          = TH1F("h_temp", "h_temp", 100, 0, 0);
		h_temp_absolute = TH1F("h_temp_abs", "h_temp_abs", 10000, -1000, -1000);

		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][0]

				ntuple[_sub_part] = TNtuple("Ntuple_"+_sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:ped:DeltaPed:threshold")
				his_pedestal[_sub_part] = TH1F("h_Delta_PEDESTAL_" + _sub_part, \
					"PEDESTAL relative differ ", 500, -2*_threshold, +2*_threshold)
				his_pedestal_ft[_sub_part] = TH2F("h_Delta_PEDESTALVSft_" + _sub_part, \
					"PEDESTAL relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_pedestal_cl[_sub_part] = TH2F("h_Delta_PEDESTALVScl_" + _sub_part, \
					"PEDESTAL relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" + _sub_part, \
					"h_Occupancy", 33, -16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_PEDESTAL_" + _sub_part, \
					"Mean Ped per FEB", 50, 0, 0)
				# - swap part
				swap_ntuple[_sub_part] = TNtuple("Ntuple_SWAP_"+_sub_part, "Ntuple (swap)", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:ped:DeltaPed:threshold")
				swap_his_pedestal[_sub_part] = TH1F("h_SWAP_Delta_PEDESTAL_" + _sub_part, \
					"PEDESTAL relative differ (swap)", 500, -2*_threshold, +2*_threshold)
				swap_his_pedestal_ft[_sub_part] = TH2F("h_SWAP_Delta_PEDESTALVSft_" + _sub_part, \
					"PEDESTAL relative differ vs FT-SL (swap)", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				swap_his_pedestal_cl[_sub_part] = TH2F("h_SWAP_Delta_PEDESTALVScl_" + _sub_part, \
					"PEDESTAL relative differ vs CL (swap)", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				swap_his_occupancy[_sub_part] = TH2I("h_SWAP_Occupancy" + _sub_part, \
					"h_Occupancy (swap)", 33, -16.5, 16.5, 32, -0.5, 31.5)
				swap_his_feb_mean[_sub_part] = TH1F("h_SWAP_feb_PEDESTAL_" + _sub_part, \
					"Mean Ped per FEB (swap)", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_PEDESTAL, refRunDict, curRunDict)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue

			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_next_ft   = -999
			_next_slot = -999

			_cur_total_entries = _cur_tree.GetEntries()
			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, 'ALL')
			_print_percent = int(0.1*_cur_total_entries + 1)

			for _entry_id in range(0, _cur_total_entries):
				if self.verbose["med"] and _entry_id%_print_percent==0:
					_percent = 100*_entry_id//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, 'ALL')

				# compared data <-> current
				_cur_tree.GetEntry(_entry_id)
				_cur_ch_id     = _cur_tree.channelId
				_cur_ped       = _cur_tree.ped
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan

				# reference data
				_old_ch_id = self.GetSwappedChannel(_cur_ch_id)
				_sel_query = "channelId==%d" % _old_ch_id
				_res_query = _ref_tree.Query("ped:calibLine:badChan", _sel_query)

				if _res_query.GetRowCount()==0:
					print MED_INDENT,"%s:\t WARNING ref_tree doesn`t have event with ch_id %d (%s, %s)" % \
						(self.id_name, _old_ch_id, _valid_type, 'ALL')
					continue
				if _res_query.GetRowCount()>1:
					print MED_INDENT,"%s:\t WARNING ref_tree have few events for ch_id %d (%s, %s)" % \
						(self.id_name, _old_ch_id, _valid_type, 'ALL')
					continue

				_row_query     = _res_query.Next()
				_ref_ped       = float(_row_query.GetField(0))
				_ref_calibLine = int(_row_query.GetField(1))
				_ref_badChan   = int(_row_query.GetField(2))

				# compute the variable
				_delta_ped = _ref_ped - _cur_ped;

				# loop over the different partition
				for _part_all in LIST_OF_PART[_run_part]: # because PEDESTAL runs have all partitions
					for _sub_part in LIST_OF_SUB_PART[_part_all]:
						if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):
							# threshold from DB
							_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][0]

							# fill the corresponding histograms
							if self.is_swapped:
								swap_ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
									_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_ped, \
									_delta_ped, _db_treshold);
							else:
								ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
									_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_ped, \
									_delta_ped, _db_treshold);

							# look at the FEB
							if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
								# fill occupancy
								_coeff = 1 if _cur_pos_neg==1 else -1
								if self.is_swapped:
									swap_his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)
								else:
									his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

								# get the mean deviation for the FEB
								FebAverage=0
								if h_temp.GetEntries()!=0:
									FebAverage = h_temp.GetMean();
								if abs(FebAverage)<ECalDBLib.thresholdDict[_sub_part][_run_gain][2]:
									FebAverage=0 # avr = 0 if no deviation
								if self.is_swapped:
									swap_his_feb_mean[_sub_part].Fill(FebAverage);
								else:
									his_feb_mean[_sub_part].Fill(FebAverage);

								# get the mean absolute ped for the FEB
								FebPed = 0
								if h_temp_absolute.GetEntries()!=0:
									FebPed = h_temp_absolute.GetMean();

								# store FEB info
								febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
								if febName in self.deviatingFebList:
									self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage, FebPed])
								else:
									self.deviatingFebDict[febName] = []
									self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage, FebPed])
									self.deviatingFebList.append(febName)

								_next_ft   = _cur_ft
								_next_slot = _cur_slot
								h_temp.Reset()
								h_temp_absolute.Reset()

							# look at the channels
							if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
								if not('deadCalib' in BadChanInterpreter(_cur_badChan)):
									continue

							if _ref_calibLine<0 or _cur_calibLine<0:
								continue

							if self.is_swapped:
								swap_his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								swap_his_pedestal[_sub_part].Fill(_delta_ped)
								swap_his_pedestal_ft[_sub_part].Fill(_cur_ft * 13 + _cur_slot - 2, _delta_ped)
								swap_his_pedestal_cl[_sub_part].Fill(_cur_calibLine, _delta_ped)
							else:
								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_pedestal[_sub_part].Fill(_delta_ped)
								his_pedestal_ft[_sub_part].Fill(_cur_ft * 13 + _cur_slot - 2, _delta_ped)
								his_pedestal_cl[_sub_part].Fill(_cur_calibLine, _delta_ped)

							h_temp.Fill(_delta_ped)
							h_temp_absolute.Fill(_cur_tree.ped)

							# flag deviating channels
							if abs(_delta_ped)>_db_treshold:
								channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + str(_cur_channel) + '_' + _run_camp
								if channelName in self.deviatingChannelList:
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_ped, _cur_calibLine])
								else:
									self.deviatingChannelDict[channelName] = []
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_ped, _cur_calibLine])
									self.deviatingChannelList.append(channelName)

								# swapped febs
								if self.is_swapped:
									_delta = _delta_ped/_db_treshold
									_abs_delta = abs(_delta)
									_delta_sigma_label = ''

									if _abs_delta>=1 and _abs_delta<3:
										_delta_sigma_label = '1sig'
									elif _abs_delta>=3:
										_delta_sigma_label = '3sig'

									_ch_info  = "part %s: FT %2d, slot %2d, channel %3d\n" % (_sub_part, _cur_ft, _cur_slot, _cur_channel)
									_dev_info = "\t -> badChan %3d, type %s, gain %s, calibLine %3d, delta %9.4f (%s)\n\n" \
										% (_cur_badChan, _valid_type, _run_gain, _cur_calibLine, _delta, _delta_sigma_label)

									_cnt_shift_channels['id'] += 1
									_ch_info = "%4d. %s" % (_cnt_shift_channels['id'], _ch_info)
									_file_shift_febs['file'].write(_ch_info)
									_file_shift_febs['file'].write(_dev_info)

									if _delta_sigma_label=='1sig':
										_cnt_shift_channels['1s'] += 1
									elif _delta_sigma_label=='3sig':
										_cnt_shift_channels['3s'] += 1

			if self.verbose["med"]:
				_percent = 100*(_entry_id+1)//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % (self.id_name, _percent, _valid_type, 'ALL')

		# save otput for swapped febs
		if _file_shift_febs['file']!=None:
			_sum_move  = "Total 1sig: %4d\n" % _cnt_shift_channels['1s']
			_sum_move += "Total 3sig: %4d\n" % _cnt_shift_channels['3s']
			_file_shift_febs['file'].write(_sum_move)
			_file_shift_febs['file'].close()
			if _cnt_shift_channels['1s']==0 and _cnt_shift_channels['3s']==0:
				if os.path.isfile(_file_shift_febs['name']):
					os.remove(_file_shift_febs['name'])

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_pedestal[_sub_part].Write()
				his_pedestal_ft[_sub_part].Write()
				his_pedestal_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()
				# - swap part
				swap_his_pedestal[_sub_part].Write()
				swap_his_pedestal_ft[_sub_part].Write()
				swap_his_pedestal_cl[_sub_part].Write()
				swap_his_occupancy[_sub_part].Write()
				swap_his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()
				swap_ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 3. ValidationNoiseFEBs() ------
	def ValidationNoiseFEBs(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'NOISE'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		_cnt_shift_channels = {'id':0, '1s':0, '3s':0}
		_file_shift_febs    = {'name':None, 'file':None}
		if self.mode_febs:
			_file_shift_febs['name'] = self.MakeShiftFebsFileName(_run_part, _valid_type, _run_gain, _run_camp)
			_file_shift_febs['file'] = open(_file_shift_febs['name'], 'w')

		# definition of the histograms
		ntuple        = dict()
		his_noise     = dict()
		his_noise_ft  = dict()
		his_noise_cl  = dict()
		his_occupancy = dict()
		his_feb_mean  = dict()
		# - swap part
		swap_ntuple        = dict()
		swap_his_noise     = dict()
		swap_his_noise_ft  = dict()
		swap_his_noise_cl  = dict()
		swap_his_occupancy = dict()
		swap_his_feb_mean  = dict()

		h_temp = TH1F("h_temp","h_temp", 100, 0, 0)
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][1]

				ntuple[_sub_part] = TNtuple("Ntuple_" + _sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:NOISE:DeltaNOISE:threshold")
				his_noise[_sub_part] = TH1F("h_Delta_NOISE_" + _sub_part, \
					"NOISE relative differ ", 500, -2*_threshold, +2*_threshold)
				his_noise_ft[_sub_part] = TH2F("h_Delta_NOISEVSft_" +_sub_part, \
					"NOISE relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_noise_cl[_sub_part] = TH2F("h_Delta_NOISEVScl_" +_sub_part, \
					"NOISE relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" + _sub_part, \
					"h_Occupancy", 33, -16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_NOISE_" + _sub_part, \
					"Mean NOISE per FEB", 50, 0, 0)
				# - swap part
				swap_ntuple[_sub_part] = TNtuple("Ntuple_SWAP_" + _sub_part, "Ntuple (swap)", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:NOISE:DeltaNOISE:threshold")
				swap_his_noise[_sub_part] = TH1F("h_SWAP_Delta_NOISE_" + _sub_part, \
					"NOISE relative differ (swap)", 500, -2*_threshold, +2*_threshold)
				swap_his_noise_ft[_sub_part] = TH2F("h_SWAP_Delta_NOISEVSft_" +_sub_part, \
					"NOISE relative differ vs FT-SL (swap)", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				swap_his_noise_cl[_sub_part] = TH2F("h_SWAP_Delta_NOISEVScl_" +_sub_part, \
					"NOISE relative differ vs CL (swap)", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				swap_his_occupancy[_sub_part] = TH2I("h_SWAP_Occupancy" + _sub_part, \
					"h_Occupancy (swap)", 33, -16.5, 16.5, 32, -0.5, 31.5)
				swap_his_feb_mean[_sub_part] = TH1F("h_SWAP_feb_NOISE_" + _sub_part, \
					"Mean NOISE per FEB (swap)", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_NOISE, refRunDict, curRunDict)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue

			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_next_ft   = -999
			_next_slot = -999

			_cur_total_entries = _cur_tree.GetEntries()
			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, 'ALL')
			_print_percent = int(0.1*_cur_total_entries + 1)

			for _entry_id in range(0, _cur_total_entries):
				if self.verbose["med"] and _entry_id%_print_percent==0:
					_percent = 100*_entry_id//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, 'ALL')

				# compared data
				_cur_tree.GetEntry(_entry_id)
				_cur_ch_id     = _cur_tree.channelId
				_cur_rms       = _cur_tree.rms
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan

				# reference data
				_old_ch_id = self.GetSwappedChannel(_cur_ch_id)
				_sel_query = "channelId==%d" % _old_ch_id
				_res_query = _ref_tree.Query("rms:calibLine:badChan", _sel_query)

				if _res_query.GetRowCount()==0:
					print MED_INDENT,"%s:\t WARNING ref_tree doesn`t have event with ch_id %d (%s, %s)" % \
						(self.id_name, _old_ch_id, _valid_type, 'ALL')
					continue
				if _res_query.GetRowCount()>1:
					print MED_INDENT,"%s:\t WARNING ref_tree have few events for ch_id %d (%s, %s)" % \
						(self.id_name, _old_ch_id, _valid_type, 'ALL')
					continue

				_row_query     = _res_query.Next()
				_ref_rms       = float(_row_query.GetField(0))
				_ref_calibLine = int(_row_query.GetField(1))
				_ref_badChan   = int(_row_query.GetField(2))

				# compute the variable
				_delta_rms = _ref_rms - _cur_rms;

				# loop over the different partition
				for _part_all in LIST_OF_PART[_run_part]: # because PEDESTAL runs have all partitions
					for _sub_part in LIST_OF_SUB_PART[_part_all]:
						if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):
							# threshold from DB
							_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][1]

							# fill the corresponding histograms
							if self.is_swapped:
								swap_ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
									_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_rms, \
									_delta_rms, _db_treshold)
							else:
								ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
									_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_rms, \
									_delta_rms, _db_treshold)

							# look at the FEB
							if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
								# fill occupancy
								_coeff = 1 if _cur_pos_neg==1 else -1
								if self.is_swapped:
									swap_his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)
								else:
									his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

								# get the mean for the FEB
								if h_temp.GetEntries()!=0:
									FebAverage = h_temp.GetMean()

									if self.is_swapped:
										swap_his_feb_mean[_sub_part].Fill(FebAverage)
									else:
										his_feb_mean[_sub_part].Fill(FebAverage)

									# flag deviating FEB
									if abs(FebAverage)>ECalDBLib.thresholdDict[_sub_part][_run_gain][3]:
										febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
										if febName in self.deviatingFebList:
											self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
										else:
											self.deviatingFebDict[febName] = []
											self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
											self.deviatingFebList.append(febName)

								_next_ft   = _cur_ft
								_next_slot = _cur_slot
								h_temp.Reset()

							# look at the channels
							if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
								if not('deadCalib' in BadChanInterpreter(_cur_badChan)):
									continue
							if _ref_calibLine<0 or _cur_calibLine<0: continue

							h_temp.Fill(_delta_rms)
							if self.is_swapped:
								swap_his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								swap_his_noise[_sub_part].Fill(_delta_rms)
								swap_his_noise_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_rms)
								swap_his_noise_cl[_sub_part].Fill(_cur_calibLine, _delta_rms)
							else:
								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_noise[_sub_part].Fill(_delta_rms)
								his_noise_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_rms)
								his_noise_cl[_sub_part].Fill(_cur_calibLine, _delta_rms)

							# flag deviating channels
							if abs(_delta_rms)>_db_treshold:
								channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + str(_cur_channel) + '_' + _run_camp
								if channelName in self.deviatingChannelList:
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_rms, _cur_calibLine])
								else:
									self.deviatingChannelDict[channelName] = []
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_rms, _cur_calibLine])
									self.deviatingChannelList.append(channelName)

								# swapped febs
								if self.is_swapped:
									_delta = _delta_rms/_db_treshold
									_abs_delta = abs(_delta)
									_delta_sigma_label = ''

									if _abs_delta>=1 and _abs_delta<3:
										_delta_sigma_label = '1sig'
									elif _abs_delta>=3:
										_delta_sigma_label = '3sig'

									_ch_info  = "part %s: FT %2d, slot %2d, channel %3d\n" % (_sub_part, _cur_ft, _cur_slot, _cur_channel)
									_dev_info = "\t -> badChan %3d, type %s, gain %s, calibLine %3d, delta %9.4f (%s)\n\n" \
										% (_cur_badChan, _valid_type, _run_gain, _cur_calibLine, _delta, _delta_sigma_label)

									_cnt_shift_channels['id'] += 1
									_ch_info = "%4d. %s" % (_cnt_shift_channels['id'], _ch_info)
									_file_shift_febs['file'].write(_ch_info)
									_file_shift_febs['file'].write(_dev_info)

									if _delta_sigma_label=='1sig':
										_cnt_shift_channels['1s'] += 1
									elif _delta_sigma_label=='3sig':
										_cnt_shift_channels['3s'] += 1

			if self.verbose["med"]:
				_percent = 100*(_entry_id+1)//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % (self.id_name, _percent, _valid_type, 'ALL')

		# save otput for swapped febs
		if _file_shift_febs['file']!=None:
			_sum_move  = "Total 1sig: %4d\n" % _cnt_shift_channels['1s']
			_sum_move += "Total 3sig: %4d\n" % _cnt_shift_channels['3s']
			_file_shift_febs['file'].write(_sum_move)
			_file_shift_febs['file'].close()
			if _cnt_shift_channels['1s']==0 and _cnt_shift_channels['3s']==0:
				if os.path.isfile(_file_shift_febs['name']):
					os.remove(_file_shift_febs['name'])

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_noise[_sub_part].Write()
				his_noise_ft[_sub_part].Write()
				his_noise_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()
				# - swap part
				swap_his_noise[_sub_part].Write()
				swap_his_noise_ft[_sub_part].Write()
				swap_his_noise_cl[_sub_part].Write()
				swap_his_occupancy[_sub_part].Write()
				swap_his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()
				swap_ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 4. ValidationRampFEBs() -----
	def ValidationRampFEBs(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'RAMP'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		_cnt_shift_channels = {'id':0, '1s':0, '3s':0}
		_file_shift_febs    = {'name':None, 'file':None}
		_file_shift_febs['name'] = self.MakeShiftFebsFileName(_run_part, _valid_type, _run_gain, _run_camp)
		_file_shift_febs['file'] = open(_file_shift_febs['name'], 'w')

		# definition of the histograms
		ntuple        = dict()
		his_ramp      = dict()
		his_ramp_ft   = dict()
		his_ramp_cl   = dict()
		his_occupancy = dict()
		his_feb_mean  = dict()
		# - swap part
		swap_ntuple        = dict()
		swap_his_ramp      = dict()
		swap_his_ramp_ft   = dict()
		swap_his_ramp_cl   = dict()
		swap_his_occupancy = dict()
		swap_his_feb_mean  = dict()

		h_temp = TH1F("h_temp", "h_temp", 100, 0, 0)
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][8]

				ntuple[_sub_part] = TNtuple("Ntuple_" + _sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:corrUndo:RAMP:DeltaRAMP:threshold")
				his_ramp[_sub_part] = TH1F("h_Delta_RAMP_" + _sub_part, \
					"RAMP relative differ ", 500, -2*_threshold, +2*_threshold)
				his_ramp_ft[_sub_part] = TH2F("h_Delta_RAMPVSft_" + _sub_part, \
					"RAMP relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_ramp_cl[_sub_part] = TH2F("h_Delta_RAMPVScl_" + _sub_part, \
					"RAMP relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" +_sub_part, \
					"h_Occupancy", 33 ,-16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_RAMP_" +_sub_part, \
					"Mean RAMP per FEB", 50, 0, 0)
				# - swap part
				swap_ntuple[_sub_part] = TNtuple("Ntuple_SWAP_" + _sub_part, "Ntuple (ramp)", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:corrUndo:RAMP:DeltaRAMP:threshold")
				swap_his_ramp[_sub_part] = TH1F("h_SWAP_Delta_RAMP_" + _sub_part, \
					"RAMP relative differ (ramp)", 500, -2*_threshold, +2*_threshold)
				swap_his_ramp_ft[_sub_part] = TH2F("h_SWAP_Delta_RAMPVSft_" + _sub_part, \
					"RAMP relative differ vs FT-SL (ramp)", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				swap_his_ramp_cl[_sub_part] = TH2F("h_SWAP_Delta_RAMPVScl_" + _sub_part, \
					"RAMP relative differ vs CL (ramp)", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				swap_his_occupancy[_sub_part] = TH2I("h_SWAP_Occupancy" +_sub_part, \
					"h_Occupancy (ramp)", 33 ,-16.5, 16.5, 32, -0.5, 31.5)
				swap_his_feb_mean[_sub_part] = TH1F("h_SWAP_feb_RAMP_" +_sub_part, \
					"Mean RAMP per FEB (ramp)", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_RAMP, refRunDict, curRunDict)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue

			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_next_ft   = -999
			_next_slot = -999

			_cur_total_entries = _cur_tree.GetEntries()
			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, _part)
			_print_percent = int(0.1*_cur_total_entries + 1)

			for _entry_id in range(0, _cur_total_entries):
				if self.verbose["med"] and _entry_id%_print_percent==0:
					_percent = 100*_entry_id//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, _part)

				_cur_tree.GetEntry(_entry_id)

				# check if X[1] exist
				if len(_cur_tree.X)==0:
					continue
				elif _cur_tree.X[1]==0:
					continue

				# compared data
				_cur_ch_id     = _cur_tree.channelId
				_cur_X1        = _cur_tree.X[1]
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan
				_cur_corrUndo  = _cur_tree.corrUndo

				# reference data
				_old_ch_id = self.GetSwappedChannel(_cur_ch_id)
				_sel_query = "channelId==%d" % _old_ch_id
				_res_query = _ref_tree.Query("X[1]:calibLine:badChan:corrUndo", _sel_query)

				if _res_query.GetRowCount()==0:
					print MED_INDENT,"%s:\t WARNING ref_tree doesn`t have event with ch_id %d (%s, %s)" % \
						(self.id_name, _old_ch_id, _valid_type, _part)
					continue

				_ref_X1 = 0
				for _res_id in range(_res_query.GetRowCount()):
					_row_query    = _res_query.Next()
					_tmp_corrUndo = int(_row_query.GetField(3))
					if _tmp_corrUndo==_cur_corrUndo:
						_ref_X1        = float(_row_query.GetField(0))
						_ref_calibLine = int(_row_query.GetField(1))
						_ref_badChan   = int(_row_query.GetField(2))
						_ref_corrUndo  = _tmp_corrUndo

				if _ref_X1==0:
					continue

				# compute the variable
				_delta_ramp = (_ref_X1 - _cur_X1)/_ref_X1

				# loop over the different partition
				for _sub_part in LIST_OF_SUB_PART[_part]:
					if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):
						# threshold from DB
						_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][8]

						# fill the corresponding histograms
						if self.is_swapped:
							swap_ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
								_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_corrUndo, \
								_cur_X1, _delta_ramp, _db_treshold)
						else:
							ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
								_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_corrUndo, \
								_cur_X1, _delta_ramp, _db_treshold)

						# look at the FEB
						if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
							# fill occupancy
							_coeff = 1 if _cur_pos_neg==1 else -1
							if self.is_swapped:
								swap_his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)
							else:
								his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

							# get the mean for the FEB
							if h_temp.GetEntries()!=0:
								FebAverage = h_temp.GetMean()
								if self.is_swapped:
									swap_his_feb_mean[_sub_part].Fill(FebAverage)
								else:
									his_feb_mean[_sub_part].Fill(FebAverage)

								# flag deviating FEB
								if abs(FebAverage)>ECalDBLib.thresholdDict[_sub_part][_run_gain][9]:
									febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
									if febName in self.deviatingFebList:
										self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
									else:
										self.deviatingFebDict[febName] = []
										self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
										self.deviatingFebList.append(febName)

							_next_ft   = _cur_ft
							_next_slot = _cur_slot
							h_temp.Reset()

						# look at the channels
						if not(InList(_ref_badChan, PATCHED_BAD_CHANNEL)) and not(InList(_cur_badChan,PATCHED_BAD_CHANNEL)):
							if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
								continue
						if _ref_calibLine<0 or _cur_calibLine<0:
							continue
						if _ref_corrUndo!=_cur_corrUndo:
							continue

						if (_ref_corrUndo==0 and _cur_corrUndo==0):
							h_temp.Fill(_delta_ramp)

							if self.is_swapped:
								swap_his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								swap_his_ramp[_sub_part].Fill(_delta_ramp)
								swap_his_ramp_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_ramp)
								swap_his_ramp_cl[_sub_part].Fill(_cur_calibLine, _delta_ramp)
							else:
								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_ramp[_sub_part].Fill(_delta_ramp)
								his_ramp_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_ramp)
								his_ramp_cl[_sub_part].Fill(_cur_calibLine, _delta_ramp)

						# flag deviating channels
						if abs(_delta_ramp)>_db_treshold:
							channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + str(_cur_channel) + '_' + _run_camp
							if channelName in self.deviatingChannelList:
								if (_ref_corrUndo==0 and _cur_corrUndo==0):
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_ramp, _cur_calibLine])
								elif (_ref_corrUndo==1 and _cur_corrUndo==1):
									self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_ramp, _cur_calibLine])
							else:
								self.deviatingChannelDict[channelName] = []
								if (_ref_corrUndo==0 and _cur_corrUndo==0):
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_ramp, _cur_calibLine])
								elif (_ref_corrUndo==1 and _cur_corrUndo==1):
									self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_ramp, _cur_calibLine])
								self.deviatingChannelList.append(channelName)

							# swapped febs
							if self.is_swapped:
								_delta = _delta_ramp/_db_treshold
								_abs_delta = abs(_delta)
								_delta_sigma_label = ''

								if _abs_delta>=1 and _abs_delta<3:
									_delta_sigma_label = '1sig'
								elif _abs_delta>=3:
									_delta_sigma_label = '3sig'

								_ch_info  = "part %s: FT %2d, slot %2d, channel %3d\n" % (_sub_part, _cur_ft, _cur_slot, _cur_channel)
								_dev_info = "\t -> badChan %3d, type %s, gain %s, calibLine %3d, delta %9.4f (%s)\n\n" \
									% (_cur_badChan, _valid_type, _run_gain, _cur_calibLine, _delta, _delta_sigma_label)

								_cnt_shift_channels['id'] += 1
								_ch_info = "%4d. %s" % (_cnt_shift_channels['id'], _ch_info)
								_file_shift_febs['file'].write(_ch_info)
								_file_shift_febs['file'].write(_dev_info)

								if _delta_sigma_label=='1sig':
									_cnt_shift_channels['1s'] += 1
								elif _delta_sigma_label=='3sig':
									_cnt_shift_channels['3s'] += 1

			if self.verbose["med"]:
				_percent = 100*(_entry_id+1)//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % (self.id_name, _percent, _valid_type, _part)

		# save otput for swapped febs
		if _file_shift_febs['file']!=None:
			_sum_move  = "Total 1sig: %4d\n" % _cnt_shift_channels['1s']
			_sum_move += "Total 3sig: %4d\n" % _cnt_shift_channels['3s']
			_file_shift_febs['file'].write(_sum_move)
			_file_shift_febs['file'].close()
			if _cnt_shift_channels['1s']==0 and _cnt_shift_channels['3s']==0:
				if os.path.isfile(_file_shift_febs['name']):
					os.remove(_file_shift_febs['name'])

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_ramp[_sub_part].Write()
				his_ramp_ft[_sub_part].Write()
				his_ramp_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()
				# - swap part
				swap_his_ramp[_sub_part].Write()
				swap_his_ramp_ft[_sub_part].Write()
				swap_his_ramp_cl[_sub_part].Write()
				swap_his_occupancy[_sub_part].Write()
				swap_his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()
				swap_ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 5. ValidationPeakTimeFEBs() -----
	def ValidationPeakTimeFEBs(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'PEAKTIME'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		_cnt_shift_channels = {'id':0, '1s':0, '3s':0}
		_file_shift_febs    = {'name':None, 'file':None}
		_file_shift_febs['name'] = self.MakeShiftFebsFileName(_run_part, _valid_type, _run_gain, _run_camp)
		_file_shift_febs['file'] = open(_file_shift_febs['name'], 'w')

		# definition of the histograms
		ntuple          = dict()
		his_peaktime    = dict()
		his_peaktime_ft = dict()
		his_peaktime_cl = dict()
		his_occupancy   = dict()
		his_feb_mean    = dict()
		# - swap part
		swap_ntuple          = dict()
		swap_his_peaktime    = dict()
		swap_his_peaktime_ft = dict()
		swap_his_peaktime_cl = dict()
		swap_his_occupancy   = dict()
		swap_his_feb_mean    = dict()

		h_temp = TH1F("h_temp", "h_temp", 100, 0, 0)
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][11]

				ntuple[_sub_part] = TNtuple("Ntuple_" + _sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:corrUndo:PEAKTIME:DeltaPEAKTIME:threshold")
				his_peaktime[_sub_part] = TH1F("h_Delta_PEAKTIME_" + _sub_part, \
					"PEAKTIME relative differ ", 500, -2*_threshold, +2*_threshold)
				his_peaktime_ft[_sub_part] = TH2F("h_Delta_PEAKTIMEVSft_" + _sub_part, \
					"PEAKTIME relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_peaktime_cl[_sub_part] = TH2F("h_Delta_PEAKTIMEVScl_" + _sub_part, \
					"PEAKTIME relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" + _sub_part, \
					"h_Occupancy", 33, -16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_PEAKTIME_" + _sub_part, \
					"Mean PEAKTIME per FEB", 50, 0, 0)
				# - swap part
				swap_ntuple[_sub_part] = TNtuple("Ntuple_SWAP_" + _sub_part, "Ntuple (swap)", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:corrUndo:PEAKTIME:DeltaPEAKTIME:threshold")
				swap_his_peaktime[_sub_part] = TH1F("h_SWAP_Delta_PEAKTIME_" + _sub_part, \
					"PEAKTIME relative differ (swap)", 500, -2*_threshold, +2*_threshold)
				swap_his_peaktime_ft[_sub_part] = TH2F("h_SWAP_Delta_PEAKTIMEVSft_" + _sub_part, \
					"PEAKTIME relative differ vs FT-SL (swap)", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				swap_his_peaktime_cl[_sub_part] = TH2F("h_SWAP_Delta_PEAKTIMEVScl_" + _sub_part, \
					"PEAKTIME relative differ vs CL (swap)", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				swap_his_occupancy[_sub_part] = TH2I("h_SWAP_Occupancy" + _sub_part, \
					"h_Occupancy (swap)", 33, -16.5, 16.5, 32, -0.5, 31.5)
				swap_his_feb_mean[_sub_part] = TH1F("h_SWAP_feb_PEAKTIME_" + _sub_part, \
					"Mean PEAKTIME per FEB (swap)", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_PEAKTIME, refRunDict, curRunDict, _valid_type)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue

			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_next_ft   = -999
			_next_slot = -999

			_cur_total_entries = _cur_tree.GetEntries()
			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, _part)
			_print_percent = int(0.1*_cur_total_entries + 1)

			for _entry_id in range(0, _cur_total_entries):
				if self.verbose["med"] and _entry_id%_print_percent==0:
					_percent = 100*_entry_id//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, _part)

				_cur_tree.GetEntry(_entry_id)

				# check if cur_TmaxAmp is not 0
				if _cur_tree.TmaxAmp==0:
					continue

				# compared data
				_cur_ch_id     = _cur_tree.channelId
				_cur_TmaxAmp   = _cur_tree.TmaxAmp
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan
				_cur_corrUndo  = _cur_tree.corrUndo

				# reference data
				_old_ch_id = self.GetSwappedChannel(_cur_ch_id)
				_sel_query = "channelId==%d" % _old_ch_id
				_res_query = _ref_tree.Query("TmaxAmp:calibLine:badChan:corrUndo", _sel_query)

				if _res_query.GetRowCount()==0:
					print MED_INDENT,"%s:\t WARNING ref_tree doesn`t have event with ch_id %d (%s, %s)" % \
						(self.id_name, _old_ch_id, _valid_type, _part)
					continue

				_ref_TmaxAmp = 0
				for _res_id in range(_res_query.GetRowCount()):
					_row_query    = _res_query.Next()
					_tmp_corrUndo = int(_row_query.GetField(3))
					if _tmp_corrUndo==_cur_corrUndo:
						_ref_TmaxAmp   = float(_row_query.GetField(0))
						_ref_calibLine = int(_row_query.GetField(1))
						_ref_badChan   = int(_row_query.GetField(2))
						_ref_corrUndo  = _tmp_corrUndo

				# check if ref_TmaxAmp is not 0
				if _ref_TmaxAmp==0:
					continue

				# compute the variable
				_delta_peakTime = _ref_TmaxAmp - _cur_TmaxAmp

				# loop over the different partition
				for _sub_part in LIST_OF_SUB_PART[_part]:
					if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft,_cur_slot):
						# threshold from DB
						_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][11]

						# fill the corresponding histograms
						if self.is_swapped:
							swap_ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
								_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_corrUndo, \
								_cur_TmaxAmp, _delta_peakTime, _db_treshold)
						else:
							ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
								_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_corrUndo, \
								_cur_TmaxAmp, _delta_peakTime, _db_treshold)

						# look at the FEB
						if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
							# fill occupancy
							_coeff = 1 if _cur_pos_neg==1 else -1
							if self.is_swapped:
								swap_his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)
							else:
								his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

							# get the mean for the FEB
							if h_temp.GetEntries()!=0:
								FebAverage = h_temp.GetMean();
								his_feb_mean[_sub_part].Fill(FebAverage);

								# flag deviating FEB
								if abs(FebAverage)>_db_treshold:
									febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
									if febName in self.deviatingFebList:
										self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
									else:
										self.deviatingFebDict[febName] = []
										self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
										self.deviatingFebList.append(febName)

							_next_ft   = _cur_ft
							_next_slot = _cur_slot
							h_temp.Reset()

						# look at the channels
						if not(InList(_ref_badChan, PATCHED_BAD_CHANNEL)) and not(InList(_cur_badChan, PATCHED_BAD_CHANNEL)):
							if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
								continue
						if _ref_calibLine<0 or _cur_calibLine<0: continue
						if _ref_corrUndo!=_cur_corrUndo: continue

						if (_ref_corrUndo==0 and _cur_corrUndo==0):
							h_temp.Fill(_delta_peakTime)
							if self.is_swapped:
								swap_his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								swap_his_peaktime[_sub_part].Fill(_delta_peakTime)
								swap_his_peaktime_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_peakTime)
								swap_his_peaktime_cl[_sub_part].Fill(_cur_calibLine, _delta_peakTime)
							else:
								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_peaktime[_sub_part].Fill(_delta_peakTime)
								his_peaktime_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_peakTime)
								his_peaktime_cl[_sub_part].Fill(_cur_calibLine, _delta_peakTime)

						# flag deviating channels
						if abs(_delta_peakTime)>_db_treshold:
							channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_'+str(_cur_channel) + '_' + _run_camp
							if channelName in self.deviatingChannelList:
								if (_ref_corrUndo==0 and _cur_corrUndo==0):
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_peakTime, _cur_calibLine])
								elif (_ref_corrUndo==1 and _cur_corrUndo==1):
									self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_peakTime, _cur_calibLine])
							else:
								self.deviatingChannelDict[channelName] = []
								if (_ref_corrUndo==0 and _cur_corrUndo==0):
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_peakTime, _cur_calibLine])
								elif (_ref_corrUndo==1 and _cur_corrUndo==1):
									self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_peakTime, _cur_calibLine])
								self.deviatingChannelList.append(channelName)

							# swapped febs
							if self.is_swapped:
								_delta = _delta_peakTime/_db_treshold
								_abs_delta = abs(_delta)
								_delta_sigma_label = ''

								if _abs_delta>=1 and _abs_delta<3:
									_delta_sigma_label = '1sig'
								elif _abs_delta>=3:
									_delta_sigma_label = '3sig'

								_ch_info  = "part %s: FT %2d, slot %2d, channel %3d\n" % (_sub_part, _cur_ft, _cur_slot, _cur_channel)
								_dev_info = "\t -> badChan %3d, type %s, gain %s, calibLine %3d, delta %9.4f (%s)\n\n" \
									% (_cur_badChan, _valid_type, _run_gain, _cur_calibLine, _delta, _delta_sigma_label)

								_cnt_shift_channels['id'] += 1
								_ch_info = "%4d. %s" % (_cnt_shift_channels['id'], _ch_info)
								_file_shift_febs['file'].write(_ch_info)
								_file_shift_febs['file'].write(_dev_info)

								if _delta_sigma_label=='1sig':
									_cnt_shift_channels['1s'] += 1
								elif _delta_sigma_label=='3sig':
									_cnt_shift_channels['3s'] += 1

			if self.verbose["med"]:
				_percent = 100*(_entry_id+1)//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % (self.id_name, _percent, _valid_type, _part)

		# save otput for swapped febs
		if _file_shift_febs['file']!=None:
			_sum_move  = "Total 1sig: %4d\n" % _cnt_shift_channels['1s']
			_sum_move += "Total 3sig: %4d\n" % _cnt_shift_channels['3s']
			_file_shift_febs['file'].write(_sum_move)
			_file_shift_febs['file'].close()
			if _cnt_shift_channels['1s']==0 and _cnt_shift_channels['3s']==0:
				if os.path.isfile(_file_shift_febs['name']):
					os.remove(_file_shift_febs['name'])

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_peaktime[_sub_part].Write()
				his_peaktime_ft[_sub_part].Write()
				his_peaktime_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()
				# - swap part
				swap_his_peaktime[_sub_part].Write()
				swap_his_peaktime_ft[_sub_part].Write()
				swap_his_peaktime_cl[_sub_part].Write()
				swap_his_occupancy[_sub_part].Write()
				swap_his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()
				swap_ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 6. ValidationDelayFEBs() -----
	def ValidationDelayFEBs(self, refRunDict, curRunDict):
		_run_camp   = str(curRunDict['campaign'])
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'DELAY'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		_cnt_shift_channels = {'id':0, '1s':0, '3s':0}
		_file_shift_febs    = {'name':None, 'file':None}
		_file_shift_febs['name'] = self.MakeShiftFebsFileName(_run_part, _valid_type, _run_gain, _run_camp)
		_file_shift_febs['file'] = open(_file_shift_febs['name'], 'w')

		# definition of the histograms
		ntuple        = dict()
		his_delay     = dict()
		his_delay_ft  = dict()
		his_delay_cl  = dict()
		his_occupancy = dict()
		his_feb_mean  = dict()
		# - swap part
		swap_ntuple        = dict()
		swap_his_delay     = dict()
		swap_his_delay_ft  = dict()
		swap_his_delay_cl  = dict()
		swap_his_occupancy = dict()
		swap_his_feb_mean  = dict()

		h_temp = TH1F("h_temp", "h_temp", 100, 0, 0)
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				_threshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][6]

				ntuple[_sub_part] = TNtuple("Ntuple_" + _sub_part, "Ntuple", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:corrUndo:DELAY:DeltaDELAY:threshold")
				his_delay[_sub_part] = TH1F("h_Delta_DELAY_" + _sub_part, \
					"DELAY relative differ ", 500, -2*_threshold, +2*_threshold)
				his_delay_ft[_sub_part] = TH2F("h_Delta_DELAYVSft_" + _sub_part, \
					"DELAY relative differ vs FT-SL ", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				his_delay_cl[_sub_part] = TH2F("h_Delta_DELAYVScl_" + _sub_part, \
					"DELAY relative differ vs CL ", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				his_occupancy[_sub_part] = TH2I("h_Occupancy" + _sub_part, \
					"h_Occupancy", 33, -16.5, 16.5, 32, -0.5, 31.5)
				his_feb_mean[_sub_part] = TH1F("h_feb_DELAY_" + _sub_part, \
					"Mean DELAY per FEB", 50, 0, 0)
				# - swap part
				swap_ntuple[_sub_part] = TNtuple("Ntuple_SWAP_" + _sub_part, "Ntuple (swap)", \
					"channelId:layer:barrel_ec:pos_neg:FT:slot:channel:calibLine:badChan:corrUndo:DELAY:DeltaDELAY:threshold")
				swap_his_delay[_sub_part] = TH1F("h_SWAP_Delta_DELAY_" + _sub_part, \
					"DELAY relative differ (swap)", 500, -2*_threshold, +2*_threshold)
				swap_his_delay_ft[_sub_part] = TH2F("h_SWAP_Delta_DELAYVSft_" + _sub_part, \
					"DELAY relative differ vs FT-SL (swap)", 416, 0.0, 416.0, 500, -2*_threshold, +2*_threshold)
				swap_his_delay_cl[_sub_part] = TH2F("h_SWAP_Delta_DELAYVScl_" + _sub_part, \
					"DELAY relative differ vs CL (swap)", 128, 0, 127, 500, -2*_threshold, +2*_threshold)
				swap_his_occupancy[_sub_part] = TH2I("h_SWAP_Occupancy" + _sub_part, \
					"h_Occupancy (swap)", 33, -16.5, 16.5, 32, -0.5, 31.5)
				swap_his_feb_mean[_sub_part] = TH1F("h_SWAP_feb_DELAY_" + _sub_part, \
					"Mean DELAY per FEB (swap)", 50, 0, 0)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_DELAY, refRunDict, curRunDict)

		# loop over the entries
		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue
			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_next_ft   = -999
			_next_slot = -999

			_cur_total_entries = _cur_tree.GetEntries()
			if self.verbose["med"]:
				print MED_INDENT,"%s:\t\t current tree has %d events (%s, %s)" % \
					(self.id_name, _cur_total_entries,  _valid_type, _part)
			_print_percent = int(0.1*_cur_total_entries + 1)

			for _entry_id in range(0, _cur_total_entries):
				if self.verbose["med"] and _entry_id%_print_percent==0:
					_percent = 100*_entry_id//_cur_total_entries
					if _percent>0:
						print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % \
							(self.id_name, _percent, _valid_type, _part)

				# check if cur_MaxAmp is not 0
				_cur_tree.GetEntry(_entry_id)
				if _cur_tree.MaxAmp==0:
					continue

				# compared data
				_cur_ch_id     = _cur_tree.channelId
				_cur_MaxAmp    = _cur_tree.MaxAmp
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_layer     = _cur_tree.layer
				_cur_channel   = _cur_tree.channel
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan
				_cur_corrUndo  = _cur_tree.corrUndo

				# reference data
				_old_ch_id = self.GetSwappedChannel(_cur_ch_id)
				_sel_query = "channelId==%d" % _old_ch_id
				_res_query = _ref_tree.Query("MaxAmp:calibLine:badChan:corrUndo", _sel_query)

				if _res_query.GetRowCount()==0:
					print MED_INDENT,"%s:\t WARNING ref_tree doesn`t have event with ch_id %d (%s, %s)" % \
						(self.id_name, _old_ch_id, _valid_type, _part)
					continue

				_ref_MaxAmp = 0
				for _res_id in range(_res_query.GetRowCount()):
					_row_query    = _res_query.Next()
					_tmp_corrUndo = int(_row_query.GetField(3))
					if _tmp_corrUndo==_cur_corrUndo:
						_ref_MaxAmp    = float(_row_query.GetField(0))
						_ref_calibLine = int(_row_query.GetField(1))
						_ref_badChan   = int(_row_query.GetField(2))
						_ref_corrUndo  = _tmp_corrUndo

				# check if ref_MaxAmp is not 0
				if _ref_MaxAmp==0:
					continue

				# compute the variable
				_delta_delay = (_ref_MaxAmp - _cur_MaxAmp)/_ref_MaxAmp

				# loop over the different partition
				for _sub_part in LIST_OF_SUB_PART[_part]:
					if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):
						# threshold from DB
						_db_treshold = ECalDBLib.thresholdDict[_sub_part][_run_gain][6]

						# fill the corresponding histograms
						if self.is_swapped:
							swap_ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
								_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_corrUndo, \
								_cur_MaxAmp, _delta_delay, _db_treshold)
						else:
							ntuple[_sub_part].Fill(_cur_ch_id, _cur_layer, _cur_barrel_ec, _cur_pos_neg, \
								_cur_ft, _cur_slot, _cur_channel, _cur_calibLine, _cur_badChan, _cur_corrUndo, \
								_cur_MaxAmp, _delta_delay, _db_treshold)

						# look at the FEB
						if (_cur_ft!=_next_ft or _cur_slot!=_next_slot): # we are looking at the next FEB
							# fill occupancy
							_coeff = 1 if _cur_pos_neg==1 else -1
							if self.is_swapped:
								swap_his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)
							else:
								his_occupancy[_sub_part].Fill(_coeff*_cur_slot, _cur_ft)

							# get the mean for the FEB
							if h_temp.GetEntries()!=0:
								FebAverage = h_temp.GetMean()
								if self.is_swapped:
									swap_his_feb_mean[_sub_part].Fill(FebAverage)
								else:
									his_feb_mean[_sub_part].Fill(FebAverage)

								# flag deviating FEB
								if abs(FebAverage)>ECalDBLib.thresholdDict[_sub_part][_run_gain][7]:
									febName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + _run_camp
									if febName in self.deviatingFebList:
										self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
									else:
										self.deviatingFebDict[febName] = []
										self.deviatingFebDict[febName].append([_valid_type, _run_gain, FebAverage])
										self.deviatingFebList.append(febName)

							_next_ft   = _cur_ft
							_next_slot = _cur_slot
							h_temp.Reset()

						# look at the channels
						if not(InList(_ref_badChan, PATCHED_BAD_CHANNEL)) and not(InList(_cur_badChan, PATCHED_BAD_CHANNEL)):
							if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
								continue
						if _ref_calibLine<0 or _cur_calibLine<0:
							continue
						if _ref_corrUndo!=_cur_corrUndo:
							continue

						if (_ref_corrUndo==0 and _cur_corrUndo==0):
							h_temp.Fill(_delta_delay)

							if self.is_swapped:
								swap_his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								swap_his_delay[_sub_part].Fill(_delta_delay)
								swap_his_delay_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_delay)
								swap_his_delay_cl[_sub_part].Fill(_cur_calibLine, _delta_delay)
							else:
								his_occupancy[_sub_part].Fill(_cur_slot, _cur_ft)
								his_delay[_sub_part].Fill(_delta_delay)
								his_delay_ft[_sub_part].Fill(_cur_ft*13 + _cur_slot - 2, _delta_delay)
								his_delay_cl[_sub_part].Fill(_cur_calibLine, _delta_delay)

						# flag deviating channels
						if abs(_delta_delay)>_db_treshold:
							channelName = _sub_part + '_' + str(_cur_ft) + '_' + str(_cur_slot) + '_' + str(_cur_channel) + '_' + _run_camp
							if channelName in self.deviatingChannelList:
								if (_ref_corrUndo==0 and _cur_corrUndo==0):
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_delay, _cur_calibLine])
								elif (_ref_corrUndo==1 and _cur_corrUndo==1):
									self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_delay, _cur_calibLine])
							else:
								self.deviatingChannelDict[channelName] = []
								if (_ref_corrUndo==0 and _cur_corrUndo==0):
									self.deviatingChannelDict[channelName].append([_cur_badChan, _valid_type, _run_gain, _delta_delay, _cur_calibLine])
								elif (_ref_corrUndo==1 and _cur_corrUndo==1):
									self.deviatingChannelDict[channelName].append([_cur_badChan, 'corrUndo'+_valid_type, _run_gain, _delta_delay, _cur_calibLine])
								self.deviatingChannelList.append(channelName)

							# swapped febs
							if self.is_swapped:
								_delta = _delta_delay/_db_treshold
								_abs_delta = abs(_delta)
								_delta_sigma_label = ''

								if _abs_delta>=1 and _abs_delta<3:
									_delta_sigma_label = '1sig'
								elif _abs_delta>=3:
									_delta_sigma_label = '3sig'

								_ch_info  = "part %s: FT %2d, slot %2d, channel %3d\n" % (_sub_part, _cur_ft, _cur_slot, _cur_channel)
								_dev_info = "\t -> badChan %3d, type %s, gain %s, calibLine %3d, delta %9.4f (%s)\n\n" \
									% (_cur_badChan, _valid_type, _run_gain, _cur_calibLine, _delta, _delta_sigma_label)

								_cnt_shift_channels['id'] += 1
								_ch_info = "%4d. %s" % (_cnt_shift_channels['id'], _ch_info)
								_file_shift_febs['file'].write(_ch_info)
								_file_shift_febs['file'].write(_dev_info)

								if _delta_sigma_label=='1sig':
									_cnt_shift_channels['1s'] += 1
								elif _delta_sigma_label=='3sig':
									_cnt_shift_channels['3s'] += 1

			if self.verbose["med"]:
				_percent = 100*(_entry_id+1)//_cur_total_entries
				print MED_INDENT,"%s:\t\t\t %3d%% events processed (%s, %s)" % (self.id_name, _percent, _valid_type, _part)

		# save otput for swapped febs
		if _file_shift_febs['file']!=None:
			_sum_move  = "Total 1sig: %4d\n" % _cnt_shift_channels['1s']
			_sum_move += "Total 3sig: %4d\n" % _cnt_shift_channels['3s']
			_file_shift_febs['file'].write(_sum_move)
			_file_shift_febs['file'].close()
			if _cnt_shift_channels['1s']==0 and _cnt_shift_channels['3s']==0:
				if os.path.isfile(_file_shift_febs['name']):
					os.remove(_file_shift_febs['name'])

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				his_delay[_sub_part].Write()
				his_delay_ft[_sub_part].Write()
				his_delay_cl[_sub_part].Write()
				his_occupancy[_sub_part].Write()
				his_feb_mean[_sub_part].Write()
				# - swap part
				swap_his_delay[_sub_part].Write()
				swap_his_delay_ft[_sub_part].Write()
				swap_his_delay_cl[_sub_part].Write()
				swap_his_occupancy[_sub_part].Write()
				swap_his_feb_mean[_sub_part].Write()

		_file_ntuple.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				ntuple[_sub_part].Write()
				swap_ntuple[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()


	# ----- 7. ValidationOFCFEBs() -----
	def ValidationOFCFEBs(self, refRunDict, curRunDict):
		_run_camp   = curRunDict['campaign']
		_run_num    = curRunDict['id']
		_run_part   = curRunDict['partition']
		_run_gain   = curRunDict['gain']
		_valid_type = 'OFC'

		_file_figure = TFile(self.MakeFigureFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")
		_file_ntuple = TFile(self.MakeNtupleFileName(_run_part, _valid_type, _run_gain, _run_camp), "recreate")

		# definition of the histograms
		h_OFC_cur = dict()
		h_OFC_ref = dict()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				h_OFC_cur[_sub_part] = TH1F("h_OFC_cur_" + _sub_part, "OFCa for the current campaign ", 500, -0.5, 1)
				h_OFC_ref[_sub_part] = TH1F("h_OFC_ref_" + _sub_part, "OFCa for the refrent campaign ", 500, -0.5, 1)

		# get the trees
		refTree, curTree = self.GetTreeForPartition(TREE_NAME_OFC, refRunDict, curRunDict, _valid_type)

		for _part in LIST_OF_PART[_run_part]:
			if not(_part in refTree.keys()): continue
			if not(_part in curTree.keys()): continue
			_ref_tree = refTree[_part]
			_cur_tree = curTree[_part]

			_ref_total_entries = _ref_tree.GetEntries()
			_cur_total_entries = _cur_tree.GetEntries()
			if (_ref_total_entries!=_cur_total_entries) and self.verbose["high"]:
				print MED_INDENT,"%s: WARNING: %s reference and current trees do not have the same number of entries" %\
					(self.id_name, _valid_type)

			_ref_entry  = 0
			_cur_entry  = 0

			while (_ref_entry<_ref_total_entries) and (_cur_entry<_cur_total_entries):
				_ref_tree.GetEntry(_ref_entry)
				_cur_tree.GetEntry(_cur_entry)

				# reference data
				_ref_ch_id     = _ref_tree.channelId
				_ref_ofc_a     = _ref_tree.OFCa
				_ref_calibLine = _ref_tree.calibLine
				_ref_badChan   = _ref_tree.badChan

				# compared data
				_cur_ch_id     = _cur_tree.channelId
				_cur_ofc_a     = _cur_tree.OFCa
				_cur_barrel_ec = _cur_tree.barrel_ec
				_cur_pos_neg   = _cur_tree.pos_neg
				_cur_ft        = _cur_tree.FT
				_cur_slot      = _cur_tree.slot
				_cur_calibLine = _cur_tree.calibLine
				_cur_badChan   = _cur_tree.badChan

				# compare the two tree
				if _ref_ch_id==_cur_ch_id:
					# loop over the different partition
					for _sub_part in LIST_OF_SUB_PART[_part]:
						if SelectedChannel(_sub_part, _cur_barrel_ec, _cur_pos_neg, _cur_ft, _cur_slot):

							# look at the channels
							if not(InList(_ref_badChan, PATCHED_BAD_CHANNEL)) and not(InList(_cur_badChan, PATCHED_BAD_CHANNEL)):
								if not(InList(_ref_badChan, GOOD_CHANNEL_FLAG)) or not(InList(_cur_badChan, GOOD_CHANNEL_FLAG)):
									continue

							if _ref_calibLine<0 or _cur_calibLine<0:
								continue

							for _ofc in _cur_ofc_a:
								h_OFC_cur[_sub_part].Fill(_ofc)
							for _ofc in _ref_ofc_a:
								h_OFC_ref[_sub_part].Fill(_ofc)

					_ref_entry += 1
					_cur_entry += 1

				# sync the tree entries if needed
				elif _ref_ch_id>_cur_ch_id:
					_cur_entry += 1
				elif _ref_ch_id<_cur_ch_id:
					_ref_entry += 1

		# save output to root
		_file_figure.cd()
		for _part in LIST_OF_PART[_run_part]:
			for _sub_part in LIST_OF_SUB_PART[_part]:
				h_OFC_cur[_sub_part].Write()
				h_OFC_ref[_sub_part].Write()

		_file_figure.Close()
		_file_ntuple.Close()
