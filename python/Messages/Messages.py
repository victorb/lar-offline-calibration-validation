#############################################################################
# Authors : Jerome ODIER, jerome.odier@cern.ch
#         : Julien MOREL, julien.morel@cern.ch
#
# Version : 1.0
#
# This file is part of 'AutomaticValidationTools'
#
#############################################################################

import sys, socket, smtplib

#############################################################################

from email.MIMEText import MIMEText

#############################################################################

def multi_split(s, splitters):
	for splitter in splitters:
		s = s.replace(splitter, '|')

	return s.split('|')

#############################################################################
# SUBSYSTEM Messages							    #
#############################################################################

level = 0

#############################################################################

logFileName = "./log.txt"

#############################################################################

messTypeDict = {'DEBUG': 0, 'WARNING': 1, 'ERROR': 2, 'FATAL': 3}
mailListDict = {                                                }

#############################################################################

def AddMessType(key, val):
	if not messTypeDict.has_key(key):
		messTypeDict[key] = val

#############################################################################

def AddMailList(key, val):
	if not mailListDict.has_key(key):
		mailListDict[key] = val

#############################################################################

def Out(key, fmt):
	if messTypeDict.has_key(key):

		if mailListDict.has_key(key):
			Email('larcalib@cern.ch', mailListDict[key], 'Larcalib - AVT message - ' + key, fmt)

		mess = "%s: %s\n" % (key, fmt)

		try:
			fp = open(logFileName, "a")

			try:
				fp.write(mess)

			finally:
				fp.close()

		except IOError:
			pass

		if messTypeDict[key] == 0:
			sys.stdout.write(mess)
		else:
			sys.stderr.write(mess)

		if key == 'FATAL':
			sys.exit(1)

#############################################################################

mailBuffer = []

#############################################################################

def Email(FROM, TO, TITLE, BODY):
	mailBuffer.append([FROM, TO, TITLE, BODY])

#############################################################################

def __finalize__():

	#####################################################################

	L = []

	for mail in mailBuffer:
		L.append(hash(mail[1] + ' - ' + mail[2]))

	#####################################################################

	for mainKey in set(L):

		BODY = ''

		for mail in mailBuffer:
			if mainKey == hash(mail[1] + ' - ' + mail[2]):
				FROM = mail[0]
				TO = mail[1]
				SUBJECT = mail[2]
				BODY = BODY + mail[3] + '\n'

		data = MIMEText(BODY)

		data['From'] = FROM
		data['To'] = TO
		data['Subject'] = SUBJECT

		try:
			server = smtplib.SMTP('cernmx.cern.ch')

			try:
				server.sendmail(FROM, multi_split(TO, [',', ';']), data.as_string())
			finally:
				server.quit()

		except (socket.error, smtplib.SMTPException), e:
			sys.stderr.write(e)

#############################################################################

import atexit

atexit.register(__finalize__)

#############################################################################
# Error									    #
#############################################################################

class Error(Exception):

#############################################################################

	def __init__(self, key, fmt):
		self.key = key
		self.fmt = fmt
		Out(key, fmt)

#############################################################################

	def __str__(self):
		return repr(self.key) + ' - ' + repr(self.fmt)

#############################################################################

