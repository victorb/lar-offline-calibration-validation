
# ----- System -----
import time, os

# ---- Locale -----
from Validation_definitions import *

# ----- ValidationIsRunnig ----
def ValidationIsRunnig(_name):
	_rc = False
	if os.path.isfile(_name):
		_rc = True
	return _rc


# ----- MakeLockFile() -----
def MakeLockFile(_name):
	_rc = True
	try:
		_file = open(_name, 'w').close()
	except:
		print LOW_VERB_INDENT,"ERROR: could not make a lock file"
		_rc = False
	return _rc


# ----- RemoveLockFile() -----
def RemoveLockFile(_name):
	if os.path.isfile(_name):
		try:
			os.remove(_name)
		except:
			print LOW_VERB_INDENT,"ERROR: could not remove lock file"


# ----- SelectedChannel() -----
def SelectedChannel(_sub_part, _barrel_ec, _pos_neg, _ft, _slot):
	# EMB
	if _sub_part=='EMBA':
		return (_barrel_ec==0 and _pos_neg==1 and _slot!=1)
	if _sub_part=='EMBC':
		return (_barrel_ec==0 and _pos_neg==0 and _slot!=1)
	# EMB PS
	if _sub_part=='EMBPSA':
		return (_barrel_ec==0 and _pos_neg==1 and _slot==1)
	if _sub_part=='EMBPSC':
		return (_barrel_ec==0 and _pos_neg==0 and _slot==1)

	# EMEC
	if _sub_part=='EMECA':
		if (_ft==3 or _ft==10 or _ft==16 or _ft==22) and (_slot in range(5, 11)):
			return False
		elif _ft==6:
			return False
		else:
			return (_barrel_ec==1 and _pos_neg==1 and _slot!=1)
	if _sub_part=='EMECC':
		if (_ft==3 or _ft==10 or _ft==16 or _ft==22) and (_slot in range(5, 11)):
			return False
		elif _ft==6:
			return False
		else:
			return (_barrel_ec==1 and _pos_neg==0 and _slot!=1)
	# EMEC PS
	if _sub_part=='EMECPSA':
		if (_ft==3 or _ft==10 or _ft==16 or _ft==22) and (_slot in range(2, 11)):
			return False
		elif _ft==6:
			return False
		else:
			return (_barrel_ec==1 and _pos_neg==1 and _slot==1)
	if _sub_part=='EMECPSC':
		if (_ft==3 or _ft==10 or _ft==16 or _ft==22) and (_slot in range(2, 11)):
			return False
		elif _ft==6:
			return False
		else:
			return (_barrel_ec==1 and _pos_neg==0 and _slot==1)

	# FCAL
	if _sub_part=='FCALA':
		return (_barrel_ec==1 and _pos_neg==1 and _ft==6)
	if _sub_part=='FCALC':
		return (_barrel_ec==1 and _pos_neg==0 and _ft==6)

	# HEC
	if _sub_part=='HECA':
		return (_barrel_ec==1 and _pos_neg==1 and (_ft==3 or _ft==10 or _ft==16 or _ft==22) and (_slot in range(5, 11)))
	if _sub_part=='HECC':
		return (_barrel_ec==1 and _pos_neg==0 and (_ft==3 or _ft==10 or _ft==16 or _ft==22) and (_slot in range(5, 11)))

	return False


# ----- BadChanInterpreter() -----
def BadChanInterpreter(_bad_chan_id):
	_info = ''
	for _i in range(len(BAD_CHANNEL_INFO)):
		if _bad_chan_id&(1<<_i)!=0x0:
			_info += BAD_CHANNEL_INFO[_i] + '/'
	if _info=='':
		return 'None'
	else:
		return _info[0: -1]


# ----- InList() -----
def InList(_bad_chan_id, _good_flag_list):
	_bad_chan_info = BadChanInterpreter(_bad_chan_id)
	for _good_flag in _good_flag_list:
		if _good_flag in _bad_chan_info:
			return True
	return False
