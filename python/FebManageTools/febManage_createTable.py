#!/usr/bin/python

import sqlite3
import os

db_dir = os.environ["VALIDATION_DATABASE_DIR"]
db_name = "%s/FEB_LS2_registration.db" % db_dir
print "\t -> create %s" % db_name

# Data description
desc_data = ["'feb_sn' text primary key", "'feb_slot_new' integer", "'feb_slot_old' integer", "'feb_subPart' text", "'feb_ft' integer", 
			 "'feb_side' integer", "'feb_be' integer", 
			 "'time_insert' DATETIME NOT NULL DEFAULT (strftime('%d-%m-%Y %H:%M:%S', 'now', 'localtime'))"]
desc_data_all = ', '.join(desc_data)

# Connect to DB
con = sqlite3.connect(db_name)
cur = con.cursor()

db_query = 'CREATE TABLE feb_pos (%s)'%(desc_data_all)
print "\t -> query %s" % db_query
cur.execute(db_query)

#db_query = 'CREATE TABLE before (%s)'%(desc_data_all)
#print "\t -> query %s" % db_query
#cur.execute(db_query)

# Save (commit) the changes
con.commit()

# Disconnect
con.close()




