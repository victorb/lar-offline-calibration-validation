
# ----- System -----
import sqlite3
import sys
import os

# ----- Locale -----
from FebManage_CrateMap import *

# ===== FebManage =====
class FebManage:
	def __init__(self, _verbose):
		# set verbosity
		self.verbose = _verbose
		self.db_name = "%s/FEB_LS2_registration.db" % os.environ["VALIDATION_DATABASE_DIR"]
		self.table_name = 'feb_pos'
		
		
	# ----- BuildDict() -----
	def BuildDict(self, _array1, _array2):
		if _array1 is None:
			return []
		_keys = []
		_dict = []
		for _a1 in _array1:
			_keys.append(_a1[0])
		for _a2 in _array2:
			_dict.append(dict(zip(_keys, _a2)))
		return _dict
		
		
	# ----- ParseFebName() -----
	def ParseFebName(self, _feb_name):
		_ft = None
		_sub_part = None
		_id_side = None
		_be = None
		
		#FEB_EMBA3_10L_PS
		#FEB_EMECA1_03R_PS
		#FEB_HECC1_06L_H2
		_tmp = _feb_name.split("_")
		_raw_part = _tmp[1]
		_crate    = _tmp[2]
		_layer    = _tmp[3]
		
		# Get FT
		_side = _raw_part[-2]
		_part = _raw_part[:-2]
		
		if _side=='A':
			_id_side = 1
		elif _side=='C':
			_id_side = 0
		
		if _part=='EMB':
			_be = 0
			if _side=='A':
				_ft = BARREL_A_FT_CRATE_MAP[_crate]
			elif _side=='C':
				_ft = BARREL_C_FT_CRATE_MAP[_crate]
		else:
			_be = 1
			if _side=='A':
				_ft = ENDCAP_A_FT_CRATE_MAP[_crate]
			elif _side=='C':
				_ft = ENDCAP_C_FT_CRATE_MAP[_crate]
		
		# Get SUB_PART
		_sub_part = _part + _side
		if _layer=='PS':
			_sub_part = _part + _layer + _side
			
		return _ft, _sub_part, _id_side, _be
	
	
	# ----- GetFebSlotDict -----
	def GetFebSlotDict(self):
		if self.verbose['low']:
			print "\t-> GetFebSlotDict() starting, database is %s" % self.db_name
		
		_query = "SELECT * FROM %s " % self.table_name
		# Connect to DB
		rows = {}
		con = None
		try:
			con = sqlite3.connect(self.db_name)
			cur = con.cursor()
			cur.execute(_query)
			rows = self.BuildDict(cur.description, cur.fetchall())
		except sqlite3.Error as _err:
			raise ValueError("ERROR in GetFebSlotDict() %s" % _err)
		except Exception as _err:
			raise ValueError("ERROR in GetFebSlotDict() %s" % _err)
		finally:
			if con: 
				con.close()
		
		return rows
		
		
	# ----- ShowFeb() -----
	def ShowFeb(self, _name_feb):
		if self.verbose['low']:
			print "\t-> ShowFeb() starting: search \"%s\" in table %s" % (_name_feb, self.db_name)
			
		_query = "SELECT * FROM %s WHERE feb_sn='%s'" % (self.table_name, _name_feb)
		# Connect to DB
		rows = {}
		con = None
		try:
			con = sqlite3.connect(self.db_name)
			cur = con.cursor()
			cur.execute(_query)
			rows = self.BuildDict(cur.description, cur.fetchall())
		except sqlite3.Error as _err:
			raise ValueError("ERROR in ShowTable() %s" % _err)
		except Exception as _err:
			raise ValueError("ERROR in ShowTable() %s" % _err)
		finally:
			if con: 
				con.close()
		
		if len(rows)==0:
			print "\t\t - %s not found" % _name_feb
		else:
			print "\t\t","-"*100
			print "\t\t   FEB_serial_number   Slot_new    Slot_old    Sub_part    FT  Side    BE           TimeInsert"
			print "\t\t","-"*100
			for row in rows:
				print "\t\t%20s   %8d   %9d   %9s   %3d   %3d   %3d  %s" \
					% (row['feb_sn'], row['feb_slot_new'], row['feb_slot_old'], row['feb_subPart'], row['feb_ft'], row['feb_side'], row['feb_be'], row['time_insert'])
			print ""
		
	
	# ----- ShowTable() -----
	def ShowTable(self):
		if self.verbose['low']:
			print "\t-> ShowTable() starting: show table \"%s\" from %s" % (self.table_name, self.db_name)
		
		_query = "SELECT * FROM %s " % self.table_name
		# Connect to DB
		rows = {}
		con = None
		try:
			con = sqlite3.connect(self.db_name)
			cur = con.cursor()
			cur.execute(_query)
			rows = self.BuildDict(cur.description, cur.fetchall())
		except sqlite3.Error as _err:
			raise ValueError("ERROR in ShowTable() %s" % _err)
		except Exception as _err:
			raise ValueError("ERROR in ShowTable() %s" % _err)
		finally:
			if con: 
				con.close()
		
		print "\n\t\tFor table \"%s\" found" % self.table_name
		print "\t\t","-"*100
		print "\t\t        FEB_serial_number   Slot_new    Slot_old    Sub_part    FT  Side    BE           TimeInsert"
		print "\t\t","-"*100
		for loop, row in enumerate(rows):
			print "\t\t%3d. %20s   %8d   %9d   %9s   %3d   %3d   %3d  %s" \
				% (loop, row['feb_sn'], row['feb_slot_new'], row['feb_slot_old'], row['feb_subPart'], row['feb_ft'], row['feb_side'], row['feb_be'], row['time_insert'])
		print ""
		
		
	# ----- AddFromFile() -----
	def AddFromFile(self, _file):
		if self.verbose['low']:
			print "\t-> AddFromFile() starting: file \"%s\"" % (_file)
		
		try:
			file_in = open(_file, 'r')
		except IOError as _err:
			raise ValueError("ERROR in AddFromFile() %s" % _err)
		
		_quieries = []
		_lines = file_in.readlines()
		if self.verbose['med']:
			print "\t\t            serial_number   new_slot  old_slot    sub_part      ft  side   be"
			print "\t\t","-"*85
		_loop = 1
		for _line in _lines:
			_tmp = _line.strip("\n").split(" ")
			if _tmp[0][0]=='#':
				continue
			
			_feb_name = _tmp[0]
			_slot_new = int(_tmp[1])
			_slot_old = int(_tmp[2])
			_feb_ft, _feb_sub_part, _feb_id_side, _feb_be = self.ParseFebName(_feb_name)
			
			if _feb_ft==None or _feb_sub_part==None or _feb_id_side==None or _feb_be==None: 
				continue
			
			_query = "'%s','%d','%d','%s','%d','%d','%d',null" \
				% (_feb_name, int(_slot_new), int(_slot_old), _feb_sub_part, _feb_ft, _feb_id_side, _feb_be)
			_query = "INSERT OR REPLACE INTO %s VALUES (%s)" % (self.table_name, _query)
			_quieries.append(_query)
			
			#_query_new = "'%s','%d','%s','%d',null" % (_feb_name, int(_slot_new), _feb_sub_part, _feb_ft)
			#_query_new = 'INSERT OR REPLACE INTO after VALUES (%s)'%(_query_new)
			#_quieries_new.append(_query_new)
			
			if self.verbose['med']:
				print "\t\t%3d. %20s %10d %8d %12s %7d  %4d  %3d" \
					% (_loop, _feb_name, _slot_new, _slot_old, _feb_sub_part, _feb_ft, _feb_id_side, _feb_be)
			_loop += 1
		file_in.close()
		
		# Connect to DB
		con = None
		try:
			con = sqlite3.connect(self.db_name)
			cur = con.cursor()
			
			for _query in _quieries:
				cur.execute(_query)
			con.commit()
			
			#for _query in _quieries_new:
				#cur.execute(_query)
			#con.commit()
		except sqlite3.Error as _err:
			raise ValueError("ERROR in AddFromFile() %s" % _err)
		except Exception as _err:
			raise ValueError("ERROR in AddFromFile() %s" % _err)
		finally:
			if con: 
				con.close()
	
	
	# ----- AddFebManually() -----
	def AddFebManually(self, _feb):
		_feb_name = _feb[0]
		_slot_new = _feb[1]
		_slot_old = _feb[2]
		if self.verbose['low']:
			print "\t-> AddFebManually() starting: feb_name \"%s\", slot_new \"%s\" and slot_old \"%s\"" \
				% (_feb_name, _slot_new, _slot_old)
			
		_feb_ft, _feb_sub_part, _feb_id_side, _feb_be = self.ParseFebName(_feb_name)
		if _feb_ft==None or _feb_sub_part==None or _feb_id_side==None or _feb_be==None: 
			return
		
		if self.verbose['med']:
			print "\t\t- FT %d, SUB_PART %s, SIDE %d, BARREl/ENDACP %d" % (_feb_ft, _feb_sub_part, _feb_id_side, _feb_be)
			
		#"feb_sn text", "feb_slot integer", "feb_subPart text", "feb_ft integer"
		_query = "'%s','%d','%d','%s','%d','%d','%d',null" \
			% (_feb_name, int(_slot_new), int(_slot_old), _feb_sub_part, _feb_ft, _feb_id_side, _feb_be)
		_query = "INSERT OR REPLACE INTO %s VALUES (%s)" % (self.table_name, _query)
		
		#_query_new = "'%s','%d','%s','%d',null" % (_feb_name, int(_slot_new), _feb_sub_part, _feb_ft)
		#_query_new = 'INSERT OR REPLACE INTO after VALUES (%s)'%(_query_new)
		
		# Connect to DB
		con = None
		try:
			con = sqlite3.connect(self.db_name)
			cur = con.cursor()
			cur.execute(_query)
			#cur.execute(_query_new)
			con.commit()
		except sqlite3.Error as _err:
			raise ValueError("ERROR in AddFebManually() %s" % _err)
		except Exception as _err:
			raise ValueError("ERROR in AddFebManually() %s" % _err)
		finally:
			if con: 
				con.close()
		









