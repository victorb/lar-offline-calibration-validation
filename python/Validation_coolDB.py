
# ----- System -----
import time

from PyCool import cool
from CoolConvUtilities.AtlCoolLib import indirectOpen

# ---- Locale -----
from Validation_definitions import *


# ---- GetTimesCoolDB() -----
def GetTimesCoolDB(_run_num):
	db = indirectOpen('COOLONL_TDAQ/CONDBR2', oracle = True)
	if db is None:
		raise ValueError("GetTimesCoolDB(): ERROR <-> cannot connect to COOLONL_TDAQ/CONDBR2 !")

	runiov = _run_num << 32
	
	# SOR
	try:
		folder = db.getFolder('/TDAQ/RunCtrl/SOR_Params')
		obj = folder.findObject(runiov, 0)
		payload = obj.payload()
		sortime = payload['SORTime']
	except Exception, _err:
		try:
			folder = db.getFolder('/TDAQ/RunCtrl/SOR')
			obj = folder.findObject(runiov, 0)
			payload = obj.payload()
			sortime = payload['SORTime']
		except Exception, _err:
			db.closeDatabase()
			raise ValueError("GetTimesCoolDB(): ERROR <-> for run `%d`, problem accessing /TDAQ/RunCtrl/SOR_Params, or /TDAQ/RunCtrl/SOR %s !" \
				% (_run_num, _err))

	# EOR
	try:
		folder = db.getFolder('/TDAQ/RunCtrl/EOR_Params')
		obj = folder.findObject(runiov, 0)
		payload = obj.payload()
		eortime = payload['EORTime']
	except Exception, _err:
		try:
			folder = db.getFolder('/TDAQ/RunCtrl/EOR')
			obj = folder.findObject(runiov, 0)
			payload = obj.payload()
			eortime = payload['EORTime']
		except Exception, _err:
			db.closeDatabase()
			raise ValueError("GetTimesCoolDB(): ERROR <-> for run `%d`, problem accessing /TDAQ/RunCtrl/EOR_Params,or /TDAQ/RunCtrl/EOR %s !" \
				% (_run_num, _err))

	db.closeDatabase()

	sortime /= 1000000000
	eortime /= 1000000000

	return [int(sortime), int(eortime), time.localtime(sortime), time.localtime(eortime)]


# ----- GetMagneticFieldsCoolDB() -----
def GetMagneticFieldsCoolDB(_time_stamp):
	db = indirectOpen('COOLOFL_DCS/CONDBR2', oracle = True)
	if db is None:
		raise  ValueError("GetMagneticFields(): ERROR <-> cannot connect to COOLOFL_DCS/CONDBR2 !")

	solenoidField = 0.0
	solenoidField_req = 0.0
	toroidField = 0.0
	toroidField_req = 0.0

	try:
		folder = db.getFolder('/EXT/DCS/MAGNETS/SENSORDATA')
		objs = folder.findObjects(1000000000*_time_stamp, cool.ChannelSelection.all())

		for obj in objs:
			channel = obj.channelId()
			if channel == 1:
				solenoidField = obj.payload()['value']
			if channel == 2:
				solenoidField_req = obj.payload()['value']
			if channel == 3:
				toroidField = obj.payload()['value']
			if channel == 4:
				toroidField_req = obj.payload()['value']
	except Exception, _err:
		raise ValueError("GetMagneticFields(): ERROR <-> for timestamp `%d`, problem accessing /EXT/DCS/MAGNETS/SENSORDATA, %s !" \
			% (_time_stamp, _err))

	finally:
		db.closeDatabase()

	return [toroidField, solenoidField, toroidField_req, solenoidField_req]

		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
