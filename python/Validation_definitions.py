
# File with list of swapped FEBs
FEBS_SWAPPED_FILE = "/afs/cern.ch/user/l/lardaq/public/SwappedFEBsOnlineIDs.txt"

# Indent values for messages
LOW_INDENT = "\t -"
MED_INDENT = "\t\t ->"
BIG_INDENT = "\t\t\t"

# DB name pattern
# DB_NAME_PATTERN = 'ECalDB_Run2_'
#DB_NAME_PATTERN = 'ECalDB_Run2_cut_'
#DB_NAME_PATTERN = 'ECalDB_Run2_cut_copy_'

#DB_NAME_PATTERN = 'ECalDB_LS2_1_'
#DB_NAME_PATTERN = 'ECalDB_LS2_'
# DB_NAME_PATTERN = 'ECalDB_LS2_copy_'

DB_NAME_PATTERN = 'ECalDB_'

GAIN_SHORT_LIST = ['H', 'M', 'L']

# Time format
DB_TIME_FORMAT    = "YYYY-MM-DD HH24:MI:SS"
HUMAN_TIME_FORMAT = "%Y-%m-%d %H:%M:%S"

# LAr partition
#LAR_PARTITION = ['EMBA', 'EMBC', 'EMBPSA', 'EMBPSC', 'EMECA', 'EMECC', 'EMECPSA', 'EMECPSC', 'FCALA', 'FCALC', 'HECA', 'HECC']
#LAR_PARTITION = ['FCALA', 'FCALC']

FEB_DEV_NAME_LIST  = ['PEDESTAL', 'AUTOCORR', 'NOISE', 'RAMP', 'DELAY', 'PEAKTIME', 'FEBTIME']
FEB_GAIN_NAME_LIST = ['H', 'M', 'L', 'HM', 'ML']

CHANNEL_DEV_NAME_LIST  = ['PEDESTAL', 'AUTOCORR', 'NOISE', 'RAMP', 'DELAY', 'PEAKTIME', 'corrUndoRAMP', 'corrUndoDELAY', 'corrUndoPEAKTIME']
CHANNEL_GAIN_NAME_LIST = ['H', 'M', 'L']

OTHER_DEV_NAME_LIST = ['FEBTIME', 'OFC', 'PEAKTIME']

TREE_NAME_AUTOCORR = 'AUTOCORR'
TREE_NAME_PEDESTAL = 'PEDESTALS'
TREE_NAME_NOISE    = 'PEDESTALS'
TREE_NAME_RAMP     = 'RAMPS'
TREE_NAME_PEAKTIME = 'CALIWAVE'
TREE_NAME_DELAY    = 'CALIWAVE'
TREE_NAME_OFC      = 'OFC'

# List of LAr partitions
LIST_OF_PART = {}
LIST_OF_PART['ALL']     = ['EMB', 'EMEC', 'EMBPS', 'HEC', 'FCAL']
LIST_OF_PART['EM']      = ['EMB', 'EMEC']
LIST_OF_PART['EMBPS']   = ['EMBPS']
LIST_OF_PART['HECFCAL'] = ['HEC', 'FCAL']

# List of LAr sub partitions
LIST_OF_SUB_PART = {}
LIST_OF_SUB_PART['EMB']   = ['EMBA', 'EMBC']
LIST_OF_SUB_PART['EMEC']  = ['EMECA', 'EMECC', 'EMECPSA', 'EMECPSC']
LIST_OF_SUB_PART['EMBPS'] = ['EMBPSA', 'EMBPSC']
LIST_OF_SUB_PART['FCAL']  = ['FCALA', 'FCALC']
LIST_OF_SUB_PART['HEC']   = ['HECA', 'HECC']

# Good channel flags
GOOD_CHANNEL_FLAG = ['None', 'lowNoiseHG']

DQ_FLAG = {'GREEN':0, 'YELLOW':1, 'RED':2, 'UNDEFINED':3}

BAD_CHANNEL_INFO = [
	'deadReadout', 'deadCalib', 'deadPhys',
	'almostDead', 'short', 'unstable', 'distorted',
	'lowNoiseHG', 'highNoiseHG', 'unstableNoiseHG',
	'lowNoiseMG', 'highNoiseMG', 'unstableNoiseMG',
	'lowNoiseLG', 'highNoiseLG', 'unstableNoiseLG',
	'missingFEB', 'peculiarCL', 'problematicFor?', 'sporadicBurstNoise'
	]
PATCHED_BAD_CHANNEL = ['deadCalib', 'deadReadout', 'deadPhys', 'almostDead', 'short']

DICT_PARTITION = {
	'Barrel-EMB-EMEC':'EM',
	'EndCap-EMB-EMEC':'EM',
	'Barrel-EMBPS'   :'EMBPS',
	'EndCap-HEC-FCAL':'HECFCAL'
	#'Barrel-EMB-EMEC':'EM',
	#'EndCap-EMB-EMEC':'EM',
	#'Barrel-EMBPS'   :'EMBPS',
	#'EndCap-HEC-FCAL':'HECFCAL',
	#'Barrel-EMB-EMEC':'EM',
	#'EndCap-EMB-EMEC':'EM',
	#'EndCap-HEC-FCAL':'HECFCAL'
	}
DICT_GAIN = {'HIGH':'H', 'MEDIUM':'M', 'LOW':'L'}

DICT_RUN = {'PEDESTAL':0, 'DELAY':1, 'RAMP':2}
