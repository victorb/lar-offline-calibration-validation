#!/usr/bin/python

# ----- System -----
import sqlite3
import sys, os, argparse

# ----- Locale -----
from ECalDB_core import ECalDB
from ECalDB_definitions import *

# ------------------------------------------------------------------
# In case: sqlite3.DatabaseError: database disk image is malformed
# 
# Step 1
# > sqlite3 your_db_name.db (for example ECalDB_LS2_L.db)
# sqlite> .mode insert
# sqlite> .output dump.sql
# sqlite> .dump
# sqlite> .exit
#
# Step 2
# > sqlite3 fixed.db < dump.sql
#
# Step 3
# > cp fixed.db your_db_name.db
#
# Step 4
# > rm dump.sql fixed.db
# ------------------------------------------------------------------

# ----- CreateParser() -----
def CreateParser():
	parser = argparse.ArgumentParser(description='Removing rows from DB', 
								  formatter_class=argparse.RawTextHelpFormatter,
								  usage=' %(prog)s  [options]')
	
	parser.add_argument('--id_camps', type=int, required=True, nargs=2, 
					 help='<Required> The campaigns from id_camps[0] to id_camps[1] will be removed from DB\n\n')
	
	return parser
	

# ----- MAIN -----
if __name__ == "__main__":
	_name_prog = sys.argv[0]
	
	# get the job input parameters
	parser = CreateParser()
	argSpace = parser.parse_args(sys.argv[1:])
	
	_verbose = {"low":True, "med":True, "high":True}
	_db_dir_name = os.environ["VALIDATION_DATABASE_DIR"]
	
	_id_first = int(argSpace.id_camps[0])
	_id_last  = int(argSpace.id_camps[1])
	
	if _id_last<_id_first:
		print "%s: ERROR id_camps[1] less then id_camps[0]" % _name_prog
		sys.exit()
		
	if _id_last<1 or _id_first<1:
		print "%s: ERROR wrong value of id_camps[i] (id_camps[i]>=1)" % _name_prog
		sys.exit()

	_id_list = range(_id_first, _id_last+1)
	_gain_list = ['H', 'M', 'L']
	#_gain_list = ['L']
	for _gain in _gain_list:
		db = ECalDB(_gain, _verbose)
		db.Open(_db_dir_name)
		print "Delete all rows from DB with gain %s" % _gain
		for _id in _id_list:
			print "\tgain %s: delete campaign with ID %d" % (_gain, _id)
			db.DeleteRowsByCampaignId(_id)
		print "\tgain %s: start execute vacuum" % _gain
		db.ExecuteVacuum()
		db.Close()




















    



